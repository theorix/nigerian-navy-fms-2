class DraftManager {
  constructor (options) {
    let draftContainer = options.pageContainer
    let draftMenuContainer = options.menuContainer
    let htmlContent = options.htmlContent
    let originalContent = options.originalContent || htmlContent
    let tags = options.tags
    let fileId = options.fileId
    let viewMode = options.viewMode || false
    let debugMode = options.debugMode || false

    // perform sanity checks
    if (!fileId) throw (new Error('Invalid File'))
    if (typeof draftContainer === 'string') {
      let container = document.getElementById(draftContainer)
      if (container) this.container = container
      else throw (new Error(`Invalid container: ${draftContainer}`))
    } else {
      if (document.body.contains(draftContainer)) this.container = draftContainer
      else throw (new Error(`Invalid container node`))
    }

    if (!viewMode) { // do not add menu on view mode
      if (typeof draftMenuContainer === 'string') {
        let menuContainer = document.getElementById(draftMenuContainer)
        if (menuContainer) this.menuContainer = menuContainer
        else throw (new Error(`Invalid Menu container: ${draftMenuContainer}`))
      } else {
        if (document.body.contains(draftMenuContainer)) this.menuContainer = draftMenuContainer
        else throw (new Error(`Invalid menu container node`))
      }
    }

    // create page container
    this.pageContainer = document.createElement('div')
    this.pageContainer.style.cssText = `margin-left: 20px; margin-right: 20px; padding-left: 30px; padding-right: 30px; padding-top: 20px; padding-bottom: 20px; background-color: white; color: black; word-wrap: break-word`
    this.pageContainer.innerHTML = htmlContent

    // initialize
    this.content = htmlContent
    this.nodes = []
    this.selecting = false
    this.currentSelection = {} // represents current selection on screen
    this.corrections = []
    this.tags = tags || []
    // this.container.innerHTML = htmlContent
    this.container.appendChild(this.pageContainer)
    this.originalContent = originalContent
    this.backupContent = htmlContent
    this.eventCount = this.tags.length
    this.viewMode = viewMode
    this.disabledTags = false // indicates whether tags are disabled
    this.attachedEvents = false // indicates whether mouse over events have been attached for the first time
    this.scrollPosition = { // represents the coordinates and distance of container scrollbar
      start: 0,
      end: 0,
      distance: 0,
      changed: false
    }
    this.fileId = fileId
    this.eventColors = {
      'Rephrase': 'rgba(255, 165, 0, .5)',
      'Remove': 'rgba(255, 50, 50, .3)',
      'Grammar Error': 'rgba(255,0,0, 0.6)',
      'Comment': 'rgba(255,85,34,.25)'
    }
    this.debugMode = debugMode

    // add mouse events
    let that = this
    if (!viewMode) {
      this.container.onmousedown = (e) => {
        that.selecting = true
      }

      this.container.onmousemove = (e) => {
        let c = that.getCordinates(e)
        if (that.selecting && !that.nodes.includes(document.elementFromPoint(c.clientX, c.clientY))) {
          // console.log('mouse moving')
          if (that.pageContainer !== document.elementFromPoint(c.clientX, c.clientY))
            that.nodes.push(document.elementFromPoint(c.clientX, c.clientY))
        }
      }

      this.container.onmouseup = (e) => {
        // console.log('mouse up')
        that.currentSelection = that.getSelection()

        // remove all child nodes
        for (let i = 0; i < that.nodes.length; i++) {
          for (let j = 0; j < that.nodes.length; j++) {
            if (that.nodes[i].contains(that.nodes[j]) && that.nodes[i] !== that.nodes[j]) {
              // remove node[j]
              that.nodes.splice(j, 1)
              i = -1
              break
            }
          }
        }
        that.selecting = false
      }

      // populate menu container
      let menuList = document.createElement('ul')
      for (let e in this.eventColors) {
        let item = document.createElement('li')
        let hoverCss = `transition: all 3s; text-align: center; cursor: pointer; background-color: ${this.eventColors[e]}; color: black; padding: 5px; margin: 5px; border-radius: 10px;`
        let normalCss = `transition: all 3s; cursor: pointer; background-color: ${this.eventColors[e]}; color: black; padding: 5px; margin: 5px; border-radius: 10px`
        item.style.cssText = normalCss
        // item.className = 'menu-transition'
        item.onmouseover = () => {
          // item.style.cssText = hoverCss
          item.style.textAlign = 'center'
          item.style.transition = 'text-align 1s'
        }
        item.onmouseout = () => {
          // item.style.cssText = normalCss
          item.style.textAlign = 'left'
          item.style.transition = 'text-align 2s'
        }

        item.innerHTML = e
        item.onclick = () => { if (!that.disabledTags) that.markSelection(e) }
        menuList.appendChild(item)
      }
      that.menuContainer.appendChild(menuList)
    }

    this.container.addEventListener('mouseover', () => {
      // attach mouse events for the first time when DOM is ready
      if (!that.attachedEvents) {
        that.attachEvents()
        that.attachedEvents = true
      }
    })

    // detect scroll event on the container and update scrollPosition accordingly
    this.scrollDistance((distance, start, end) => {
      let oldPosition = that.scrollPosition
      that.scrollPosition = { // set new position
        start,
        end,
        distance,
        changed: false
      }

      if (that.scrollPosition.end !== oldPosition.end) {
        that.scrollPosition.changed = true
        // re-attach all mouse events to reflect new coordinates
        that.attachEvents()
      }
      // console.log('You travelled ' + parseInt(Math.abs(distance), 10) + 'px ' + (distance < 0 ? 'up' : 'down'));
    })
  }

  stripTags (html) {
    return html.replace(/(<([^>]+)>)/gi, '')
  }

  decodeHtml (html) {
    let strippedString = this.stripTags(html)
    var txt = document.createElement('textarea')
    txt.innerHTML = strippedString
    return txt.value
  }

  encodeHTMLEntities (txt) {
    let p = document.createElement('p')
    p.textContent = txt
    return p.innerHTML
  }

  generateEventWrapper (htmlStr, options) {
    // if (!htmlStr) return ''
    let wrapped = `<span style='background-color: ${options.color}; text-decoration: line-through black solid; cursor: pointer' class = 'fms-v-2.7 ${options.eventClass}'>${htmlStr}</span>`
    console.log('generateEventWrapper:', htmlStr, wrapped)
    return wrapped
  }

  hasWrapperEvent (node) {
    if (node.tagName === 'SPAN' && node.className === 'fms-v-2.7') return true
    return false
  }

  textNodesUnder (node) {
    let all = []
    for (node = node.firstChild; node; node = node.nextSibling) {
      if (node.nodeType === 3) all.push(node)
      else all = all.concat(this.textNodesUnder(node))
    }
    return all
  }

  getCordinates (e) {
    let cordinates = {
      movementX: e.movementX,
      movementY: e.movementY,
      clientX: e.clientX,
      clientY: e.clientY,
      offsetX: e.offsetX,
      offsetY: e.offsetY,
      pageX: e.pageX,
      pageY: e.pageY,
      screenX: e.screenX,
      screenY: e.screenY,
      region: e.region,
      which: e.which
    }
    return cordinates
  }

  getSelection () {
    var selObj = window.getSelection()
    var selRange = selObj.getRangeAt(0)
    let text = selObj.toString()
    return {
      text,
      startOffset: selRange.startOffset,
      endOffset: selRange.endOffset,
      anchorOffset: selObj.anchorOffset,
      focusOffset: selObj.focusOffset,
      startNode: selRange.startContainer,
      endNode: selRange.endContainer
    }
  }

  /*
    this function is similar to string.replace() function, except that it only replaces the substr that occurs between position i, j
  */
  replaceBetween (i, j, substr, newSubstr, string) {
    let leftStr = string.substr(0, i)
    let str = string.substr(i, substr.length)
    let rightStr = string.substr(j + 1)

    // replace str
    str = str.replace(substr, newSubstr)

    // return concatenated string
    return leftStr + str + rightStr
  }

  /*
    given a start and end positions (i and j respectively) in a string, find the nth substring in the string
    in other words, assume that the substr occurs multiple times in a string, find the nth substr that accurs between i, j
    Note: A return value of 0 (zero) means that the substr never occurs in the string
    the function works by sliding the substr through the string to
    1. find a match
    2. count the number of matches
    3. return the number matches so far if position intersects (or returns a zero if no match is found)
  */
  getOffset (i, j, substr, string) {
    let m = 0
    let n = substr.length
    let offset = 0
    for (let k = 0; k <= string.length - substr.length; k++) {
      if (string.substr(m, n) === substr) offset++
      if (m === i) return offset
      m++
    }
    return 0
  }

  /*
    given an offset (which represents the nth substr in a string), find the starting position of the substr
    the function works by sliding the substr through the string to
    1. find a match
    2. count the number of matches
    3. return the starting position of the substr if number of matches = offset
  */
  getPosition (offset, substr, string) {
    let m = 0
    let n = substr.length
    let p = 0
    let str = this.stripTags(string)
    // str = str.replace(/[\n\r]+/g, '')
    // substr = substr.replace(/[\n\r]+/g, '')
    // console.log('strippedTags:', str)
    // console.log('ok:', substr)

    // slide the substr through the string
    for (let k = 0; k <= str.length - substr.length; k++) {
      let subs = str.substr(m, n)
      if (subs === substr) p++
      if (p === offset) return m
      m++
    }
    return -1
  }

  replaceWithoutTags (offset, substr, string, options) {
    console.log('********************* ReplaceWithout Tags************************')

    let str = string
    let tokens = this.tokenize(string)
    let modifiedStr = ''
    let i = offset.startIndex
    let j = offset.endIndex
    let initialStart = i
    let initialEnd = j

    let lengthTags = 0
    let stringParts = ''
    let foundStart = false
    let foundEnd = false
    let prevTag

    for (let t of tokens) {
      if (t.type === 'string') {
        stringParts += t.data
        if (!foundStart) {
          // update start and end index
          if (prevTag) {
            let incrI = lengthTags - prevTag.data.length - (i - initialStart)
            let incrJ = lengthTags - prevTag.data.length - (j - initialEnd)
            i += incrI + prevTag.data.length
            j += incrJ + prevTag.data.length
          }

          for (let u = t.i; u <= t.j; u++) {
            if (u === i) {
              foundStart = true
              console.log('FOUND START:', u, i)
            }

            if (u === j) {
              foundEnd = true
              console.log('FOUND END:', u, j)
            }
          }

          let L = stringParts.length - t.data.length + lengthTags

          if (foundStart && foundEnd) {
            let s1 = str.substr(L, i - L)
            let s2 = str.substr(i, j - i + 1)
            let s3 = str.substr(j + 1, t.data.length - 1 + L - j)
            modifiedStr += s1
            modifiedStr += this.generateEventWrapper(s2, options)
            modifiedStr += s3
          } else if (foundStart) {
            let s = t.data.substr(0, i - L)
            if (s) modifiedStr += s
            modifiedStr += this.generateEventWrapper(t.data.substr(i - L), options)
          } else modifiedStr += t.data
        } else if (foundStart && !foundEnd) {
          // update the end
          if (prevTag) {
            let incrJ = lengthTags - prevTag.data.length - (j - initialEnd)
            j += incrJ + prevTag.data.length
          }

          let k = 0
          for (let u = t.i; u <= t.j; u++) {
            if (u === j) {
              foundEnd = true
              break
            } else k++
          }
          if (foundEnd) {
            let sub1 = t.data.substr(0, k)
            let sub2 = t.data.substr(k)
            if (sub1) modifiedStr += this.generateEventWrapper(sub1, options)
            if (sub2) modifiedStr += sub2
          } else modifiedStr += this.generateEventWrapper(t.data.substr(0), options)
        } else if ((!foundStart && !foundEnd) || (foundStart && foundEnd)) {
          modifiedStr += t.data
        }
      } else { // t.type === tag
        lengthTags += t.data.length
        prevTag = t
        modifiedStr += t.data
      }
    }
    console.log('********************* ************************')

    return modifiedStr
  }

  tokenize (string) {
    let str = string // main string
    let isClosingTag = false
    let matchedStr = ''
    let tokens = []
    let i = 0 // substr index

    for (let k = 0; k < str.length; k++) {
      // match html tags
      if (str[k] === '<') {
        isClosingTag = str[k + 1] === '/'
        if (matchedStr) {
          tokens.push({
            type: 'string',
            data: matchedStr,
            i,
            j: k - 1
          })
        }
        matchedStr = '<'
        i = k
        continue
      }
      if (str[k] === '>') {
        matchedStr += str[k]
        tokens.push({
          type: 'tag',
          isClosingTag,
          data: matchedStr,
          i: i,
          j: k
        })
        // reset state
        i = k + 1
        matchedStr = ''
        continue
      }

      matchedStr += str[k]
    }

    if (matchedStr) {
      tokens.push({
        type: 'string',
        data: matchedStr,
        i: i,
        j: str.length - 1
      })
    }

    // return modified string
    return tokens
  }

  // main entry point to the draft module
  markSelection (selectionType, comment = '') {
    let txt = this.currentSelection.text
    if (txt) {
      this.eventCount++
      let correction = {
        type: selectionType,
        selection: this.currentSelection,
        nodes: this.nodes,
        comment: comment,
        eventClass: `file-${this.fileId}_event-${this.eventCount}`,
        color: this.eventColors[selectionType]
      }
      // console.log('SELECTED:',txt)
      // store tag
      this.corrections.push(correction)
      this.tags.push({
        type: selectionType,
        comment: comment,
        eventClass: `file-${this.fileId}_event-${this.eventCount}`,
        color: this.eventColors[selectionType]
      })

      // modify html accordingly
      this.modifyHTML(correction)

      // re-attach all mouse events
      this.attachEvents()
    }
    this.nodes = []
    this.currentSelection = {}
  }

  attachEvents () {
    for (let c of this.tags) {
      this.actionBox(c)
    }
  }

  createMessageUnder (options) {
    // create message element
    let messages = []
    let elements = this.container.getElementsByClassName(options.eventClass)
    for (let elem of elements) {
      // create message element
      let message = document.createElement('div')
      let title = document.createElement('p')
      title.style.cssText = `border-style: solid; border-top: 0; border-left: 0; border-right: 0; border-color: ${options.color}`
      let msgBody = document.createElement('p')

      title.innerHTML = options.type
      msgBody.innerHTML = options.comment
      message.appendChild(title)
      message.appendChild(msgBody)

      // better to use a css class for the style here
      message.style.cssText = 'position:fixed; color: black; background-color: rgb(230,230,230); font-size: 13px; padding: 10px;'

      // assign coordinates, don't forget "px"!
      let coords = elem.getBoundingClientRect()
      message.style.left = coords.left + 'px'
      message.style.top = coords.bottom + 'px'

      messages.push({
        element: elem,
        content: message
      })
    }

    return messages
  }

  actionBox (options) {
    let that = this
    let messages = that.createMessageUnder(options)
    messages.forEach((message) => {
      message.element.onmouseover = (e) => {
        document.body.append(message.content)
      }

      message.element.onmouseout = (e) => {
        message.content.remove()
      }

      if (!that.viewMode) {
        message.element.onclick = (e) => {
          // console.log('item clicked')
          // let elements = document.getElementsByClassName(options.eventClass)

          let messageContainer = document.createElement('div')
          let inputText = document.createElement('textarea')
          let closeButton = document.createElement('span')
          let okButton = document.createElement('span')
          let removeSelectionButton = document.createElement('span')
          let buttonContainer = document.createElement('div')
          let title = document.createElement('p')

          closeButton.innerHTML = 'x'
          closeButton.style.cssText = `cursor: pointer; color: red; float: right`
          okButton.innerHTML = 'Add comment'
          okButton.style.cssText = `cursor: pointer; color: green; margin-right: 5px; margin-left: 2px`
          removeSelectionButton.innerHTML = 'Untag Text'
          removeSelectionButton.style.cssText = `cursor: pointer; color: blue; margin-left: 5px; margin-right: 5px`
          buttonContainer.appendChild(okButton)
          // buttonContainer.appendChild(closeButton)
          buttonContainer.appendChild(removeSelectionButton)
          closeButton.onclick = () => {
            messageContainer.remove()
          }
          okButton.onclick = () => {
            options.comment = inputText.value
            messageContainer.remove()
            that.actionBox(options)
          }
          removeSelectionButton.onclick = () => {
            message.element.removeAttribute('style')
            message.element.removeAttribute('class')
            // message.element.removeEventListener('mouseover', mouseOver)
            message.element.onmouseover = null
            message.element.onmouseout = null
            message.element.onclick = null
            messageContainer.remove()
            message.content.remove()
          }

          inputText.value = options.comment
          inputText.setAttribute('placeholder', '...comment')
          title.style.cssText = `border-style: solid; border-top: 0; border-left: 0; border-right: 0; border-color: ${options.color}`
          title.innerHTML = options.type

          messageContainer.appendChild(closeButton)
          messageContainer.appendChild(title)
          messageContainer.appendChild(inputText)
          messageContainer.appendChild(buttonContainer)

          // better to use a css class for the style here
          messageContainer.style.cssText = 'position:fixed; color: black; background-color: rgb(230,230,230); font-size: 13px; padding-top: 2px; padding-left: 10px; padding-right: 10px; padding-bottom: 5px'

          // assign coordinates, don't forget "px"!
          let coords = message.element.getBoundingClientRect()

          messageContainer.style.left = coords.left + 'px'
          messageContainer.style.top = coords.bottom + 'px'
          document.body.appendChild(messageContainer)
        }
      }
    })
  }

  modifyHTML (options) {
    // let selectedTxt = this.currentSelection;
    let selectedTxt = options.selection
    let nodes = options.nodes

    let txt = selectedTxt.text
    if (txt) {
      let nodeCount = 0
      for (let node of nodes) {
        nodeCount++ // keep track of number of nodes processed
        if (txt.includes(node.innerText)) { // full selection
          // check for duplicate event wrapper
          // if(this.hasWrapperEvent(node)) return

          // add event wrapper
          node.innerHTML = this.generateEventWrapper(node.innerHTML, options)
          txt = txt.replace(node.innerText, '') // remove processed text from selection
        } else { // half selection
          // initialize
          let str = ''
          let txtNodes = this.textNodesUnder(node) // get all text nodes
          let startIndex = 0
          let endIndex = 0
          let n = 0
          let focusOffset = selectedTxt.focusOffset
          let anchorOffset = selectedTxt.anchorOffset
          let startNode = selectedTxt.startNode
          let endNode = selectedTxt.endNode

          // discard other nodes
          if (nodes.length > 1 && nodeCount === 1) {
            endNode = txtNodes[txtNodes.length - 1]
            focusOffset = txtNodes[txtNodes.length - 1].textContent.length
            str = ''
          }

          if (nodes.length > 1 && nodeCount !== 1) {
            // str = txt.trim()
            startNode = txtNodes[0]
            anchorOffset = 0
          }

          // scan through text nodes to find the appropriate start / end index
          let withinString = false
          for (let i = 0; i < txtNodes.length; i++) {
            if (txtNodes[i] === startNode) {
              startIndex = anchorOffset + n
              withinString = true
            }

            if (withinString) str += txtNodes[i].textContent

            if (txtNodes[i] === endNode) {
              endIndex = focusOffset + n
              withinString = false
            }
            n += txtNodes[i].textContent.length
          }

          // determine the text involved in the selection via the anchorOffset
          if (nodes.length > 1 && nodeCount === 1) {
            str = str.substr(anchorOffset)
            txt = txt.replace(str, '') // remove processed str from selection
          } else if (nodes.length > 1 && nodeCount !== 1) {
            str = txt.trim()
          } else {
            str = txt
          }

          // get the offset from the remaining copied txt
          let offset = this.getOffset(startIndex, endIndex, str, node.innerText)

          // convert txt to HTML entities and replace in innerHTML of node
          let converted = this.encodeHTMLEntities(str)
          let startPos = this.getPosition(offset, converted, node.innerHTML)
          node.innerHTML = this.replaceWithoutTags(
            { offset, startIndex: startPos, endIndex: startPos + converted.length - 1 },
            converted, node.innerHTML, options
          )

          console.log('OFFSET:', offset, startIndex, endIndex)
          console.log('Position:', startPos, startPos + converted.length)
        }
      }
    }
  }

  /*!
  * Run a callback after the user scrolls, calculating the distance and direction scrolled
  * Original Code by Chris Ferdinandi, MIT License, https://gomakethings.com
  * @param  {Function} callback The callback function to run
  * @param  {Integer}  refresh  How long to wait between scroll events [optional]
  */
  scrollDistance (callback, refresh) {
    // Make sure a valid callback was provided
    if (!callback || typeof callback !== 'function') return

    // Variables
    let isScrolling, start, end, distance
    let that = this

    // Listen for scroll events
    this.container.addEventListener('scroll', function (event) {
      // Set starting position
      if (!start) {
        start = that.container.scrollTop
      }

      // Clear our timeout throughout the scroll
      window.clearTimeout(isScrolling)

      // Set a timeout to run after scrolling ends
      isScrolling = setTimeout(function () {
        // Calculate distance
        end = that.container.scrollTop
        distance = end - start

        // Run the callback
        callback(distance, start, end)

        // Reset calculations
        start = null
        end = null
        distance = null
      }, refresh || 66)
    }, false)
  }

  disableTags () {
    this.backupContent = this.pageContainer.innerHTML
    this.pageContainer.innerHTML = this.originalContent
    this.disabledTags = true
  }

  enableTags () {
    this.pageContainer.innerHTML = this.backupContent
    this.disabledTags = false
    // re-attach all mouse events to reflect new coordinates
    this.attachEvents()
  }

  get draftState () {
    return {
      tags: this.tags,
      html: this.pageContainer.innerHTML === this.originalContent ? this.backupContent : this.pageContainer.innerHTML,
      fileId: this.fileId
    }
  }
}

export default DraftManager
