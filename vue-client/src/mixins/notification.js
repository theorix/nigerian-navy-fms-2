const Swal = require('sweetalert2')

export default {
  methods: {
    notify (message, type, timer = 3000) {
      const toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer
      })

      toast({
        type,
        title: message
      })
    },

    showError (message, time = 5000) {
      this.notify(message, 'error', time)
    },

    showSuccess (message, time = 4000) {
      this.notify(message, 'success', time)
    },

    alertSuccess (message, timer = 1500) {
      Swal({
        position: 'top-center',
        type: 'success',
        title: 'Success',
        text: message,
        showConfirmButton: false,
        timer
      })
    },

    alertFailure (message) {
      Swal({
        type: 'error',
        title: 'Error',
        text: message,
        footer: '********* Error *********'
      })
    },

    showMessage (message) {
      Swal(message)
    },

    confirm (option, callback) {
      const swalWithBootstrapButtons = Swal.mixin({
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
      })

      swalWithBootstrapButtons.fire({
        title: option.title || 'Are you sure?',
        text: option.text || "You won't be able to revert this!",
        type: option.type || 'warning',
        showCancelButton: true,
        confirmButtonText: option.confirmBtnText || 'Yes, delete it!',
        cancelButtonText: option.cancelBtnText || 'No, cancel!',
        reverseButtons: true
      }).then((result) => {
        if (result.value) {
          if (callback) callback()
          swalWithBootstrapButtons.fire(
            'Successful',
            option.successText,
            'success'
          )
        } else if (
          // Read more about handling dismissals
          result.dismiss === Swal.DismissReason.cancel
        ) {
          swalWithBootstrapButtons.fire(
            'Cancelled',
            option.failText,
            'error'
          )
        }
      })
    },

    showQueue (option, callback) {
      /*
         queue structure ==> [
                            {
                              title: 'Question 1',
                              text: 'Chaining swal2 modals is easy'
                            },
                            'Question 2',
                            'Question 3'
                          ]
        progress step structure ==> ['1', '2', '3']
      */
      Swal.mixin({
        input: !option.inputType ? 'text' : option.inputType,
        confirmButtonText: 'Next &rarr;',
        showCancelButton: true,
        progressSteps: option.progressSteps
      }).queue(option.queue).then((result) => {
        if (result.value) {
          callback(result)
        }
      })
    }
  }
}
