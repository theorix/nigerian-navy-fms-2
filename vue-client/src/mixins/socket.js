import io from 'socket.io-client'
import config from '../config'
let socket

export default {
  methods: {
    socketConnect: function () {
      let email = localStorage.getItem('myEmail2')
      socket = io(config.socketUrl, { query: `email=${email}` })
      return socket
    },
    getSocket: function () {
      return socket
    },

    socketSend: function (msg) {
      socket.emit('message', JSON.stringify(msg))
    },

    fetchData () {
      const profile = JSON.parse(localStorage.getItem('userProfile'))

      // get server config
      this.$store.dispatch('getServerConfig')

      this.$root.$emit('app_init')
    }
  }
}
