// import axios from 'axios'
// import config from '../config'

export default {
  data () {
    return {
      dept: '',
      subdept: '',
      year: '',
      volume: '',

      deptRefreshed: true,
      sdeptRefreshed: true,
      yearRefreshed: true
    }
  },

  computed: {
    departments: function () {
      return this.$store.getters.departments
    },

    subdepartments: function () {
      return this.$store.getters.subDepartments
    },

    years: function () {
      console.log('ss', this.dept, this.subdept)
      console.log('years geeter:', this.$store.getters.years)
      return this.$store.getters.years
    },

    volumes: function () {
      return this.$store.getters.volumes
    },

    files: function () {
      console.log('years')
    },

    currentYear: function () {
      return new Date().getFullYear()
    }
  },

  watch: {
    dept (newVal, oldVal) {
      if (oldVal || newVal) {
        this.$store.commit('years', [])
        this.$store.commit('volumes', [])
        if (!this.deptRefreshed) this.subdept = ''
        this.deptRefreshed = false
        this.$store.dispatch('getSubDepartments', {
          dept: this.dept
        })
      }
    },

    subdept (newVal, oldVal) {
      console.log('getYears...1', newVal, oldVal)
      this.$store.commit('volumes', [])
      if (!this.sdeptRefreshed) this.year = ''
      this.sdeptRefreshed = false
      if (oldVal || newVal) {
        this.$store.dispatch('getYears', {
          dept: this.dept,
          subdept: this.subdept
        })
      } else {
        console.log('oldV, newV', newVal, oldVal)
      }
    },

    year (newVal, oldVal) {
      if (!this.yearRefreshed) this.volume = ''
      this.yearRefreshed = false
      if (oldVal || newVal) {
        this.$store.dispatch('getVolumes', {
          dept: this.dept,
          subdept: this.subdept,
          year: this.year
        })
      }
    },

    volume (newVal, oldVal) {
      if (oldVal || newVal) {
        let outgoing = this.$route.query.outgoing
        this.$store.dispatch('getFiles', {
          dept: this.dept,
          subdept: this.subdept,
          year: this.year,
          volume: this.volume,
          outgoing
        })
      }
    },

    '$route.query.outgoing': function (newV, oldV) {
      this.dept = ''
      console.log('OUTGOING DOC ARCHIVE E33:', newV)
    }
  },

  mounted () {
    this.dept = this.$route.params.dept
    this.subdept = this.$route.params.subdept
    this.year = this.$route.params.year
    this.volume = this.$route.params.volume
    this.$store.dispatch('getDepartments')
  }
}
