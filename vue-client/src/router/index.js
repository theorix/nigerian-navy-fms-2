import Vue from 'vue'
import Router from 'vue-router'

import Login from '@/components/Login'
import Signup from '@/components/Signup'
import Dashboard from '@/components/Dashboard'
import Profile from '@/components/Profile'
import Archive from '@/components/Archive'
import CreateFile from '@/components/CreateFile'
import Editor from '@/components/helpers/Editor'
import NewVolume from '@/components/NewVolume'
import FileDetails from '@/components/FileDetails'
import LoadVolume from '@/components/extra/LoadVolume'
import FileThread from '@/components/FileThread'
import Draft from '@/components/draft/Draft'
import ListDraft from '@/components/draft/List'
import DraftMenu from '@/components/draft/Menu'
import DraftInbox from '@/components/draft/Inbox'
import CreateThread from '@/components/CreateThread'
import Inbox from '@/components/Inbox'
import Outbox from '@/components/Outbox'
import TrashedMessages from '@/components/TrashedMessages'
import ViewTrashedMessage from '@/components/ViewTrashedMessage'
import ViewMessage from '@/components/ViewMessage'
import ViewOutMessage from '@/components/ViewOutMessage'
import Upload from '@/components/Upload'
import CCUpload from '@/components/CCUpload'
import SignalUpload from '@/components/SignalUpload'
import ViewSignal from '@/components/ViewSignal'
import ManageArchive from '@/components/ManageArchive'

import Chat from '@/components/chat/Chat'

// admin components
import AdminHome from '@/components/admin/AdminHome'
import UserInbox from '@/components/admin/UserInbox'
import UserProfile from '@/components/admin/UserProfile'
import AdminNotification from '@/components/admin/AdminNotification'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/login'
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/register',
      name: 'register',
      component: Signup
    },
    {
      path: '/signup',
      redirect: '/register'
    },
    {
      path: '/loadvolume/:dept/:subdept/:year/:volume',
      component: LoadVolume
    },
    {
      path: '/loadvolume',
      component: LoadVolume
    },
    {
      path: '/dashboard',
      component: Dashboard,
      children: [
        {
          path: 'profile',
          name: 'profile',
          component: Profile
        },
        {
          path: 'archive/:dept?/:subdept?/:year?/:volume?',
          component: Archive
        },
        {
          path: 'archive/:dept/:subdept/:year/:volume/:fileId',
          component: FileDetails
        },
        {
          path: 'file/thread/:fileId',
          component: FileThread
        },
        // {
        //   path: 'file/draft/:fileId',
        //   component: Draft
        // },
        {
          path: 'draft/menu',
          component: DraftMenu
        },
        {
          path: 'draft/inbox',
          component: DraftInbox
        },
        {
          path: 'draft/list',
          component: ListDraft
        },
        {
          path: 'file/draft/:draftType/:id?',
          component: Draft
        },
        {
          path: 'file/thread/create/:dept/:subdept/:year/:volume/:folioId',
          component: CreateThread
        },
        {
          path: 'sec/upload/:type?',
          component: Upload
        },
        {
          path: 'ccupload/:memoId',
          component: CCUpload
        },
        {
          path: 'memo/:type?',
          component: CreateFile
        },
        {
          path: 'memo/:type/:memoId',
          component: CreateFile
        },
        {
          path: 'createfile/editor',
          component: Editor
        },
        {
          path: 'newvolume',
          component: NewVolume
        },
        {
          path: 'details',
          component: FileDetails
        },

        {
          path: 'inbox',
          component: Inbox
        },
        {
          path: 'mail/:type/:mailId',
          component: ViewMessage
        },
        {
          path: 'outbox',
          component: Outbox
        },

        {
          path: 'outbox/:mailId',
          component: ViewOutMessage
        },

        {
          path: 'trash',
          component: TrashedMessages
        },

        {
          path: 'trash/:mailId',
          component: ViewTrashedMessage
        },

        {
          path: 'signals/:type?/:inboxId?',
          component: Inbox
        },
        {
          path: 'signal/:type?/:signalId?',
          component: ViewSignal
        },
        {
          path: 'sig/:type?',
          component: SignalUpload
        },
        {
          path: 'sec/manage/archive/:dept?/:subdept?/:year?/:volume?',
          component: ManageArchive
        },

        {
          path: 'chat',
          component: Chat
        }
      ]
    },

    {
      path: '/admin',
      component: AdminHome,
      children: [
        {
          path: 'user/inbox',
          component: UserInbox
        },
        {
          path: 'user/profile',
          component: UserProfile
        },
        {
          path: 'notification',
          component: AdminNotification
        }
      ]
    },
    // {
    //   path: '/draft',
    //   name: 'draft',
    //   component: Draft
    // },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
