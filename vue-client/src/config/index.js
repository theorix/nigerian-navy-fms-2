
export default {
  baseUrl: 'http://localhost/fms2.0',
  socketUrl: 'http://localhost:4003',
  rootDir: '/fms2.0/hqenc',
  navyInfo: {
  "type": "HEADQUARTERS",
  "name": "EASTERN NAVAL COMMAND",
  "abbrName": "HQENC",
  "region": "Calabar",
  "domain": "hqenc.mil.ng",
  "logo": "encLogo.jpg"
}

}
