import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Meta from 'vue-meta'
import store from './store'
import LoadScript from 'vue-plugin-load-script'

Vue.config.productionTip = false

Vue.use(Meta)
Vue.use(LoadScript)

window.$ = require('jquery')
window.jQuery = require('jquery')

new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app')
