import Vue from 'vue'
import Vuex from 'vuex'

import search from './modules/search'
import mail from './modules/mail'
import chat from './modules/chat'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    search,
    mail,
    chat
  }
})
