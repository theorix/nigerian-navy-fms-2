import axios from 'axios'
import config from '../../../config'

export const getDepartments = function (context, payload) {
  axios.get(`${config.socketUrl}/archive`, { withCredentials: false })
    .then((response) => {
      context.commit('departments', response.data.data)
    }).catch((error) => {
      console.log(error.message)
    })
}

export const getSubDepartments = function (context, payload) {
  axios.get(`${config.socketUrl}/archive/${payload.dept}`, { withCredentials: false })
    .then((response) => {
      context.commit('subDepartments', response.data.data)
    }).catch((error) => {
      console.log(error.message)
    })
}

export const getYears = function (context, payload) {
  axios.get(`${config.socketUrl}/archive/${payload.dept}/${payload.subdept}`, { withCredentials: false })
    .then((response) => {
      console.log('get years:', payload, response)
      context.commit('years', response.data.data)
    }).catch((error) => {
      console.log(error.message)
    })
}

export const getVolumes = function (context, payload) {
  axios.get(`${config.socketUrl}/archive/${payload.dept}/${payload.subdept}/${payload.year}`, { withCredentials: false })
    .then((response) => {
      console.log(response)
      context.commit('volumes', response.data.data)
    }).catch((error) => {
      console.log(error.message)
    })
}

export const getFiles = function (context, payload) {
  let urlStr = `${config.socketUrl}/archive/${payload.dept}/${payload.subdept}/${payload.year}/${payload.volume}`
  if (payload.outgoing) urlStr += '?outgoing=true'
  console.log('FILES URL:', urlStr)
  axios.get(urlStr, { withCredentials: false })
    .then((response) => {
      console.log('files--:', response)
      context.commit('files', response.data.data)
    }).catch((error) => {
      console.log(error.message)
    })
}

export const getServerConfig = function (context, payload) {
  axios.get(`${config.socketUrl}/config`).then((response) => {
    context.commit('storeServerConfig', response.data)
  })
}
