export const archive = function (state) {
  return state.archive
}

export const departments = function (state) {
  return state.departments
}

export const subDepartments = function (state) {
  return state.subdepartments
}

export const years = function (state) {
  // console.log('calledYears:', state.years)
  return state.years
}

export const volumes = function (state) {
  return state.volumes['volumes'] ? state.volumes['volumes'] : []
}

export const files = function (state) { console.log('files: ', state.files)
  return state.files['files'] ? state.files['files'] : []
}

export const department = function (state) {
  return function (id) {
    for (let d of state.departments) {
      if (Number(id) === Number(d.id)) return d
    }
  }
}

export const subDepartment = function (state) {
  return function (id) {
    for (let d of state.subdepartments) {
      if (Number(id) === Number(d.id)) return d
    }
  }
}

export const volume = function (state) {
  return function (id) {
    for (let d of state.volumes) {
      if (Number(id) === Number(d.id)) return d
    }
  }
}

export const searchResult = function (state) {
  return state.searchResult['files']
}

export const file = function (state) {
  return state.file
}

export const appointments = function (state) {
  console.log('appointments', state.appointments)
  return state.appointments
}

export const getFromClipBoard = function (state) {
  return function (key) {
    const v = state.clipBoard[key]
    delete state.clipBoard[key]
    return v
  }
}

export const serverConfiguration = function (state) {
  return state.serverConfig
}
