import Vue from 'vue'

export const departments = function (state, depts) {
  state.departments = depts
}

export const subDepartments = function (state, subdepts) {
  state.subdepartments = subdepts
}

export const years = function (state, yrs) {
  console.log('commited years:', yrs)
  state.years = yrs
}

export const volumes = function (state, volumes) {
  state.volumes = volumes
}

export const files = function (state, files) {
  state.files = files
}

export const searchResult = function (state, result) {
  state.searchResult = result
}

export const file = function (state, f) {
  state.file = f
}

export const appointments = function (state, apts) {
  state.appointments = apts
}

export const saveToClipBoard = function (state, data) {
  if (data.key) {
    state.clipBoard[data.key] = data.value
  }
}

export const storeServerConfig = function (state, conf) {
  Vue.set(state.serverConfig, 'memoDir', conf['memoDir'])
  Vue.set(state.serverConfig, 'signalDir', conf['signalDir'])
  Vue.set(state.serverConfig, 'archiveDir', conf['archiveDir'])
  Vue.set(state.serverConfig, 'signatureDir', conf['signatureDir'])
  Vue.set(state.serverConfig, 'profilePictureDir', conf['profilePictureDir'])
  Vue.set(state.serverConfig, 'outgoingDocsDir', conf['outgoingDocsDir'])
  Vue.set(state.serverConfig, 'draftFinalDocsDir', conf['draftFinalDocsDir'])
}
