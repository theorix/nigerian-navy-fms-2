export default {
  archive: [
    {
      year: 1990,
      department: 'Department 1',
      subdepartment: 'SubDepartment 1',
      volume: 'VXX',
      folio: 1,
      url: '/archive/'
    },

    {
      year: 1992,
      department: 'Department 5',
      subdepartment: 'SubDepartment 5',
      volume: 'XII',
      folio: 3,
      url: '/archive/'
    },

    {
      year: 2001,
      department: 'Department 3',
      subdepartment: 'SubDepartment 3',
      volume: 'III',
      folio: 4,
      url: '/archive/'
    },

    {
      year: 2017,
      department: 'Department 7',
      subdepartment: 'SubDepartment 7',
      volume: 'VXI',
      folio: 5,
      url: '/archive/'
    }
  ],

  departments: [],

  subdepartments: [],

  years: [],
  volumes: [],

  files: [],

  searchResult: [],

  file: {},

  appointments: [],

  serverConfig: {},

  clipBoard: {
    fileId: ''
  }
}
