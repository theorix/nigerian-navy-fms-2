import Vue from 'vue'

export const addGroup = function (state, group) {
  // console.log('new group:', group)
  Vue.set(state.recentChats, group.userData.id, group)
}

export const editGroup = function (state, data) {
  let oldGroup = state.recentChats[data.id]
  console.log('gpData:', data, state.recentChats)
  oldGroup.userData.name = data.name
  oldGroup.members = data.members
  Vue.set(state.recentChats, data.id, oldGroup)
}

export const setCurrentChat = function (state, chat) {
  Vue.set(state, 'currentChat', chat)
}

export const recentChats = function (state, chats) {
  console.log('RECENTS:', chats)
  Vue.set(state, 'recentChats', chats)
}
