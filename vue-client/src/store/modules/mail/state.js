export default {
  inbox: [],
  outbox: [],
  trash: [],
  unread: 0,
  memo: '',

  // signals
  in_signals: [],
  in_unread: 0,
  out_signals: [],
  out_unread: 0,
  ng_signals: [],
  ng_unread: 0,
  nhq_signals: [],
  nhq_unread: 0,
  signalsUnread: 0,

  recentInboxMessages: [],
  recentSignalMessages: [],

  // draft inbox
  draftMessages: [],
  recentDraftMessages: [],
  draft_unread: 0,

  mailCount: {
    ng: 0,
    outgoing: 0,
    incoming: 0,
    nhq: 0,
    regular: 0,
    trash: 0,
    draft: 0
  },

  currentMail: {},

  thread: [],

  stampSignatures: [],

  // inbox sequence states
  inboxState: {
    inbox: {
      on: false,
      state: {}
    },

    incoming: {
      on: false,
      state: {}
    },

    outgoing: {
      on: false,
      state: {}
    },

    ng: {
      on: false,
      state: {}
    },

    nhq: {
      on: false,
      state: {}
    },

    trash: {
      on: false,
      state: {}
    }
  },

  userList: [],
  usersById: {}
}
