import axios from 'axios'
import config from '../../../config'

export const getThread = async function (context, fId) {
  const { data } = await axios.get(`${config.socketUrl}/thread/${fId}`)
  context.commit('newThread', data)
}

export const getTread = async function (context, fId) {
  const { data } = await axios.get(`${config.socketUrl}/thread/${fId}?outgoing=true`)
  context.commit('newThread', data)
}

export const getStamp = async function (context, fId) {
  console.log('stampAction 1:', fId)
  const { data } = await axios.get(`${config.socketUrl}/stamp/${fId}`)
  console.log('stamp Action:', data)
  context.commit('stampSignatures', data)
}

export const getInboxMessages = async function (context, inboxData) {
  let inType = ''
  switch (inboxData.type) {
    case 'incoming':
      inType = 'incoming'
      break

    case 'outgoing':
      inType = 'outgoing'
      break

    case 'ng':
      inType = 'ng'
      break

    case 'nhq':
      inType = 'nhq'
      break

    case 'draft':
      inType = 'draft'
      break

    default:
      inType = 'regular'
  }

  let data = ''
  let url = ''
  if (inType === 'draft') url = `${config.socketUrl}/draft/sequence/${inboxData.userId}/?page=${inboxData.page}&filter=${inboxData.filter}&type=${inType}`
  else url = `${config.socketUrl}/users/${inboxData.userId}/inbox/?page=${inboxData.page}&filter=${inboxData.filter}&type=${inType}`

  if (inboxData.search) {
    url += `&search=${inboxData.search}`
  }
  if (inboxData.startDate) {
    url += `&startDate=${inboxData.startDate}`
  }
  if (inboxData.endDate) {
    url += `&endDate=${inboxData.endDate}`
  }

  data = (await axios.get(url)).data
  context.commit('inboxMessages', data)
}

export const getRecentInboxMessages = async function (context, msgData) {
  let result
  if (msgData.type === 'draft') {
    const { data } = await axios.get(`${config.socketUrl}/draft/sequence/recent/${msgData.userId}`)
    result = data
  } else {
    const { data } = await axios.get(`${config.socketUrl}/inbox/recent/${msgData.userId}/${msgData.type}`)
    result = data
  }
  context.commit('fillRecentMessages', { type: msgData.type, messages: result.data })
}

export const fetchInboxMessage = async function (context, inboxId) {
  const { data } = await axios.get(`${config.socketUrl}/inbox/${inboxId}}`)
  context.commit('inboxMessage', data)
}

export const countInbox = async function (context, countData) {
  let result
  switch (countData.type) {
    case 'incoming':
      result = await axios.get(`${config.socketUrl}/inbox/count/signals/incoming/?userId=${countData.userId}`)
      context.commit('inboxCount', {
        type: 'incoming',
        count: result.data
      })
      break

    case 'outgoing':
      result = await axios.get(`${config.socketUrl}/inbox/count/signals/outgoing/?userId=${countData.userId}`)
      context.commit('inboxCount', {
        type: 'outgoing',
        count: result.data
      })
      break

    case 'ng':
      result = await axios.get(`${config.socketUrl}/inbox/count/signals/ng/?userId=${countData.userId}`)
      context.commit('inboxCount', {
        type: 'ng',
        count: result.data
      })
      break

    case 'nhq':
      result = await axios.get(`${config.socketUrl}/inbox/count/signals/nhq/?userId=${countData.userId}`)
      context.commit('inboxCount', {
        type: 'nhq',
        count: result.data
      })
      break

    case 'signals':
      result = await axios.get(`${config.socketUrl}/inbox/count/signals/?userId=${countData.userId}`)
      context.commit('inboxCount', {
        type: 'signals',
        count: result.data
      })
      break

    case 'trash':
      result = await axios.get(`${config.socketUrl}/inbox/count/trashed/?userId=${countData.userId}`)
      context.commit('inboxCount', {
        type: 'trash',
        count: result.data
      })
      break

    case 'draft':
      result = await axios.get(`${config.socketUrl}/draft/count/sequence/?userId=${countData.userId}`)
      console.log('DRAFT COUNT:', result)
      context.commit('inboxCount', {
        type: 'draft',
        count: result.data
      })
      break

    default:
      result = await axios.get(`${config.socketUrl}/inbox/count/regular/?userId=${countData.userId}`)
      context.commit('inboxCount', {
        type: 'regular',
        count: result.data
      })
  }
}

export const getOutboxMessages = async function (context, outboxData) {
  const { data } = await axios.get(`${config.socketUrl}/users/${outboxData.userId}/outbox/?page=${outboxData.page}`)
  console.log('outbox - action:', data)
  context.commit('outboxMessages', data)
}

export const getTrashedMessages = async function (context, trashData) {
  let url = `${config.socketUrl}/users/${trashData.userId}/trash/?page=${trashData.page}&filter=${trashData.filter}`
  if (trashData.search) {
    url += `&search=${trashData.search}`
  }
  if (trashData.startDate) {
    url += `&startDate=${trashData.startDate}`
  }
  if (trashData.endDate) {
    url += `&endDate=${trashData.endDate}`
  }

  const data = (await axios.get(url)).data
  context.commit('trashedMessages', data)
}

export const getMemo = function (context, fId) {
  axios.get(`${config.socketUrl}/memo/${fId}`)
    .then((res) => {
      console.log('mem resp:', res)
      if (res.data.success) {
        context.commit('memo', res.data.data)
      }
    })
}
