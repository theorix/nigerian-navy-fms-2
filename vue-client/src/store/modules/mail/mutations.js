import config from '../../../config'

export const inboxMessages = function (state, data) {
  console.log('Inbox Messages', data.messages)

  switch (data.type) {
    case 'incoming':
      state.in_signals = data.messages // .reverse()
      break
    case 'outgoing':
      state.out_signals = data.messages // .reverse()
      break

    case 'ng':
      state.ng_signals = data.messages // .reverse()
      break

    case 'nhq':
      state.nhq_signals = data.messages // .reverse()
      break

    case 'draft':
      state.draftMessages = data.messages
      break

    default:
      state.inbox = data.messages // .reverse()
      break
  }
}

export const inboxMessage = function (state, message) {
  state.currentMail = message
}

export const outboxMessages = function (state, messages) {
  state.outbox = messages // outMails
}

export const trashedMessages = function (state, messages) {
  state.trash = messages
}

export const removeFromTrash = function (state, messages) {
  for (let id of messages) {
    for (let i = 0; i < state.trash.length; i++) {
      if (Number(state.trash[i].id) === Number(id)) state.trash.splice(i, 1)
    }
  }
}

export const removeFromInbox = function (state, data) {
  switch (data.type) {
    case 'incoming':
      for (let id of data.messages) {
        for (let i = 0; i < state.in_signals.length; i++) {
          if (Number(state.in_signals[i].id) === Number(id)) {
            if (!Number(state.in_signals[i].commited)) {
              state.in_unread--
              state.signalsUnread--
            }
            state.in_signals.splice(i, 1)
          }
        }
      }
      break
    case 'outgoing':
      for (let id of data.messages) {
        for (let i = 0; i < state.out_signals.length; i++) {
          if (Number(state.out_signals[i].id) === Number(id)) {
            if (!Number(state.out_signals[i].commited)) {
              state.out_unread--
              state.signalsUnread--
            }
            state.out_signals.splice(i, 1)
          }
        }
      }
      break

    case 'ng':
      for (let id of data.messages) {
        for (let i = 0; i < state.ng_signals.length; i++) {
          if (Number(state.ng_signals[i].id) === Number(id)) {
            if (!Number(state.ng_signals[i].commited)) {
              state.ng_unread--
              state.signalsUnread--
            }
            state.ng_signals.splice(i, 1)
          }
        }
      }
      break

    case 'nhq':
      for (let id of data.messages) {
        for (let i = 0; i < state.nhq_signals.length; i++) {
          if (Number(state.nhq_signals[i].id) === Number(id)) {
            if (!Number(state.nhq_signals[i].commited)) {
              state.nhq_unread--
              state.signalsUnread--
            }
            state.nhq_signals.splice(i, 1)
          }
        }
      }
      break

    default:
      for (let id of data.messages) {
        for (let i = 0; i < state.inbox.length; i++) {
          if (Number(state.inbox[i].id) === Number(id)) {
            if (!Number(state.inbox[i].commited)) state.unread--
            state.inbox.splice(i, 1)
          }
        }
      }
      break
  }
}

export const commitSignal = function (state, data) {
  switch (data.type) {
    case 'incomming':
      for (let i = 0; i < state.in_signals.length; i++) {
        let signal = state.in_signals[i]
        if (Number(signal.file_id) === Number(data.id) && !Number(signal.commited)) {
          state.in_signals[i].commited = 1
        }
      }
      break

    case 'outgoing':
      for (let i = 0; i < state.in_signals.length; i++) {
        let signal = state.out_signals[i]
        if (Number(signal.file_id) === Number(data.id) && !Number(signal.commited)) {
          state.out_signals[i].commited = 1
        }
      }
      break

    case 'ng':
      for (let i = 0; i < state.in_signals.length; i++) {
        let signal = state.ng_signals[i]
        if (Number(signal.file_id) === Number(data.id) && !Number(signal.commited)) {
          state.ng_signals[i].commited = 1
        }
      }
      break

    case 'nhq':
      for (let i = 0; i < state.in_signals.length; i++) {
        let signal = state.nhq_signals[i]
        if (Number(signal.file_id) === Number(data.id) && !Number(signal.commited)) {
          state.nhq_signals[i].commited = 1
        }
      }
      break
  }
}

export const markReadInbox = function (state, data) {
  switch (data.type) {
    case 'incoming':
      for (let id of data.messages) {
        for (let i = 0; i < state.in_signals.length; i++) {
          if (Number(state.in_signals[i].id) === Number(id)) {
            if (!Number(state.in_signals[i].commited)) {
              state.in_unread--
              state.signalsUnread--
            }
            state.in_signals[i].commited = 1
          }
        }
      }
      break
    case 'outgoing':
      for (let id of data.messages) {
        for (let i = 0; i < state.out_signals.length; i++) {
          if (Number(state.out_signals[i].id) === Number(id)) {
            if (!Number(state.out_signals[i].commited)) {
              state.out_unread--
              state.signalsUnread--
            }
            state.out_signals[i].commited = 1
          }
        }
      }
      break

    case 'ng':
      for (let id of data.messages) {
        for (let i = 0; i < state.ng_signals.length; i++) {
          if (Number(state.ng_signals[i].id) === Number(id)) {
            if (!Number(state.ng_signals[i].commited)) {
              state.ng_unread--
              state.signalsUnread--
            }
            state.ng_signals[i].commited = 1
          }
        }
      }
      break

    case 'nhq':
      for (let id of data.messages) {
        for (let i = 0; i < state.nhq_signals.length; i++) {
          if (Number(state.nhq_signals[i].id) === Number(id)) {
            if (!Number(state.nhq_signals[i].commited)) {
              state.nhq_unread--
              state.signalsUnread--
            }
            state.nhq_signals[i].commited = 1
          }
        }
      }
      break

    case 'draft':
      for (let id of data.messages) {
        for (let i = 0; i < state.draftMessages.length; i++) {
          if (Number(state.draftMessages[i].id) === Number(id)) {
            if (!Number(state.draftMessages[i].commited)) {
              state.draft_unread--
            }
            state.draftMessages[i].commited = 1
          }
        }
      }
      break

    default:
      for (let id of data.messages) {
        for (let i = 0; i < state.inbox.length; i++) {
          if (Number(state.inbox[i].id) === Number(id)) {
            if (!Number(state.inbox[i].commited)) state.unread--
            state.inbox[i].commited = 1
          }
        }
      }
      break
  }
}

export const inboxCount = function (state, data) {
  switch (data.type) {
    case 'incoming':
      state.mailCount.incoming = data.count.all
      state.in_unread = data.count.unread
      break

    case 'outgoing':
      state.mailCount.outgoing = data.count.all
      state.out_unread = data.count.unread
      break

    case 'ng':
      state.mailCount.ng = data.count.all
      state.ng_unread = data.count.unread
      break

    case 'nhq':
      state.mailCount.nhq = data.count.all
      state.nhq_unread = data.count.unread
      break

    case 'signals':
      state.signalsUnread = data.count.unread
      break

    case 'trash':
      state.mailCount.trash = data.count.all
      break

    case 'draft':
      state.mailCount.draft = data.count.all
      state.draft_unread = data.count.unread
      break

    default:
      state.mailCount.regular = data.count.all
      state.unread = data.count.unread
  }
}

export const newMessage = function (state, message) {
  if (Number(message.type) === 0 || Number(message.type) === 1) {
    if (state.recentInboxMessages.length > 3) {
      state.recentInboxMessages.pop()
    }
    state.recentInboxMessages.unshift(message)
  } else {
    if (state.recentSignalMessages.length > 3) {
      state.recentSignalMessages.pop()
    }
    state.recentSignalMessages.unshift(message)
  }

  switch (Number(message.type)) {
    case 2:
      if (!message.commited) {
        state.in_unread = Number(state.in_unread) + 1
        state.signalsUnread = Number(state.signalsUnread) + 1
      }
      break
    case 3:
      if (!message.commited) {
        state.out_unread = Number(state.out_unread) + 1
        state.signalsUnread = Number(state.signalsUnread) + 1
      }
      break
    case 4:
      if (!message.commited) {
        state.ng_unread = Number(state.ng_unread) + 1
        state.signalsUnread = Number(state.signalsUnread) + 1
      }
      break
    case 5:
      if (!message.commited) {
        state.nhq_unread = Number(state.nhq_unread) + 1
        state.signalsUnread = Number(state.signalsUnread) + 1
      }
      break

    default:
      if (!message.commited) {
        state.unread = Number(state.unread) + 1
      }
  }
}

export const newDraftMessage = function (state, message) {
  if (state.recentDraftMessages.length > 3) {
    state.recentDraftMessages.pop()
  }
  state.recentDraftMessages.unshift(message)

  if (!message.commited) {
    state.draft_unread = Number(state.draft_unread) + 1
  }

  // temporarily store in draftMessages
  state.draftMessages.push(message)
}

export const fillRecentMessages = function (state, data) {
  if (data.type === 'signal') state.recentSignalMessages = data.messages
  else if (data.type === 'draft') state.recentDraftMessages = data.messages
  else state.recentInboxMessages = data.messages
}

export const newOutMessage = function (state, msg) {
  state.outbox.unshift(msg)
}

export const newThread = function (state, f) {
  state.thread = f
}

export const stampSignatures = function (state, stamp) {
  state.stampSignatures = stamp
}

export const thread = function (state, thred) {
  if (state.thread.threads) state.thread.threads.push(thred)
  else state.thread.threads = [thred]
}

export const sign = function (state, signData) {
  const stmp = []
  for (let i = 0; i < state.stamp.length; i++) {
    stmp[i] = {}
    stmp[i].signature = ''
    stmp[i].date = ''
    stmp[i].appointment = state.stamp[i].appointment

    if (state.stamp[i].appointment === signData.appointment) {
      stmp[i].signature = !signData.signature ? `${config.rootDir}/static/img/sign.png` : signData.signature
      stmp[i].date = new Date().toDateString()
    }
    return stmp
  }
}

export const memo = function (state, m) {
  state.memo = m
}

export const decrementUnread = function (state, data) {
  switch (data.type) {
    case 'incomming':
      state.in_unread = state.in_unread - data.amount
      state.signalsUnread = state.signalsUnread - data.amount
      break

    case 'outgoing':
      state.out_unread = state.out_unread - data.amount
      state.signalsUnread = state.signalsUnread - data.amount
      break

    case 'ng':
      state.ng_unread = state.ng_unread - data.amount
      state.signalsUnread = state.signalsUnread - data.amount
      break

    case 'nhq':
      state.nhq_unread = state.nhq_unread - data.amount
      state.signalsUnread = state.signalsUnread - data.amount
      break
    case 'inbox':
      state.unread = state.unread - data.amount
      break
  }
}

// inbox sequence functions
export const turnOnSequence = function (state, data) {
  switch (data.inboxType) {
    case 'incoming':
      state.inboxState.incoming.on = true
      state.inboxState.incoming.state = data
      break

    case 'outgoing':
      state.inboxState.outgoing.on = true
      state.inboxState.outgoing.state = data
      break

    case 'ng':
      state.inboxState.ng.on = true
      state.inboxState.ng.state = data
      break

    case 'nhq':
      state.inboxState.nhq.on = true
      state.inboxState.nhq.state = data
      break

    case 'regular':
      state.inboxState.inbox.on = true
      state.inboxState.inbox.state = data
      break

    case 'trash':
      state.inboxstate.trash.on = true
      state.inboxState.trash.state = data
      break
  }
}

export const turnOffSequence = function (state, path) {
  switch (path) {
    case '/dashboard':
      state.inboxState.inbox.on = false
      break
    case '/dashboard/signals/incomming':
      state.inboxState.incoming.on = false
      break
    case '/dashboard/signals/outgoing':
      state.inboxState.outgoing.on = false
      break
    case '/dashboard/signals/ng':
      state.inboxState.ng.on = false
      break
    case '/dashboard/signals/nhq':
      state.inboxState.nhq.on = false
      break
    case '/dashboard/trash':
      state.inboxState.trash.on = false
  }
}

export const updateInboxState = function (state, data) {
  switch (data.inboxType) {
    case 'incoming':
      state.inboxState.incoming.state = data
      break

    case 'outgoing':
      state.inboxState.outgoing.state = data
      break

    case 'ng':
      state.inboxState.ng.state = data
      break

    case 'nhq':
      state.inboxState.nhq.state = data
      break

    case 'regular':
      state.inboxState.inbox.state = data
      break

    case 'trash':
      state.inboxState.trash.state = data
      break
  }
}

export const setSequenceIndex = function (state, data) {
  switch (data.path) {
    case '/dashboard':
      state.inboxState.inbox.state.index = data.index
      break
    case '/dashboard/signals/incomming':
      state.inboxState.incoming.state.index = data.index
      break
    case '/dashboard/signals/outgoing':
      state.inboxState.outgoing.state.index = data.index
      break
    case '/dashboard/signals/ng':
      state.inboxState.ng.state.index = data.index
      break
    case '/dashboard/signals/nhq':
      state.inboxState.nhq.state.index = data.index
      break
    case '/dashboard/trash':
      state.inboxState.trash.state.index = data.index
  }
}

export const setUserList = function (state, data) {
  state.userList = data.normal
  state.usersById = data.byId
}
