import axios from 'axios'
import config from '../../../config'

export const inbox = function (state) {
  return state.inbox
}

export const draftInbox = function (state) {
  return state.draftMessages
}

export const getRecentInboxMessages = function (state) {
  return function (type) {
    if (type === 'signal') return state.recentSignalMessages
    else if (type === 'draft') return state.recentDraftMessages
    else return state.recentInboxMessages
  }
}

export const outbox = function (state) {
  return state.outbox
}

export const trashedMessages = function (state) {
  console.log('getter-trash:', state.trash)
  return state.trash
}

export const trashedMessage = function (state) {
  return function (id) {
    for (let m of state.trash) {
      if (Number(id) === Number(m.id)) return m
    }
  }
}

export const unread = function (state) {
  return state.unread
}

export const inUnread = function (state) {
  return state.in_unread
}

export const outUnread = function (state) {
  return state.out_unread
}

export const ngUnread = function (state) {
  return state.ng_unread
}

export const nhqUnread = function (state) {
  return state.nhq_unread
}

export const unreadSignals = function (state) {
  console.log('unread signals:', state.signalsUnread)
  return state.signalsUnread
}

export const unreadDraftMessages = function (state) {
  return state.draft_unread
}

// count all (both read and unread) inbox messages
export const cAllRegularMessages = function (state) {
  return state.mailCount.regular
}

export const cAllInSignals = function (state) {
  return state.mailCount.incoming
}

export const cAllOutSignals = function (state) {
  return state.mailCount.outgoing
}

export const cAllNgSignals = function (state) {
  return state.mailCount.ng
}

export const cAllNhqSignals = function (state) {
  return state.mailCount.nhq
}

export const cAllTrash = function (state) {
  return state.mailCount.trash
}

export const mail = function (state) {
  return function (id, mailType) {
    console.log('id:', id)

    switch (mailType) {
      case '0':
      case '1':
        return state.inbox.find((m) => m.id === id)
      case '2':
        return state.in_signals.find((m) => m.id === id)
      case '3':
        return state.out_signals.find((m) => m.id === id)
      case '4':
        return state.ng_signals.find((m) => m.id === id)
      case '5':
        return state.nhq_signals.find((m) => m.id === id)
      default:
        return state.inbox.find((m) => m.id === id)
    }
  }
}

export const outMail = function (state) {
  return function (id) {
    for (let m of state.outbox) {
      if (Number(id) === Number(m.id)) return m
    }
  }
}

export const getCurrentMail = function (state) {
  return state.currentMail
}

export const signals = function (state) {
  console.log('getter signals', state.nhq_signals)
  return function (type) {
    switch (type) {
      case 'incomming':
        return state.in_signals
      case 'outgoing':
        return state.out_signals
      case 'ng':
        return state.ng_signals
      case 'nhq':
        return state.nhq_signals
      default:
        return []
    }
  }
}

export const thread = function (state) {
  console.log('gtters', state.thread)
  return state.thread
}

export const file = function (state) {
  console.log('************file getter*****************')
  return state.thread.file
}

export const memo = function (state) {
  return state.memo
}

export const stamp = function (state) {
  return state.stampSignatures
}

// inbox sequence
export const sequenceIsOn = function (state) {
  return function (path) {
    switch (path) {
      case 'inbox':
        return state.inboxState.inbox.on

      case 'incoming':
        return state.inboxState.incoming.on

      case 'outgoing':
        return state.inboxState.outgoing.on

      case 'ng':
        return state.inboxState.ng.on

      case 'nhq':
        return state.inboxState.nhq.on

      case 'trash':
        return state.inboxState.trash.on
    }
  }
}
export const fetchInboxState = function (state) {
  return function (path) {
    switch (path) {
      case '/dashboard/signals/incomming':
        return state.inboxState.incoming.state

      case '/dashboard/signals/outgoing':
        return state.inboxState.outgoing.state

      case '/dashboard/signals/ng':
        return state.inboxState.ng.state

      case '/dashboard/signals/nhq':
        return state.inboxState.nhq.state

      case '/dashboard':
        return state.inboxState.inbox.state

      case '/dashboard/signals/trash':
        return state.inboxState.trash.state
    }
  }
}

export const fetchNextMail = function (state) {
  function getNext (inboxState, inbxMails, prev = false) {
    console.log('inboxState:', inboxState, inbxMails)
    if (inboxState.index > -2) {
      let i = Number(inboxState.index)
      if (!prev) { // get next mail
        if (i < inbxMails.length - 1) {
          return {
            i: ++i,
            mail: inbxMails[i]
          }
        } else {
          return false // no data, ... fetch from server
        }
      } else { // get previous mail
        if (i > 0) {
          return {
            i: --i,
            mail: inbxMails[i]
          }
        } else {
          return false // no data, ... fetch from server
        }
      }
    }
  }

  async function fetchFromServer (inboxState, prev) {
    const profile = JSON.parse(localStorage.getItem('userProfile'))
    const s = inboxState
    const p = !prev ? (s.currentPage + 1) : (s.currentPage - 1)
    let data

    if (!s.searchTerm) {
      data = (await axios.get(`${config.socketUrl}/users/${profile.id}/inbox/?page=${p}&filter=${s.inboxFilter}&type=${s.inboxType}`)).data
    } else {
      data = (await axios.get(`${config.socketUrl}/users/${profile.id}/inbox/?page=${p}&filter=${s.inboxFilter}&type=${s.inboxType}&search=${s.searchTerm}`)).data
    }
    return data
  }

  return async function (type, prev = false) {
    let m
    switch (type) {
      case 'incoming':
        m = getNext(state.inboxState.incoming.state, state.in_signals, prev)
        if (m) {
          // store the new index
          state.inboxState.incoming.state.index = m.i
          return m.mail
        } else {
          // fetch data from server
          const data = await fetchFromServer(state.inboxState.incoming.state, prev)
          if (data.messages.length > 0) {
            state.in_signals = data.messages
            state.inboxState.incoming.state.index = !prev ? -1 : data.messages.length // reset index
            state.inboxState.incoming.state.currentPage = !prev ? (state.inboxState.incoming.state.currentPage + 1) : (state.inboxState.incoming.state.currentPage - 1)
            m = getNext(state.inboxState.incoming.state, state.in_signals, prev)
            state.inboxState.incoming.state.index = m.i
            return m.mail
          } else return false // no more data
        }

      case 'outgoing':
        m = getNext(state.inboxState.outgoing.state, state.out_signals, prev)
        if (m) {
          // store the new index
          state.inboxState.outgoing.state.index = m.i
          return m.mail
        } else {
          // fetch data from server
          const data = await fetchFromServer(state.inboxState.outgoing.state, prev)
          if (data.messages.length > 0) {
            state.out_signals = data.messages
            state.inboxState.outgoing.state.index = !prev ? -1 : data.messages.length // reset index
            state.inboxState.outgoing.state.currentPage = !prev ? (state.inboxState.outgoing.state.currentPage + 1) : (state.inboxState.outgoing.state.currentPage - 1)
            m = getNext(state.inboxState.outgoing.state, state.out_signals, prev)
            state.inboxState.outgoing.state.index = m.i
            return m.mail
          } else return false // no more data
        }

      case 'ng':
        m = getNext(state.inboxState.ng.state, state.ng_signals, prev)
        if (m) {
          // store the new index
          state.inboxState.ng.state.index = m.i
          return m.mail
        } else {
          // fetch data from server
          const data = await fetchFromServer(state.inboxState.ng.state, prev)
          if (data.messages.length > 0) {
            state.ng_signals = data.messages
            state.inboxState.ng.state.index = !prev ? -1 : data.messages.length // reset index
            state.inboxState.ng.state.currentPage = !prev ? (state.inboxState.ng.state.currentPage + 1) : (state.inboxState.ng.state.currentPage - 1)
            m = getNext(state.inboxState.ng.state, state.ng_signals, prev)
            state.inboxState.ng.state.index = m.i
            return m.mail
          } else return false // no more data
        }

      case 'nhq':
        m = getNext(state.inboxState.nhq.state, state.nhq_signals, prev)
        if (m) {
          // store the new index
          state.inboxState.nhq.state.index = m.i
          return m.mail
        } else {
          // fetch data from server
          const data = await fetchFromServer(state.inboxState.nhq.state, prev)
          if (data.messages.length > 0) {
            state.nhq_signals = data.messages
            state.inboxState.nhq.state.index = !prev ? -1 : data.messages.length // reset index
            state.inboxState.nhq.state.currentPage = !prev ? (state.inboxState.nhq.state.currentPage + 1) : (state.inboxState.nhq.state.currentPage - 1)
            m = getNext(state.inboxState.nhq.state, state.nhq_signals, prev)
            state.inboxState.nhq.state.index = m.i
            return m.mail
          } else return false // no more data
        }

      case 'inbox':
        m = getNext(state.inboxState.inbox.state, state.inbox, prev)
        console.log('getMailsssssss:', m)
        if (m) {
          // store the new index
          state.inboxState.inbox.state.index = m.i
          return m.mail
        } else {
          // fetch data from server
          const data = await fetchFromServer(state.inboxState.inbox.state, prev)
          if (data.messages.length > 0) {
            state.inbox = data.messages
            state.inboxState.inbox.state.index = !prev ? -1 : data.messages.length // reset index
            state.inboxState.inbox.state.currentPage = !prev ? (state.inboxState.inbox.state.currentPage + 1) : (state.inboxState.inbox.state.currentPage - 1)
            m = getNext(state.inboxState.inbox.state, state.inbox, prev)
            state.inboxState.inbox.state.index = m.i
            return m.mail
          } else return false // no more data
        }

      case 'trash':
        m = getNext(state.inboxState.trash.state, state.trash, prev)
        if (m) {
          // store the new index
          state.inboxState.trash.state.index = m.i
          return m.mail
        } else {
          // fetch data from server
        }
        // return state.inboxState.trash.state
    }
  }
}

export const userList = function (state) {
  return state.userList
}

export const userById = function (state) {
  return function (id) {
    return state.usersById[id]
  }
}
