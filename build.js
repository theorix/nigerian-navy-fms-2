const fs = require('fs')
const path = require('path')
var exec = require('child_process').exec

function generateEnvFile (projectName, options) {
  let envFile = `
# Server config
PFSENSE_NAME=Ubuntu16
SERVER_PASSWORD=mr cheat
#PFSENSE_NAME=pfsense-30

## current directory on the server means the 'node-server' directory
SERVER_PORT=${options.nodeServerPort}
CLIENT_DIR_NAME=${projectName}
APP_VERSION=${options.appVersion}

## Server Directories
ARCHIVE_ROOT_URL=/var/www/html/fms${options.appVersion}/${projectName}/static/${projectName.toUpperCase()}
SIGNAL_UPLOAD_DIR=/var/www/html/fms${options.appVersion}/${projectName}/static/uploads/signals
MEMO_UPLOAD_DIR=/var/www/html/fms${options.appVersion}/${projectName}/static/uploads/memos
SIGNATURE_DIR=/var/www/html/fms${options.appVersion}/${projectName}/static/uploads/signatures
PROFILE_PICTURE_DIR=/var/www/html/fms${options.appVersion}/${projectName}/static/uploads/profiles
OUTGOING_DOCS_DIR=/var/www/html/fms${options.appVersion}/${projectName}/static/uploads/outgoing
DRAFT_FINAL_DOCS_DIR=/var/www/html/fms${options.appVersion}/${projectName}/static/uploads/drafts

# client Directories
## These directories are sent to the client during execution
### current directory '.' on the client means the '${projectName}' directory where the client code is deployed
CLIENT_MEMO_DIR=${options.baseUrl}/${projectName}/static/uploads/memos
CLIENT_SIGNAL_DIR=${options.baseUrl}/${projectName}/static/uploads/signals
CLIENT_ARCHIVE_DIR=${options.baseUrl}/${projectName}/static/${projectName.toUpperCase()}
CLIENT_SIGNATURE_DIR=${options.baseUrl}/${projectName}/static/uploads/signatures
CLIENT_PROFILE_PICTURE_DIR=${options.baseUrl}/${projectName}/static/uploads/profiles
CLIENT_OUTGOING_DOCS_DIR=${options.baseUrl}/${projectName}/static/uploads/outgoing
CLIENT_DRAFT_FINAL_DOCS_DIR=${options.baseUrl}/${projectName}/static/uploads/drafts


# database config
DB_DRIVER=mysql
MYSQL_HOST=127.0.0.1
MYSQL_USER=root
MYSQL_PASSWORD=mr cheat
MYSQL_DATABASE=fms_v2_db

# Debug info
DEBUG_MODE=1

`
  fs.writeFileSync('./node-server/.env', envFile)
}

function generateIndexPage (projectName, options) {
  let indexHtml = `
<!DOCTYPE html>
<html>
   <head>
      <title>HTML Meta Tag</title>
      <meta http-equiv = "refresh" content = "6; url = ${options.userUrl}" />
      <style>
        h2 {
          text-align: center;
          color: white;
          font-weight: bold
        }
        a {
          text-decoration: none;
          color: white;
        }
        img {
          width: 200px;
          height; 200px;
          display: block;
          margin-left: auto;
          margin-right: auto;
        }
      </style>
   </head>
   <body>
      <div style='background-color: rgb(46, 109, 164); width: 700px; height: 600px; margin-left: auto; margin-right: auto; margin-top: 20px; padding: 20px'>
        <img src = './${projectName}/static/img/${options.logo}'/>
        <h2>WELCOME TO FMS ${options.appVersion}</h2>
        <h2>FILE MANAGEMENT SYSTEM</h2>
        <h2>${options.type}</h2>
        <h2>${options.name}</h2>

        <h4>Click <a href='${options.userUrl}/${projectName}'>Here</a> to get started</h4>
      </div>
   </body>
</html>
  `
  fs.writeFileSync('./index.html', indexHtml)
}

function runBuild (options) {
  console.log('Building the Vue client...')
  exec(`npm --prefix ./vue-client run build`, (error, stdout, stderr) => {
    if (error) {
      console.log('Error:', error.message)
    } else {
      console.log('Done building Vue.js client\n', 'Copying Static Files...')
      exec(`rm -r ./client/static; mv ./client ./${configName}; cp -r ./static ./${configName}; mv ./${configName}/static/ARCHIVE ./${configName}/static/${configName.toUpperCase()} `, (error, stdout, stderr) => {
        if (error) {
          console.log('Error:', error.message)
        } else {
          console.log('Done.\n', 'BUILD COMPLETED!!!')
          if (flag && (flag === '--bundle' || flag === '-b')) {
            console.log('\nBundling system...')
            let bundleCmd = `mkdir ./fms${options.appVersion}; cp -r node-server ./fms${options.appVersion}; cp -r ${configName} ./fms${options.appVersion}; cp index.html ./fms${options.appVersion}; zip -r fms.zip fms${options.appVersion}; rm -r fms${options.appVersion}`
            exec(bundleCmd, { maxBuffer: 1024 * 1000 }, (err) => {
              if (err) console.log('Error:', err.message)
              else console.log('Done.\n', 'FMS bundled!!!')
            })
          }
        }
      })
    }
  })
}

let configName = process.argv[2]
let flag = process.argv[3]
if (!configName) {
  console.log('configuration name missing')
} else {
  try {
    // read json configuration based on the configName
    let config = fs.readFileSync(`./configs/${configName}.json`, { encoding: 'utf8' })
    let generalConfig = fs.readFileSync('./configs/general.json', { encoding: 'utf8' })
    let configObj = JSON.parse(config)
    generalConfig = JSON.parse(generalConfig)

    configObj.baseUrl = `http://${generalConfig.ipAddress}/fms${generalConfig.appVersion}`
    configObj.userUrl = `http://${generalConfig.domainName}/fms${generalConfig.appVersion}/${configName}`
    configObj.appVersion = generalConfig.appVersion
    configObj.nodeServerPort = generalConfig.nodeServerPort

    // set active project config
    // generalConfig.activeConfig = `${configName}`

    // generate project config file
    fs.writeFileSync('./vue-client/src/config/index.js', `
export default {
  baseUrl: 'http://${generalConfig.ipAddress}/fms${generalConfig.appVersion}',
  socketUrl: 'http://${generalConfig.ipAddress}:${generalConfig.nodeServerPort}',
  rootDir: '/fms${generalConfig.appVersion}/${configName}',
  navyInfo: ${config}
}
`)
    // generate vue.js config file
    fs.writeFileSync('./vue-client/vue.config.js', `
module.exports = {
  publicPath: '/fms${generalConfig.appVersion}/${configName}/',
  outputDir: '../client'
}
`)

    // generate server .env File
    generateEnvFile(configName, configObj)

    // generate html index redirect page for root folder
    generateIndexPage(configName, configObj)

    console.log('PROJECT CONFIG:', config)

    fs.stat(`./${configName}`, (err, stat) => {
      if (err) {
        console.log('Error-fs.stat:', err.message)
        runBuild(configObj)
      } else {
        if (stat && stat.isDirectory()) {
          exec(`rm -r ./${configName}`, (error) => {
            if (error) {
              console.log('Error:', error.message)
            }
            runBuild(configObj)
          })
        } else {
          runBuild(configObj)
        }
      }
    })
  } catch (err) {
    console.log('Error:', err.message)
  }
}
