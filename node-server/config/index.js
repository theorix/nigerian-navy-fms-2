const path = require('path')
const dotenv = require('dotenv')

dotenv.config(
  {
    path: path.join(__dirname, '../.env')
  }
)

module.exports = {
  appName: 'fms' + process.env.APP_VERSION,
  server: {
    port: Number.parseInt(process.env.SERVER_PORT),
    pfsenseName: process.env.PFSENSE_NAME,
    serverPassword: process.env.SERVER_PASSWORD,
    signalUploadDir: process.env.SIGNAL_UPLOAD_DIR,
    memoUploadDir: process.env.MEMO_UPLOAD_DIR,
    archiveRootURL: process.env.ARCHIVE_ROOT_URL,
    signatureDir: process.env.SIGNATURE_DIR,
    profilePictureDir: process.env.PROFILE_PICTURE_DIR,
    outgoingDocsDir: process.env.OUTGOING_DOCS_DIR,
    draftFinalDocsDir: process.env.DRAFT_FINAL_DOCS_DIR
  },

  client: {
    memoDir: process.env.CLIENT_MEMO_DIR,
    archiveDir: process.env.CLIENT_ARCHIVE_DIR,
    signalDir: process.env.CLIENT_SIGNAL_DIR,
    signatureDir: process.env.CLIENT_SIGNATURE_DIR,
    profilePictureDir: process.env.CLIENT_PROFILE_PICTURE_DIR,
    outgoingDocsDir: process.env.CLIENT_OUTGOING_DOCS_DIR,
    draftFinalDocsDir: process.env.CLIENT_DRAFT_FINAL_DOCS_DIR
  },

  db: {
    driver: process.env.DB_DRIVER,
    mysql: {
      host: process.env.MYSQL_HOST,
      user: process.env.MYSQL_USER,
      password: process.env.MYSQL_PASSWORD,
      database: process.env.MYSQL_DATABASE
    }
  },
  debugMode: parseInt(process.env.DEBUG_MODE)
}
