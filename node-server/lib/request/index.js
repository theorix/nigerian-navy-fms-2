module.exports = {
  respond: (res, httpStatus, msg, data = null) => {
    res.status(httpStatus).send({
      status: httpStatus,
      message: msg,
      data: data
    })
  },
  apiRespond: (req, res, httpStatus, msg, data = null) => {
    res.status(httpStatus).send({
      success: !msg,
      message: msg,
      request: req.originalUrl,
      result: data
    })
  }
}
