// database helper functions

const dbFunctions = {
  toDataArray: function (data) {
    let dataArray = []
    for (let k in data) {
      let d = {}
      d[k] = data[k]
      dataArray.push(d)
    }

    return dataArray
  }
}

module.exports = dbFunctions
