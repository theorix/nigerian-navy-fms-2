const appConfig = require('../../config')
const Database = require(`../../database/drivers/${appConfig.db.driver}`)
const db = new Database()

db.connect()

module.exports = db
