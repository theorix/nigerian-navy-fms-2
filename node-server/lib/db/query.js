let dbHelper = require('./helper')
const mysql = require('mysql')

/**
  * @param {String} token => any of the supported operators
  * The function check whether or not the suplied string is an operator.
  * It returns a boolen value.
*/
function isOperator (token) {
  if (typeof token !== 'string') return 0

  switch (token) {
    case '$and':
      return 1
    case '$or':
      return 2
    case '$lt':
      return 3
    case '$lte':
      return 4
    case '$gt':
      return 5
    case '$gte':
      return 6
    case '$ne':
      return 7
  }

  // check for $or-*
  if (token[0] === '$' && token[1] === 'o' && token[2] === 'r' && token[3] === '-') {
    return 2
  }

  // check for $and-*
  if (token[0] === '$' && token[1] === 'a' && token[2] === 'n' && token[3] === 'd' && token[4] === '-') {
    return 1
  }

  return 0
}

/**
  * @param {Object} obj => { key: value } OR { key: { opKey: value }}
  * opKey => $lt, $gt, $lte ... etc are the supported operators
  * value => string or number
  * This simply interpretes what kind of object passed to it.
  * For example, if given { book: 'Javascript' } it will interprete as: book = Javascript
*/
function decodeOp (obj) {
  let op, lvalue, rvalue, t
  for (let key in obj) {
    lvalue = key
    t = obj[key]
    break
  }
  if (typeof t === 'object' && t !== null) {
    for (let key in t) {
      op = key
      rvalue = t[op]
    }
  } else if (typeof t === 'number' || typeof t === 'string') {
    rvalue = t
  } else return ''

  switch (op) {
    case '$lt':
      return `${lvalue} < ${mysql.escape(rvalue)}`
    case '$lte':
      return `${lvalue} <= ${mysql.escape(rvalue)}`
    case '$gt':
      return `${lvalue} > ${mysql.escape(rvalue)}`
    case '$gte':
      return `${lvalue} >= ${mysql.escape(rvalue)}`
    case '$ne':
      return `${lvalue} <> ${mysql.escape(rvalue)}`
    default:
      return `${lvalue} = ${mysql.escape(rvalue)}`
  }
}

/**
 * @param  {Object} obj => { key: value } OR { key: { opKey: value }}
 * This module uses stack to store it's internal state hence,
 * this function is responsible for creating such a state.
 * The function returns an object of the form:
 * {
 *  A: [],
 *  i: 0,
 *  length: A.length,
 *  operator: 'OR' || 'AND' || 'comparison'
 * }
 *
 * The comparison operator is one of: $lt, $gt, $lte, $gt4 etc...
 */
function createState (obj) {
  let A = []
  let key
  let value
  let operator

  if (typeof obj === 'object') {
    A = dbHelper.toDataArray(obj)
  } else return null

  if (A.length === 1) {
    for (let k in obj) {
      key = k
      value = obj[key]
    }
  }

  const oprator = isOperator(key)

  if ((A.length > 1 && !Array.isArray(value)) || (A.length === 1 && Array.isArray(value) && oprator === 1)) {
    if ((A.length === 1 && Array.isArray(value) && oprator === 1)) A = obj[key]
    operator = 'AND'
  } else if (A.length === 1 && Array.isArray(value) && oprator === 2) {
    operator = 'OR'
    A = obj[key]
  } else if (A.length === 1 && !oprator && (typeof value === 'string' || typeof value === 'number' || value === undefined || value === null)) {
    operator = 'comparison'
  } else if (A.length === 1 && !oprator && typeof value === 'object') {
    operator = 'comparison'
  } else {
    return null
  }

  return {
    A: A,
    i: 0,
    length: A.length,
    operator: operator
  }
}

/**
 * @param  {Object} query => for example: {quantity: {$lte: '3'}, price: {$gt: '7000'}, id:65}
 *
 * This is the method that actually does the compilation of query object to SQL WHERE clause.
 * It actully loops through all the states in the stack and simply creates a STATE by using the
 * createState() function or generates a string using the decodeOp() function as stipulated
 * by the current operator it is working with.
 *
 * In particular, if the operator is AND or OR, the function creates a state,
 * otherwise it generates a string.
 */
function compileQuery (query) {
  let opStack = []
  let strStack = []
  let queryString

  // check whether there is an $ext property
  let ext
  if (query.$ext) {
    ext = query.$ext
    delete query['$ext']
  }

  let items = 0
  let s = createState(query)

  if (s !== null) {
    let currentOp = s.operator

    for (let i = s.i; i < s.length; i++) {
      if (currentOp === 'AND' || currentOp === 'OR') {
        // save current state on the stack
        s.i = i
        s.items = items
        opStack.push(s)

        s = createState(s.A[i])
        currentOp = s.operator
        i = s.i - 1
        items = 0
      } else {
        // generate string
        const decoded = decodeOp(s.A[i])
        if (decoded) {
          strStack.push(decoded)
          ++items
        }
      }

      if (i === (s.length - 1)) {
        do {
          s = opStack.pop()
          if (!s) break
          i = s.i
          let it = s.items
          currentOp = s.operator
          if (currentOp === 'AND' || currentOp === 'OR') {
            let single = true
            let tmp
            let n = 0
            tmp = strStack.pop()

            while (strStack.length > 0 && it > 0) {
              if (++n === 1) {
                single = false
              }

              tmp += ` ${currentOp}  ${strStack.pop()}`
              it--
            }

            if (!single) {
              strStack.push(`(${tmp})`)
            } else strStack.push(tmp)
          }
        } while (i === (s.length - 1) && opStack.length > 0)
      }
      if (!s) break
    }

    queryString = strStack.pop()
  } else {
    queryString = null
  }

  // add the extension to the query string
  if (ext && queryString) {
    let extString = ''
    if (ext['$orderby']) {
      const asc = ext['$orderby']['order'] === 'ASC' ? 'ASC' : 'DESC'
      extString = ` ORDER BY ${ext['$orderby']['key']} ${asc} `
    }
    if (ext['$limit']) {
      extString += ` LIMIT ${ext['$limit']} `
    }
    return (queryString || '1 = 1') + extString
  } else return queryString || '1 = 1'
}

module.exports.compileKey = compileQuery

/**
****************** Examples (Do not delete!!!) *******************************
//    SIMPLE QUERY
console.log('Test1:', compileQuery({quantity: {$lte: '3'}, price: {$gt: '7000'}, id:65}))
//    OUTPUT
// (id = '65' AND (price > '7000' AND quantity <= '3'))

//  SLIGHTLY COMPLEX QUERY
console.log('Test2:',compileQuery(
  {
    "$or-0": [{fee: {$lte:9.053}}, {fee: {$gte: 15.432}}],
    "$or-1": [{price: {$lte:1}}, {amount: {$gt: 7}}, {FirstName:'Israel'}],
    "$or-2": [{id: {$lt:1}}, {id: {$gt: 7}}, {id:9}],
    "Adam":"Eve"
  }
))
//  OUTPUT
// (Adam = 'Eve' AND ((id = '9' OR (id > '7' OR id < '1')) AND ((FirstName = 'Israel' OR (amount > '7' OR price <= '1')) AND (fee >= '15.432' OR fee <= '9.053'))))

//  A LITTLE MORE COMPLEX QUERY
console.log('Test3:',compileQuery(
  {
    "$or-0": [{fee: {$lte:9.053}}, {fee: {$gte: 15.432}}],
    "$or-1": [{price: {$lte:1}}, {amount: {$gt: 7}}, {FirstName:'Israel'}],
    "$or-2": [{id: {$lt:1}}, {id: {$gt: 7}}, {id:9}],
    "bread":"tea",
    "$or-3": [{bmi: {$lt:4.5}}, {weight: {$gt: 90}}, {id:9, money:5000, grant: {$lte: 40}}]
  }
))
//  OUTPUT
// Test3: (((grant <= '40' AND (money = '5000' AND id = '9')) OR (weight > '90' OR bmi < '4.5')) AND (bread = 'tea' AND ((id = '9' OR (id > '7' OR id < '1')) AND ((FirstName = 'Israel' OR (amount > '7' OR price <= '1')) AND (fee >= '15.432' OR fee <= '9.053')))))

// Test with LIMIT and ORDER BY clause
console.log("Test 5:", compileQuery({
    age: {$lt: 5},
    $ext: {
      $limit: 3,
      $orderby: { key: 'name', order: 'ASC'}
    }
  }))

// Test with $and operator
  console.log(
    compileQuery(
      { $and: [
        { age: { $gt: 4 } },
        { age: { $lt: 7 } }
      ],
      $or: [
        { a: 6 },
        { c: 5 },
        { d: 8 }
      ]
      }
    )
  )

// OUTPUT: ((d = 8 OR  (c = 5 OR  a = 6)) AND  (age < 7 AND  age > 4))

  console.log(
    compileQuery(
      { $and: [
        { age: { $gt: 4 } },
        { age: { $lt: 7 } }
      ],

      '$and-1': [
        { a: 6 },
        { c: 5 },
        { d: 8 }
      ]
      }
    )
  )
// OUTPUT: ((d = 8 AND  (c = 5 AND  a = 6)) AND  (age < 7 AND  age > 4))
*/
