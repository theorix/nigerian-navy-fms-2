const appConfig = require('../config')
const defaultFormatZoned = 'YYYY-MM-DD HH:mm:ss'
const defaultFormatUtc = 'YYYY-MM-DD HH:mm:ss'

function nowUtc (format = null) {
  const utcMoment = require('moment').utc()

  return format
    ? utcMoment.format(format)
    : utcMoment.format(defaultFormatUtc)
}

function fromStringUtc (timeString, format = null) {
  const moment = require('moment')

  return format
    ? moment(timeString).utc().format(format)
    : moment(timeString).utc().format(defaultFormatUtc)
}

function fromString (timeString, format = null) {
  const moment = require('moment')

  return format
    ? moment(timeString).format(format)
    : moment(timeString).format(defaultFormatZoned)
}

function fromStringZoned (timeString, format = null) {
  const zonedMoment = require('moment-timezone')
  zonedMoment.tz.setDefault(appConfig.time.zoneString)

  return format
    ? zonedMoment(timeString).format(format)
    : zonedMoment(timeString).format(defaultFormatZoned)
}

function futureUtc (duration, unit, format = null) {
  const utcMoment = require('moment').utc()

  return format
    ? utcMoment.add(duration, unit).format(format)
    : utcMoment.add(duration, unit).format(defaultFormatUtc)
}

function pastUtc (duration, unit, format = null) {
  const utcMoment = require('moment').utc()

  return format
    ? utcMoment.subtract(duration, unit).format(format)
    : utcMoment.subtract(duration, unit).format(defaultFormatUtc)
}

module.exports = {
  nowUtc,
  fromStringUtc,
  fromString,
  fromStringZoned,
  futureUtc,
  pastUtc,
  moment: require('moment')
}
