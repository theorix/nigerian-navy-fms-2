const appConfig = require('../../config')
const swig = require('swig-templates')
const Mailer = require('./Mailer')

const mailer = new Mailer({
  sender: `"${appConfig.name}" <${appConfig.email.noReply}>`
})

function sendByTemplate (recipients, subject, templateName, templateVars) {
  const template = swig.compileFile(`${__dirname}/resources/templates/${templateName}.html`)
  const compiledBody = template(templateVars)

  // TODO: try/catch and return true/false
  mailer.send(recipients.join(','), subject, compiledBody)
}

function send (recipients, subject, templateName, templateVars) {
  const template = swig.compileFile(`${__dirname}/resources/templates/${templateName}.html`)
  const compiledBody = template(templateVars)

  // TODO: try/catch and return true/false
  mailer.send(recipients.join(','), subject, compiledBody)
}

module.exports.send = send
module.exports.sendByTemplate = sendByTemplate
