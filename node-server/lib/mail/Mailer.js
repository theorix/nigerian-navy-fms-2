const nodemailer = require('nodemailer')
const appConfig = require('../../config')
const errorHandler = new (require('../error/Error'))('Mailer')

// create reusable transporter object using the default SMTP transport
const transporter = nodemailer.createTransport({
  host: appConfig.mailer.host,
  port: appConfig.mailer.port,
  secure: appConfig.mailer.ssl,
  auth: {
    user: appConfig.mailer.user,
    pass: appConfig.mailer.password
  },
  tls: {
    // do not fail on invalid certs
    rejectUnauthorized: false
  }
})

class Mailer {
  constructor (options) {
    this.sender = options.sender
  }

  send (recipients, subject, body) {
    // generate the mail options
    let mailOptions = {
      from: this.sender, // sender address
      to: recipients, // list of receivers
      subject: subject, // Subject line
      html: body
    }

    return new Promise((resolve, reject) => {
      // verify connection configuration
      transporter.verify(function (error, success) {
        if (error) {
          errorHandler.log(error)
          resolve(false)
        } else {
          // send mail with defined transport object
          transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
              errorHandler.log(error)
              resolve(false)
            }
            resolve(true)
          })
        }
      })
    })
  }
}

module.exports = Mailer
