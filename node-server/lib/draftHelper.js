const Model = require('../database/models')
const config = require('../config')
const path = require('path')

async function getDraft (where) {
  return new Promise(async (resolve, reject) => {
    let draft = ''
    let d = await Model.Draft.fetch(where)

    if (d.length > 0) {
      d = d[0]
      let user = await Model.User.Query(`${Model.queries.usersQuery} WHERE users.id = "${d.user_id}"`)
      user = user.results[0]

      const p = path.resolve(config.server.draftFinalDocsDir).split(config.appName)
      let draftRoot
      if (d.type !== 'formal') draftRoot = `${p[1]}/USER/${user.appointment}/draft_${d.id}`
      else draftRoot = `${p[1]}/ARCHIVE/draft_${d.id}`

      // let draftPath = `${config.server.draftFinalDocsDir}/USER/${user.appointment}/draft_${d.id}`
      d['finalCopy'] = d.final_copy ? `${draftRoot}/${d.id}.pdf` : ''
      d['extra_info'] = d.extra_info ? JSON.parse(d.extra_info) : ''
      let v = await Model.DraftVersions.fetch({ draft_id: d.id })

      let mappedV = []
      for (let i of v) {
        i.tags = JSON.parse(i.tags)
        i.docUrl = `${draftRoot}/version_${i.version_number}/document_${d.file_id}.odt`
        i.tagUrl = `${draftRoot}/version_${i.version_number}/tagged_document_${d.file_id}.odt`
        // i.document = await docManager.convertTo('html', `${draftPath}/version_${i.version_number}/document_${d.file_id}.odt`, true)
        // if (i.tagged_document) {
        //   i.tagged_document = await docManager.readFile(`${draftPath}/version_${i.version_number}/tagged_document_${d.file_id}.html`, 'utf8')
        // }
        mappedV.push(i)
      }

      draft = {
        info: d,
        versions: mappedV
      }
      resolve(draft)
    } else resolve(false)
  })
}

module.exports = {
  getDraftById: function (draftId) {
    return getDraft({ id: draftId })
  },
  getDraftByFileId: function (fileId) {
    return getDraft({ file_id: fileId })
  }
}
