const Scheduler = require('./Scheduler')

const scheduler = new Scheduler()

// scheduler.run('npnrTask', {
//   task: function (someInput) {
//     console.log('Runing task with:', someInput)
//   },
//   input: 'INPUT DATA',
//   duration: 60000
// }, (err, tId) => {
//   if (err) throw err
//   console.log('Task Id:', tId, 'Scheduled from begin Time: 1 minute')

//   scheduler.run('nprTask', {
//     task: function (someInput) {
//       console.log('Runing task with:', someInput)
//     },
//     input: 'INPUT DATA 2',
//     duration: 60000
//   }, (err, tId) => {
//     if (err) throw err
//     console.log('Task Id:', tId, 'Scheduled to run every 1 minute')
//   })
// })

// scheduler.schedule({
//   task: function (someInput) {
//     console.log('Runing task with:', someInput)
//   },
//   input: 'HELLO SCHEDULER!!!',
//   beginTime: 0,
//   interval: 5000,
//   duration: 60000
// }, (err, tId) => {
//   if (err) throw err
//   console.log('Done scheduling with Id:', tId)
// })

module.exports = scheduler
