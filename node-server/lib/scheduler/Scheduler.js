const logger = require('../log')
const EventEmitter = require('events').EventEmitter

function Scheduler () {
  let tasks = {}
  let taskId = 0
  const eventBus = new EventEmitter()
  let that = this

  // expose the event system
  this.on = function (eventName, callback) {
    eventBus.on(eventName, callback)
  }
  this.emit = function (eventName, data) {
    eventBus.emit(eventName, data)
  }

  this.getScheduledTasks = function () {
    let T = []
    for (let t in tasks) {
      T.push({
        tId: t,
        schedulerType: tasks[t].schedularType
      })
    }
    return T
  }

  this.run = function (schedulerType, taskData, callback) {
    if (that[schedulerType]) {
      if (typeof taskData === 'object' && typeof taskData.task === 'function') {
        // call the required scheduler to run the task.
        const id = that[schedulerType](taskData.duration, taskData.task, taskData.input)
        tasks[++taskId] = {
          schedulerType,
          task: taskData.task,
          tId: id
        }
        if (callback) {
          callback(null, taskId)
        }
      } else {
        if (callback) {
          that.emit('error', new Error('Bad argument'))
          callback(new Error('Bad argument'))
        }
      }
    } else {
      if (callback) {
        that.emit('error', new Error('Invalid Scheduler'))
        callback(new Error('Invalid Scheduler'))
      }
    }
  }

  this.cancel = function (runId) {
    const t = tasks[runId]
    if (!t) return
    switch (t.schedulerType) {
      case 'nprTask':
        clearInterval(t.tId)
        delete tasks[runId]
        logger.info(`nprTask with Id ${runId}, Canceled`)
        break
      case 'npnrTask':
        clearTimeout(t.tId)
        delete tasks[runId]
        logger.info(`npnrTask with Id ${runId}, Canceled`)
        break
    }
    that.emit('cancel', runId)
  }

  this.schedule = function (taskData, callback) {
    let beginTime = 0
    if (!taskData || typeof taskData !== 'object') return callback(new Error('Invalid task data'))
    if (typeof taskData.task !== 'function') return callback(new Error('No task specified'))
    if (isNaN(taskData.duration)) return callback(new Error('Invalid task duration'))
    if (isNaN(taskData.interval) || taskData.interval <= 0) taskData.interval = 1 // defaults to 1 minute
    if (isNaN(taskData.beginTime) || taskData.beginTime < 0) beginTime = 0 // defaults to 0 minute

    this.run('npnrTask', {
      task: function (scheduleData) {
        // the task inside the nprTask runner uses closure on scheduleData.n
        that.run('nprTask', {
          task: function (tData) {
            if (++scheduleData.n <= scheduleData.N) {
              // schedule the main task here
              logger.warn(`task ${scheduleData.tId}: ${scheduleData.n} of ${scheduleData.N}`)
              that.run('npnrTask', {
                task: scheduleData.mainTask,
                input: scheduleData.mainInput,
                duration: 0
              })
            } else {
              // cancel / end task
              logger.warn('Schedule ended with task ID:', `end-${scheduleData.tId}`)
              that.emit(`end-${scheduleData.tId}`, scheduleData.tId)
              that.cancel(scheduleData.tId)
            }
          },
          input: { // this input will be passed to task as tData

          },
          duration: scheduleData.interval
        }, (err, tId) => {
          if (err) throw err
          else {
            // again user closure on scheduleData
            scheduleData.tId = tId
            if (callback) callback(null, tId)
          }
        })
      },
      input: { // this input will be passed to task as scheduleData
        interval: taskData.interval,
        n: 0, // number of times task was executed
        N: Math.floor(taskData.duration / taskData.interval), // number of times task will run within given duration
        mainTask: taskData.task,
        mainInput: taskData.input,
        tId: 0
      },
      duration: beginTime
    }, (err, tId) => {
      if (err) throw err
      logger.info(`Task set scheduled with ID ${tId}`)
    })
  }

  this.prTask = function (task, input) {

  }

  this.nprTask = function (duration, task, input) {
    let runId = 0
    if (input) {
      runId = setInterval(task, duration, input)
      that.emit('scheduled', taskId)
      return runId
    } else {
      runId = setInterval(task, duration)
      return runId
    }
  }

  this.npnrTask = function (duration, task, input) {
    let runId = 0
    if (input) {
      runId = setTimeout(task, duration, input)
      that.emit('scheduled', taskId)
      return runId
    } else {
      runId = setTimeout(task, duration)
      that.emit('scheduled', taskId)
      return runId
    }
  }
}

module.exports = Scheduler
