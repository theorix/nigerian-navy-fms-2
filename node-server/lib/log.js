const chalk = require('chalk')
const appConfig = require('../config')

module.exports = {
  success: function (...args) {
    if (appConfig.debugMode) { console.warn(chalk.greenBright.bgBlack(...args)) }
  },

  info: function (...args) {
    if (appConfig.debugMode) { console.warn(chalk.blueBright.bgBlack(...args)) }
  },

  warn: function (...args) {
    if (appConfig.debugMode) { console.warn(chalk.yellowBright.bgBlack(...args)) }
  },

  error: function (...args) {
    if (appConfig.debugMode) { console.warn(chalk.redBright.bgBlack(...args)) }
  },

  neutral: function (...args) {
    if (appConfig.debugMode) { console.warn(chalk.whiteBright.bgBlack(...args)) }
  },

  inverted: function (...args) {
    if (appConfig.debugMode) { console.warn(chalk.black.bgWhite(...args)) }
  }
}
