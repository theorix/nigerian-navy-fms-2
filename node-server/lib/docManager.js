// Wrapper module unoconv, convert supported office files to html
const fs = require('fs')
const path = require('path')
var exec = require('child_process').exec

module.exports = {
  convertTo: function (fileType, filename, convertAndDelete = false) {
    return new Promise((resolve, reject) => {
      exec(`unoconv -f ${fileType} "${filename}"`, function (error, stdout, stderr) {
        if (error) return reject(error)

        const ext = path.extname(filename)
        const basename = path.basename(filename, ext)
        const directoryName = path.dirname(filename)
        const filePath = path.join(directoryName, `${basename}.${fileType}`)
        let readType = 'utf8'
        if (fileType !== 'html') readType = null
        fs.readFile(filePath, readType, (err, data) => {
          if (err) return reject(err)
          // delete file
          if (convertAndDelete) fs.unlinkSync(filePath)
          return resolve(data)
        })
      })
    })
  },

  saveFile: function (fullPath, content) {
    return new Promise((resolve, reject) => {
      if (!fullPath) return reject(new Error('File path cannot be empty'))
      fs.writeFile(fullPath, content, function (err) {
        if (err) reject(err)
        else resolve(true)
      })
    })
  },

  readFile: function (fullPath, readType, readAndDelete = false) {
    return new Promise((resolve, reject) => {
      fs.readFile(fullPath, readType, (err, data) => {
        if (err) return reject(err)
        // delete file
        if (readAndDelete) fs.unlinkSync(fullPath)
        return resolve(data)
      })
    })
  }
}
