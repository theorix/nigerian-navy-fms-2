const Model = require('../database/models')
const Inbox = Model.Inbox
const Recipients = Model.Recipients
const logger = require('../lib/log')
const cacheHelper = require('../lib/cacheHelper')
const scheduler = require('../lib/scheduler')

const onlineUsers = []

function createThread (data) {
  // write the thread info to table and return the insert id
  logger.info('threadData:', data)
  return data.folio_number
}

async function updateUserCache (userId, amount = 1) {
  // update the cache
  const cache = await cacheHelper.getCache(userId)
  if (cache) {
    cache['inboxCount']['cRegular'] += amount
    cache['inboxCount']['cRegularUnread'] += amount
    return cacheHelper.updateCache(userId, cache, true)
  }
}

function socketHandler (clientSocket) {
  logger.success('socket connected', clientSocket.handshake.query.email)
  onlineUsers[clientSocket.handshake.query.email] = clientSocket

  cacheHelper.init(clientSocket.handshake.query.email, (err) => {
    if (err) return logger.warn('Error while initializing Cache:', err.message)
    // notify client to count inbox from cache
    clientSocket.emit('message', {
      type: 'fetch-inbox-count'
    })
  })

  clientSocket.on('message', async function (data) {
    logger.info(data)
    try {
      let msg = JSON.parse(data)
      switch (msg.type) {
        case 'chat-message':
          // const sender = msg.content.sender
          // const receiver = msg.content.receiver
          // const groupId = msg.content.isGroup ? msg.content.groupId : 0
          // const message = msg.content.message
          break

        case 'draft':
          logger.info('draft message received!!')
          // add each recipients to draft recipients
          let draftId = msg.content.draftVersion.draft_id
          let draft = await Model.Draft.get({ id: draftId })
          draft = draft[0]
          let participants = JSON.parse(draft.participants)
          if (!participants[msg.content.sender.id]) {
            participants[msg.content.sender.id] = []
          }

          // add
          for (let recipient of msg.content.recipients) {
            if (participants[msg.content.sender.id].indexOf(msg.content.sender.id) === -1) {
              participants[msg.content.sender.id].push(recipient.id)
            }

            let seqId = await Model.DraftSequence.create({
              version_id: msg.content.draftVersion.id,
              sender_id: msg.content.sender.id,
              receiver_id: recipient.id
            })

            if (onlineUsers[recipient.email]) {
              logger.info('sending to', recipient.email)
              onlineUsers[recipient.email].emit('message', {
                type: 'draft',
                content: {
                  user_id: recipient.id,
                  sender: msg.content.sender,
                  receiver: recipient,
                  subject: draft.subject,
                  draftId,
                  draftType: draft.type,
                  fileId: draft.file_id,
                  draftVersion: msg.content.draftVersion,
                  id: seqId,
                  created_at: new Date().toDateString(),
                  commited: 0
                }
              })
            }

            // store participants
            draft.participants = JSON.stringify(participants)
            await draft.save()
          }
          break
        case 'thread':
          logger.info('thread recieved!!')
          const threadId = createThread(msg.content.file)
          logger.info(msg.content)
          if (threadId) {
            clientSocket.emit('message', {
              type: 'mail-sent',
              content: {
                message: 'Thread created!!!'
              }
            })

            // send an inbox message to users involved
            let count = 0
            for (let recipient of msg.content.recipients) {
              logger.info('email', recipient.email)
              count++
              let recp = await Recipients.get({
                user_id: recipient.id,
                file_id: msg.content.file.folio_number
              })
              if (recp.length === 0) {
                await Recipients.create({
                  user_id: recipient.id,
                  file_id: msg.content.file.folio_number,
                  color: msg.content.color
                })
              }

              // send to the first person only if msgType is queue
              if ((msg.content.msgType === '1' || msg.content.msgType === 1) || count === 1) {
                const inboxId = await Inbox.create(
                  {
                    title: msg.content.title,
                    user_id: recipient.id,
                    file_id: threadId,
                    sender_email: msg.content.sender_email,
                    text: msg.content.text
                  }
                )
                // update the cache
                await updateUserCache(recipient.id)

                if (onlineUsers[recipient.email]) {
                  logger.info('sending to', recipient.email)
                  onlineUsers[recipient.email].emit('message', {
                    type: 'inbox',
                    content: {
                      type: 0,
                      title: msg.content.title,
                      text: msg.content.text,
                      user_id: recipient.id,
                      file_id: threadId,
                      sender_email: msg.content.sender_email,
                      created_at: new Date().toDateString(),
                      commited: 0,
                      id: inboxId
                    }
                  })
                }
              }
            }
          }
          break

        case 'forward':
          Inbox.create(
            {
              title: msg.content.title,
              user_id: msg.recipient.id,
              file_id: msg.content.fileId,
              sender_email: msg.content.sender_email,
              text: msg.content.text
            }
          ).then(async (id) => {
            logger.success('done forwarding message')
            await updateUserCache(msg.recipient.id)

            if (onlineUsers[msg.recipient.email] && id) {
              onlineUsers[msg.recipient.email].emit('message', {
                type: 'inbox',
                content: {
                  type: 0,
                  title: msg.content.title,
                  text: msg.content.text,
                  user_id: msg.recipient.id,
                  file_id: msg.content.fileId,
                  sender_email: msg.content.sender_email,
                  created_at: new Date().toDateString(),
                  commited: 0,
                  id
                }
              })
            }
          }).catch((err) => {
            console.log(err.message)
          })
          break

        case 'memo':
          logger.info('memo received:', msg)
          Inbox.create(
            {
              title: msg.content.title,
              user_id: msg.recipient.id,
              file_id: msg.content.memoId,
              sender_email: msg.content.sender_email,
              text: msg.content.text,
              type: 1
            }
          ).then(async (id) => {
            logger.success('done creating memo')
            await updateUserCache(msg.recipient.id)

            if (onlineUsers[msg.recipient.email] && id) {
              onlineUsers[msg.recipient.email].emit('message', {
                type: 'inbox',
                content: {
                  title: msg.content.title,
                  text: msg.content.text,
                  user_id: msg.recipient.id,
                  file_id: msg.content.memoId,
                  sender_email: msg.content.sender_email,
                  created_at: new Date().toDateString(),
                  commited: 0,
                  id,
                  type: 1
                }
              })
            }
          }).catch((err) => {
            console.log(err.message)
          })
          break

        case 'print-task':
          logger.info('print-task received:', msg)
          Inbox.create(
            {
              title: msg.content.title,
              user_id: msg.recipient.id,
              file_id: msg.content.fileId,
              sender_email: msg.content.sender_email,
              text: msg.content.text,
              type: msg.content.type
            }
          ).then(async (id) => {
            await updateUserCache(msg.recipient.id)

            if (onlineUsers[msg.recipient.email] && id) {
              onlineUsers[msg.recipient.email].emit('message', {
                type: 'inbox',
                content: {
                  title: msg.content.title,
                  text: msg.content.text,
                  user_id: msg.recipient.id,
                  file_id: msg.content.fileId,
                  sender_email: msg.content.sender_email,
                  created_at: new Date().toDateString(),
                  commited: 0,
                  id,
                  type: msg.content.type
                }
              })
            }
          }).catch((err) => {
            console.log(err.message)
          })
          break

        case 'scheduled-notifications':
          clientSocket.emit('message', {
            type: 'scheduled-notifications',
            list: scheduler.getScheduledTasks()
          })
          break
      }
    } catch (err) {
      console.log(err.message)
    }
  })

  clientSocket.on('disconnect', () => {
    logger.info('socket', clientSocket.id, 'diconnected!!!')
    for (let socket in onlineUsers) {
      // console.log(socket)
      if (onlineUsers[socket] === clientSocket) {
        clientSocket.off('connection', socketHandler)
        delete onlineUsers[socket]
      }
    }
  })
}

module.exports = {
  handler: socketHandler,
  onlineUsers: onlineUsers
}
