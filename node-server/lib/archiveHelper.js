const fs = require('fs')
const Path = require('path')
const logger = require('./log')

module.exports = {
  isDirectory: function (path) {
    return new Promise((resolve, reject) => {
      fs.stat(path, (err, stats) => {
        if (err) return reject(err)
        return resolve(stats.isDirectory())
      })
    })
  },

  pathExists: function (path, create = false) {
    return new Promise((resolve, reject) => {
      fs.stat(path, function (err, stats) {
        // Check if error defined and the error code is "not exists"
        if (err && err.code === 'ENOENT') {
          // Create the directory
          if (create) {
            fs.mkdir(path, (err) => {
              if (err) return reject(err)
              resolve(true)
            })
          } else {
            resolve(false)
          }
        } else {
          // just in case there was a different error:
          if (err) {
            return reject(err)
          }
          resolve(true)
        }
      })
    })
  },

  readDir: function (path) {
    return new Promise(async (resolve, reject) => {
      fs.stat(path, (err, stats) => {
        if (err) return resolve([])
        if (stats.isDirectory()) {
          fs.readdir(path, function (err, items) {
            if (err) return reject(err)
            return resolve(items)
          })
        } else {
          return resolve([])
        }
      })
    })
  },

  moveFile: function (filePath, dir, newName = '') {
    // get basename
    const fname = Path.basename(filePath)
    let dest = ''
    if (newName) {
      dest = Path.resolve(dir, `${newName}`)
    } else {
      dest = Path.resolve(dir, fname)
    }

    return new Promise((resolve, reject) => {
      fs.rename(filePath, dest, (err) => {
        if (err) reject(err)
        else {
          logger.info('File Moved successfull!!!')
          resolve(true)
        }
      })
    })
  },

  removeFile: function (filePath) {
    return new Promise((resolve, reject) => {
      fs.unlink(filePath, (err) => {
        if (err) return resolve(false)
        logger.info(`File: ${filePath} deleted!!!`)
        resolve(true)
      })
    })
  }
}
