const fs = require('fs')
const Path = require('path')
const logger = require('./log')
const archiveHelper = require('./archiveHelper')
const Model = require('../database/models')

const userCache = {}

const Inbox = Model.Inbox
let N = 15 // number of results per page

async function getUsersWithSameAppointment (userId) {
  let users
  let user = (await Model.User.get({
    id: userId
  }))[0]

  if (user) {
    users = await Model.User.get({
      appointment: user.appointment
    })
  }
  return users
}

async function countInboxMessages (data) {
  let usrs = await getUsersWithSameAppointment(data.user_id)
  let c = 0
  let C = 0
  for (let u of usrs) {
    data['user_id'] = u.id
    c = await Inbox.count(data)
    C += Number(c)
  }
  return C
}

module.exports = {
  init: async function (userEmail, callback) {
    const user = (await Model.User.fetch({ email: userEmail, active: 1 }))[0]
    const cachePath = `./userCache/${user.appointment}.json`
    // cache exists for this user?
    if (await archiveHelper.pathExists(cachePath)) {
      // load cache
      if (!userCache[user.id]) {
        fs.readFile(cachePath, (err, data) => {
          if (err) return callback(err)
          try {
            userCache[user.id] = JSON.parse(data)
          } catch (e) {
            logger.warn(`Error processing cache for ${user.email}`)
            // logger.warn(e.message)
          }
          callback()
          logger.info('cache loaded from file:', data)
        })
      } else {
        callback()
      }
    } else {
      // create cache
      let cacheData = {
        'inboxCount': {}
      }

      // >>>>>>>>>>>>>>>>>> get inbox counts >>>>>>>>>>>>>>>>>>>
      cacheData['inboxCount']['cRegular'] = await countInboxMessages({
        user_id: user.id,
        trashed: 0,
        $or: [{ type: 0 }, { type: 1 }]
      })

      cacheData['inboxCount']['cRegularUnread'] = await countInboxMessages({
        user_id: user.id,
        commited: 0,
        trashed: 0,
        $or: [{ type: 0 }, { type: 1 }]
      })

      cacheData['inboxCount']['cSignals'] = await countInboxMessages({
        user_id: user.id,
        trashed: 0,
        type: { $gt: 0 }
      })

      cacheData['inboxCount']['cSignalsUnread'] = await countInboxMessages({
        user_id: user.id,
        commited: 0,
        trashed: 0,
        type: { $gt: 0 }
      })

      cacheData['inboxCount']['cIncoming'] = await countInboxMessages({
        user_id: user.id,
        trashed: 0,
        type: 2
      })

      cacheData['inboxCount']['cIncomingUnread'] = await countInboxMessages({
        user_id: user.id,
        commited: 0,
        trashed: 0,
        type: 2
      })

      cacheData['inboxCount']['cOutgoing'] = await countInboxMessages({
        user_id: user.id,
        trashed: 0,
        type: 3
      })

      cacheData['inboxCount']['cOutgoingUnread'] = await countInboxMessages({
        user_id: user.id,
        commited: 0,
        trashed: 0,
        type: 3
      })

      cacheData['inboxCount']['cNg'] = await countInboxMessages({
        user_id: user.id,
        trashed: 0,
        type: 4
      })

      cacheData['inboxCount']['cNgUnread'] = await countInboxMessages({
        user_id: user.id,
        commited: 0,
        trashed: 0,
        type: 4
      })

      cacheData['inboxCount']['cNhq'] = await countInboxMessages({
        user_id: user.id,
        trashed: 0,
        type: 5
      })

      cacheData['inboxCount']['cNhqUnread'] = await countInboxMessages({
        user_id: user.id,
        commited: 0,
        trashed: 0,
        type: 5
      })

      cacheData['inboxCount']['cTrashed'] = await countInboxMessages({
        user_id: user.id,
        trashed: 1
      })

      cacheData['inboxCount']['cTrashedUnread'] = await countInboxMessages({
        user_id: user.id,
        commited: 0,
        trashed: 1
      })

      // add to cache
      userCache[user.id] = cacheData

      // persist cache
      fs.writeFile(cachePath, JSON.stringify(cacheData), (err) => {
        if (err) callback(err)
        callback()

        logger.info('cache writen to file:', JSON.stringify(cacheData))
      })
    }
  },

  getCache: function (userId) {
    return new Promise(async (resolve, reject) => {
      if (userCache[userId]) return resolve(userCache[userId])
      else {
        const user = (await Model.User.fetch({ id: userId }))[0]
        const cachePath = `./userCache/${user.appointment}.json`
        // cache exists for this user?
        if (await archiveHelper.pathExists(cachePath)) {
          fs.readFile(cachePath, (err, data) => {
            if (err) return reject(err)
            userCache[userId] = JSON.parse(data)
            resolve(userCache[userId])
            logger.info('cache loaded from file:', data)
          })
        } else resolve(false)
      }
    })
  },

  updateCache: function (userId, data, persist = false) {
    return new Promise(async (resolve, reject) => {
      userCache[userId] = data
      if (persist) {
        const tmpData = userCache[userId]
        delete tmpData.temp // do not persist temporary cache data

        // persist cache
        const user = (await Model.User.fetch({ id: userId }))[0]
        const cachePath = `./userCache/${user.appointment}.json`
        fs.writeFile(cachePath, JSON.stringify(tmpData), (err) => {
          if (err) return reject(err)
          resolve(true)
          logger.info('cache writen to file:', JSON.stringify(tmpData))
        })
      }
    })
  },

  removeCache: function (userId, recreate = false) {
    let that = this
    return new Promise(async (resolve, reject) => {
      // persist cache
      const user = (await Model.User.fetch({ id: userId }))[0]
      const cachePath = `./userCache/${user.appointment}.json`
      if (await archiveHelper.removeFile(cachePath)) {
        if (userCache[userId]) delete userCache[userId]
        resolve(true)
      } else resolve(false)

      if (recreate) {
        await that.init(user.email, (err) => {
          if (err) return logger.warn('Error while initializing Cache:', err.message)
          // notify client to count inbox from cache
          const socketHandler = require('./socketHandler')
          console.log('socketHandler:', socketHandler)
          if (socketHandler.onlineUsers[user.email]) {
            socketHandler.onlineUsers[user.email].emit('message', {
              type: 'fetch-inbox-count'
            })
          }
        })
      }
    })
  },

  getUsersWithSameAppointment
}
