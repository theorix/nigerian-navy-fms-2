// Require child_process
const exec = require('child_process').exec
const config = require('../config')

// Create shutdown function
function shutdown (callback) {
  // stop pfsense and then the server
  const pfsenseName = config.server.pfsenseName
  exec(`VBoxManage controlvm "${pfsenseName}" poweroff`, function (error, stdout, stderr) {
    if (error) return callback(error)
    exec(`echo "${config.server.serverPassword}" | sudo -S shutdown now`, function (error, stdout, stderr) {
      if (error) return callback(error)
      callback(null, stdout)
    })
    console.log('server SHUTDOWN done!!!')
  })
}

// Create shutdown function
function reboot (callback) {
  // stop pfsense and then the server
  const pfsenseName = config.server.pfsenseName
  exec(`VBoxManage controlvm "${pfsenseName}" poweroff`, function (error, stdout, stderr) {
    if (error) return callback(error)
    exec(`echo "${config.server.serverPassword}" | sudo -S shutdown -r now`, function (error, stdout, stderr) {
      if (error) return callback(error)
      callback(null, stdout)
    })
    console.log('server SHUTDOWN done!!!')
  })
}

function startPfsense (callback) {
  const pfsenseName = config.server.pfsenseName
  exec(`VBoxManage startvm "${pfsenseName}"`, function (error, stdout, stderr) {
    if (error) return callback(error)
    callback(null, stdout)
  })
}

module.exports = {
  shutdown,
  reboot,
  startPfsense
}
