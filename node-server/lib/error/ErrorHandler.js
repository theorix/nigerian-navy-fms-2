const appConfig = require('../../config')
const EventEmitter = require('events').EventEmitter
const log = require('../log')

class ErrorHandler extends EventEmitter {
  constructor (context = 'Global') {
    super()
    // context is a string representing the object to which this error is bound
    this.context = context
  }

  show (err, meta) {
    log.error('*******************************************')
    log.error('ORIGIN:', this.context)
    log.error('NAME:', err.name)
    log.error('CODE:', err.code)
    log.error('MESSAGE:', err.message)
    log.error('STACKTRACE:', err.stack)
    log.error('*******************************************')
  }

  log (err, meta = null) {
    if (appConfig.debug) {
      this.show(err, meta)
    }

    this.emit('error-log', err)
  }
}

module.exports = ErrorHandler
