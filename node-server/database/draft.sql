ALTER TABLE `draft` ADD `final_copy` VARCHAR(50) NULL AFTER `updated_at`, ADD `extra_info` VARCHAR(200) NULL AFTER `final_copy`;
ALTER TABLE `draft_versions` CHANGE `tagged_document` `tagged_document` TINYINT(1) NULL DEFAULT '0';
ALTER TABLE `draft_versions` CHANGE `document` `document` VARCHAR(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;
ALTER TABLE `draft` CHANGE `extra_info` `extra_info` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;