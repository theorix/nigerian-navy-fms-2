CREATE TABLE `fms_v2_db`.`chat_messages` ( `id` INT(20) NOT NULL AUTO_INCREMENT , `sender_id` INT(5) NOT NULL , `receiver_id` INT(5) NOT NULL , `group_msg` TINYINT(1) NOT NULL DEFAULT '0' , `message` TEXT NOT NULL , `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB;


CREATE TABLE `fms_v2_db`.`chat_groups` ( `id` INT(3) NOT NULL AUTO_INCREMENT , `group_name` VARCHAR(255) NOT NULL , `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB;

CREATE TABLE `fms_v2_db`.`chat_group_members` ( `id` INT(2) NOT NULL AUTO_INCREMENT , `group_id` INT(5) NOT NULL , `user_id` INT(3) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

CREATE TABLE `fms_v2_db`.`chat_acknowledgement` ( `id` INT(20) NOT NULL AUTO_INCREMENT , `message_id` INT(20) NOT NULL , `user_id` INT(3) NOT NULL , `msg_read` TINYINT(1) NOT NULL DEFAULT '0' , `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB;
