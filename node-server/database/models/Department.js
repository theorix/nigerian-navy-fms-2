
module.exports = {
  config: {
    tableName: 'departments',
    primaryKey: 'id'
  },
  fields: [
    'id', 'name', 'path', 'number'
  ]
}
