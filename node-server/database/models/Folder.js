
module.exports = {
  config: {
    tableName: 'folders',
    primaryKey: 'id'
  },
  fields: [
    'id', 'path', 'volume_id'
  ]
}
