
module.exports = {
  config: {
    tableName: 'signatures',
    primaryKey: 'id'
  },
  fields: [
    'id', 'path', 'initial', 'user_id'
  ]
}
