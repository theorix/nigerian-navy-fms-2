
module.exports = {
  config: {
    tableName: 'recipients',
    primaryKey: 'id'
  },
  fields: [
    'id', 'user_id', 'file_id', 'commented', 'color'
  ]
}
