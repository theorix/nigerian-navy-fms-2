
module.exports = {
  config: {
    tableName: 'outgoing_docs',
    primaryKey: 'id'
  },
  fields: [
    'id', 'file_id', 'folio_number', 'volume_id', 'subject', 'created_at'
  ]
}

// ALTER TABLE `outgoing_docs` ADD `folio_number` INT(10) NULL DEFAULT NULL AFTER `file_id`, ADD `volume_id` INT(10) NOT NULL AFTER `folio_number`, ADD `type` VARCHAR(10) NOT NULL DEFAULT 'pdf' AFTER `volume_id`;
