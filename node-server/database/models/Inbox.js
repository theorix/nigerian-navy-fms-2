
module.exports = {
  config: {
    tableName: 'inbox',
    primaryKey: 'id'
  },
  fields: [
    'id', 'user_id', 'file_id', 'title', 'sender_email', 'created_at', 'text', 'commited', 'trashed', 'type'
  ]
}
