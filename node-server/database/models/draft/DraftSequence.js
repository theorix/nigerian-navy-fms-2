module.exports = {
  config: {
    tableName: 'draft_sequence',
    primaryKey: 'id'
  },
  fields: [
    'id', 'version_id', 'sender_id', 'receiver_id', 'created_at', 'commited'
  ]
}
