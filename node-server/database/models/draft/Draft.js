module.exports = {
  config: {
    tableName: 'draft',
    primaryKey: 'id'
  },
  fields: [
    'id', 'file_id', 'subject', 'participants', 'type', 'user_id', 'extra_info', 'created_at', 'updated_at', 'status', 'final_copy', 'approved_by'
  ]
}
