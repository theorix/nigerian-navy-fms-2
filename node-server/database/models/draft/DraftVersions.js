module.exports = {
  config: {
    tableName: 'draft_versions',
    primaryKey: 'id'
  },
  fields: [
    'id', 'version_number', 'document', 'tagged_document', 'draft_id', 'tags', 'created_at'
  ]
}
