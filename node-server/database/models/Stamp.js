
module.exports = {
  config: {
    tableName: 'stamps',
    primaryKey: 'id'
  },
  fields: [
    'id', 'signature_id', 'file_id', 'date'
  ]
}
