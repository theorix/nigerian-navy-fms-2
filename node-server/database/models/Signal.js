
module.exports = {
  config: {
    tableName: 'signals',
    primaryKey: 'id'
  },
  fields: [
    'id', 'user_id', 'created_at', 'type', 'subject', 'type'
  ]
}
