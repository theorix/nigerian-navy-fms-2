
module.exports = {
  config: {
    tableName: 'users',
    primaryKey: 'id'
  },
  fields: [
    'id', 'firstname', 'surname', 'middlename', 'email', 'password', 'appointment', 'active'
  ],

  relate: {
    table: 'inbox',
    on: 'user_id',
    with: 'id',
    type: 'many'
  }
}
