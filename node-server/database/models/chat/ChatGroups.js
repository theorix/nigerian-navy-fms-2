module.exports = {
  config: {
    tableName: 'chat_groups',
    primaryKey: 'id'
  },
  fields: [
    'id', 'group_name', 'created_at'
  ]
}
