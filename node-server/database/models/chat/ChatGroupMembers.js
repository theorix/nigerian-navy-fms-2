module.exports = {
  config: {
    tableName: 'chat_group_members',
    primaryKey: 'id'
  },
  fields: [
    'id', 'group_id', 'user_id', 'admin'
  ]
}
