module.exports = {
  config: {
    tableName: 'chat_acknowledgements',
    primaryKey: 'id'
  },
  fields: [
    'id', 'message_id', 'user_id', 'msg_read', 'updateda_at'
  ]
}
