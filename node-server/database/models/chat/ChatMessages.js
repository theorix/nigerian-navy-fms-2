module.exports = {
  config: {
    tableName: 'chat_messages',
    primaryKey: 'id'
  },
  fields: [
    'id', 'sender_id', 'receiver_id', 'group_msg', 'message', 'created_at'
  ]
}
