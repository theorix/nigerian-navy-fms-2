
module.exports = {
  config: {
    tableName: 'memos',
    primaryKey: 'id'
  },
  fields: [
    'id', 'user_id', 'type', 'content', '_from', '_to',
    'name_in_block_letter', 'rank', 'signature', 'initial', 'appointment',
    'subject', 'created_at'
  ]
}
