
module.exports = {
  config: {
    tableName: 'volumes',
    primaryKey: 'id'
  },
  fields: [
    'id', 'name', 'year', 'type', 'opening_date', 'closing_date', 'sub_department_id'
  ]
}
