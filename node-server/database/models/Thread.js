
module.exports = {
  config: {
    tableName: 'thread',
    primaryKey: 'id'
  },
  fields: [
    'id', 'file_id', 'type', 'comment', 'created_at', 'user_id'
  ]
}
