
module.exports = {
  config: {
    tableName: 'files',
    primaryKey: 'id'
  },
  fields: [
    'id', 'folio_number', 'subject', 'content', 'type', 'volume_id', '_from', '_to',
    'name_in_block_letter', 'rank', 'signature', 'initial', 'appointment', 'created_at'
  ]
}
