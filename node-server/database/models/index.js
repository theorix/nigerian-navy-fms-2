const Department = require('./Department')
const Subdepartment = require('./Subdepartment')
const File = require('./File')
const OutgoingDocs = require('./OutgoingDocs')
const Folder = require('./Folder')
const Inbox = require('./Inbox')
const Memo = require('./Memo')
const Recipient = require('./Recipients')
const Signal = require('./Signal')
const SignalComment = require('./SignalComment')
const Signature = require('./Signature')
const Appointment = require('./Appointment')
const Volume = require('./Volume')
const User = require('./User')
const Thread = require('./Thread')
const Stamp = require('./Stamp')
const config = require('../../config')

// draft
const Draft = require('./draft/Draft')
const DraftSequence = require('./draft/DraftSequence')
const DraftVersions = require('./draft/DraftVersions')

// chat
const ChatMessages = require('./chat/ChatMessages')
const ChatGroups = require('./chat/ChatGroups')
const ChatGroupMembers = require('./chat/ChatGroupMembers')
const ChatAcknowledgements = require('./chat/ChatAcknowledgement')

/* ***************** raw queries ***************** */
let usersQuery = `SELECT users.id, users.firstname, users.surname, users.middlename, users.email,
      appointments.name as appointment, appointments.color, signatures.path as signature, signatures.initial, signatures.id as signature_id FROM users
      INNER JOIN appointments ON users.appointment = appointments.id
      INNER JOIN signatures ON signatures.user_id = users.id
      `

let fileBaseQuery = `SELECT files.id, files.folio_number, files.subject, files.content, files.type, files.rank, files.signature, files.initial, files.appointment, files.created_at, files._to, files._from, files.name_in_block_letter,
      volumes.id as volume, volumes.year, volumes.name as volume_name, sub_departments.id as sub_department, sub_departments.number as subdept_number, departments.id as department, departments.number as dept_number from files
      INNER JOIN volumes ON volumes.id = files.volume_id
      INNER JOIN sub_departments ON sub_departments.id = volumes.sub_department_id
      INNER JOIN departments ON departments.id = sub_departments.department_id`

let outgoingFileBaseQuery = `SELECT outgoing_docs.id, outgoing_docs.folio_number, outgoing_docs.subject, outgoing_docs.type,
      volumes.id as volume, volumes.year, volumes.name as volume_name, sub_departments.id as sub_department, sub_departments.number as subdept_number, departments.id as department, departments.number as dept_number from outgoing_docs
      INNER JOIN volumes ON volumes.id = outgoing_docs.volume_id
      INNER JOIN sub_departments ON sub_departments.id = volumes.sub_department_id
      INNER JOIN departments ON departments.id = sub_departments.department_id`
/* ***************** end queries ***************** */

// configure ORM
const duffORM = require('../../lib/duff-orm')
duffORM.configure({
  db: {
    driver: 'mysql',
    mysql: {
      host: config.db.mysql.host,
      user: config.db.mysql.user,
      password: config.db.mysql.password,
      database: config.db.mysql.database
    }
  },
  debug: false
})

// generate the models using the orm
const Model = duffORM.model()
module.exports = {
  Department: Model(Department),
  Subdepartment: Model(Subdepartment),
  File: Model(File),
  OutgoingDocs: Model(OutgoingDocs),
  Folder: Model(Folder),
  Appointment: Model(Appointment),
  Inbox: Model(Inbox),
  Memo: Model(Memo),
  Recipients: Model(Recipient),
  Signal: Model(Signal),
  SignalComment: Model(SignalComment),
  Stamp: Model(Stamp),
  Signature: Model(Signature),
  Thread: Model(Thread),
  User: Model(User),
  Volume: Model(Volume),

  // draft
  Draft: Model(Draft),
  DraftSequence: Model(DraftSequence),
  DraftVersions: Model(DraftVersions),

  // chat
  ChatMessages: Model(ChatMessages),
  ChatGroups: Model(ChatGroups),
  ChatGroupMembers: Model(ChatGroupMembers),
  ChatAcknowledgements: Model(ChatAcknowledgements),

  queries: {
    usersQuery,
    fileBaseQuery,
    outgoingFileBaseQuery
  }
}
