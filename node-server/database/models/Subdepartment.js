
module.exports = {
  config: {
    tableName: 'sub_departments',
    primaryKey: 'id'
  },
  fields: [
    'id', 'name', 'number', 'department_id'
  ]
}
