
module.exports = {
  config: {
    tableName: 'signal_comments',
    primaryKey: 'id'
  },
  fields: [
    'id', 'signal_id', 'user_id', 'content', 'created_at'
  ]
}
