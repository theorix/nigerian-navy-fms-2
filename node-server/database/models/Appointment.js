module.exports = {
  config: {
    tableName: 'appointments',
    primaryKey: 'id'
  },
  fields: ['id', 'name', 'color']
}
