const archiveHelper = require('./lib/archiveHelper')
const config = require('./config')
const Path = require('path')

// read the archiveRoot url dir

async function upload (file, D) {
  // console.log('uploaded', file, 'in ', Path.join(D))
  const data = D.split('/')
  // console.log(data)
  if (file.includes('.pdf') || file.includes('.jpg')) {
    console.log('filePath:', Path.join(D), file)
    file.replace()
    let newName = file.replace('.pdf', '')
    newName = newName.replace('.jpg', '')
    console.log('newName', newName, file)
    archiveHelper.moveFile(`${Path.join(D)}/${file}`, Path.join(D), newName)
  }
}

function resolvePath (name, d, D) {
  const path = Path.join(D.slice(0, d).join('/'), name)
  // console.log('resolve:', path)
  return path
}

async function processDir (dir, d, D) {
  D[d] = dir
  const path = resolvePath(dir, d, D)
  const dirs = await archiveHelper.readDir(path)
  // console.log('dirs:', dirs, path)
  for (let file of dirs) {
    if (await archiveHelper.isDirectory(Path.join(path, file))) {
      await processDir(file, d + 1, D)
    } else {
      await upload(file, path)
    }
  }
}

(
  async function () {
    await processDir(config.server.archiveRootURL, 0, [])
  }
)()
