const Model = require('../database/models')
const logger = require('../lib/log')
const userCache = require('../lib/cacheHelper')

// const Inbox = Model.Inbox
let N = 15 // number of results per page

const controller = {
  getInbox: async function (req, res) {
    const inbox = (await Model.Inbox.get({ id: req.params.fileId }))[0]
    // console.log('mail:', inbox)
    res.send(inbox)
  },
  getUserInbox: async function (req, res) {
    logger.info('queryStringPage:', req.query.page)
    logger.info('userId:', req.params.userId)
    logger.info('filterParam:', req.query.filter)
    logger.info('inbox type:', req.query.type)
    logger.info('searchTerm:', req.query.search)
    logger.info('startDate:', req.query.startDate)
    let searchByTitleAndContent = ''
    let search = ''
    if (req.query.search) {
      searchByTitleAndContent = Model.Inbox.searchQ(['title', 'text'], req.query.search)
      search = ` AND (${searchByTitleAndContent})`
    } else {
      search = `  ORDER BY inbox.id DESC `
    }

    // limit by date
    let limitByDate = ''
    if (req.query.startDate) {
      let startDate, endDate
      startDate = new Date(req.query.startDate).toISOString().slice(0, 19).replace('T', ' ')
      if (req.query.endDate) endDate = new Date(req.query.endDate).toISOString().slice(0, 19).replace('T', ' ')
      else endDate = new Date().toISOString().slice(0, 19).replace('T', ' ')

      limitByDate = ` AND (inbox.created_at >= "${startDate}" AND inbox.created_at <= "${endDate}")`
    }

    // get user
    const usr = (await Model.User.get({
      id: req.params.userId
    }
    ))[0]

    const L = (req.query.page - 1) * N
    logger.info('L:', L, req.query.page)

    // finally, get all inboxes of all users with same appointment
    let inB = ''
    let inboxType = ''
    switch (req.query.type) {
      case 'incoming':
        inboxType = `inbox.type = "2"`
        break

      case 'outgoing':
        inboxType = `inbox.type = "3"`
        break

      case 'ng':
        inboxType = `inbox.type = "4"`
        break

      case 'nhq':
        inboxType = `inbox.type = "5"`
        break

      default:
        inboxType = `(inbox.type = "0" OR inbox.type = "1")`
        break
    }

    // logger.warn(`
    // SELECT inbox.id, inbox.sender_email, inbox.file_id, inbox.title, inbox.created_at, inbox.text, inbox.commited, inbox.type, users.email
    // FROM inbox INNER JOIN users ON users.id = inbox.user_id WHERE users.appointment = "${usr.appointment}" AND inbox.trashed = "0"
    // AND ${inboxType} ${limitByDate} ${search}
    // LIMIT ${L}, ${N}
    // `)

    switch (req.query.filter) {
      case 'read':
        inB = (await Model.User.Query(
          `
          SELECT inbox.id, inbox.file_id, inbox.sender_email, inbox.title, inbox.created_at, inbox.text, inbox.commited, inbox.type, users.email 
          FROM inbox INNER JOIN users ON users.id = inbox.user_id WHERE users.appointment = "${usr.appointment}" AND inbox.trashed = "0" AND inbox.commited = "1"
          AND ${inboxType} ${limitByDate} ${search}
          LIMIT ${L}, ${N}
          `
        ))
        break

      case 'unread':
        inB = (await Model.User.Query(
          `
          SELECT inbox.id, inbox.sender_email, inbox.file_id, inbox.title, inbox.created_at, inbox.text, inbox.commited, inbox.type, users.email 
          FROM inbox INNER JOIN users ON users.id = inbox.user_id WHERE users.appointment = "${usr.appointment}" AND inbox.trashed = "0" AND inbox.commited = "0"
          AND ${inboxType} ${limitByDate} ${search} 
          LIMIT ${L}, ${N}
          `
        ))

        break

      default:
        inB = (await Model.User.Query(
          `
          SELECT inbox.id, inbox.sender_email, inbox.file_id, inbox.title, inbox.created_at, inbox.text, inbox.commited, inbox.type, users.email 
          FROM inbox INNER JOIN users ON users.id = inbox.user_id WHERE users.appointment = "${usr.appointment}" AND inbox.trashed = "0"
          AND ${inboxType} ${limitByDate} ${search}
          LIMIT ${L}, ${N}
          `
        ))
    }

    // logger.info(`mails:-${inboxType}-${req.query.filter}`, JSON.stringify(inB.results))
    res.send({ messages: inB.results, type: req.query.type })
  },

  getRecentMessages: async function (req, res) {
    const type = req.params.type
    const userId = req.params.userId
    // get user
    const usr = (await Model.User.get({
      id: userId
    }
    ))[0]

    let inboxType = ''

    if (type === 'signal') inboxType = `inbox.type > 1`
    else inboxType = `inbox.type < 2`

    const recent = await Model.Inbox.Query(`
    SELECT inbox.id, inbox.file_id, inbox.sender_email, inbox.title, inbox.created_at, inbox.text, inbox.commited, inbox.type, users.email 
    FROM inbox INNER JOIN users ON users.id = inbox.user_id WHERE users.appointment = "${usr.appointment}" AND inbox.trashed = "0" AND inbox.commited = "0"
    AND ${inboxType} ORDER BY inbox.id DESC
    LIMIT 4
    `)

    res.send({
      success: true,
      data: recent.results
    })
  },

  getUserOutbox: async function (req, res) {
    logger.info('queryStringPageOutbox:', req.query.page)
    logger.info('userIdOutbox:', req.params.userId)

    const L = (req.query.page - 1) * N

    const usr = (await Model.User.get({
      id: req.params.userId
    }
    ))[0]

    // get user outbox
    const outB = (await Model.User.Query(
      `
      SELECT inbox.id, inbox.file_id, inbox.title, inbox.created_at, inbox.text, inbox.commited, inbox.type, users.email 
      FROM inbox INNER JOIN users ON users.id = inbox.user_id WHERE  inbox.sender_email = "${usr.email}" ORDER BY inbox.id DESC LIMIT ${L}, ${N}
      `
    ))

    // logger.info('Outmails:', JSON.stringify(outB.results))
    res.send(outB.results)
  },

  getTrashedMessages: async function (req, res) {
    logger.info('queryStringPage:', req.query.page)
    logger.info('userId:', req.params.userId)
    logger.info('filterParam:', req.query.filter)

    let searchByTitleAndContent = ''
    let search = ''
    if (req.query.search) {
      searchByTitleAndContent = Model.Inbox.searchQ(['title', 'text'], req.query.search)
      search = ` AND (${searchByTitleAndContent})`
    } else {
      search = `  ORDER BY inbox.id DESC `
    }

    // limit by date
    let limitByDate = ''
    if (req.query.startDate) {
      let startDate, endDate
      startDate = new Date(req.query.startDate).toISOString().slice(0, 19).replace('T', ' ')
      if (req.query.endDate) endDate = new Date(req.query.endDate).toISOString().slice(0, 19).replace('T', ' ')
      else endDate = new Date().toISOString().slice(0, 19).replace('T', ' ')

      limitByDate = ` AND (inbox.created_at >= "${startDate}" AND inbox.created_at <= "${endDate}")`
    }

    // get user
    const usr = (await Model.User.get({
      id: req.params.userId
    }
    ))[0]

    const L = (req.query.page - 1) * N
    let trashedB = ''

    switch (req.query.filter) {
      case 'read':
        trashedB = (await Model.User.Query(
          `
          SELECT inbox.id, inbox.sender_email, inbox.file_id, inbox.title, inbox.created_at, inbox.text, inbox.commited, inbox.type, users.email 
          FROM inbox INNER JOIN users ON users.id = inbox.user_id WHERE users.appointment = "${usr.appointment}" AND inbox.trashed = "1" AND inbox.commited = "1" 
          ${limitByDate} ${search}
          LIMIT ${L}, ${N}
    
          `
        ))
        break

      case 'unread':
        trashedB = (await Model.User.Query(
          `
          SELECT inbox.id, inbox.sender_email, inbox.file_id, inbox.title, inbox.created_at, inbox.text, inbox.commited, inbox.type, users.email 
          FROM inbox INNER JOIN users ON users.id = inbox.user_id WHERE users.appointment = "${usr.appointment}" AND inbox.trashed = "1" AND inbox.trashed = "1" AND inbox.commited = "0" 
          ${limitByDate} ${search}
          LIMIT ${L}, ${N}
    
          `
        ))
        break

      default:
        trashedB = (await Model.User.Query(
          `
          SELECT inbox.id, inbox.sender_email, inbox.file_id, inbox.title, inbox.created_at, inbox.text, inbox.commited, inbox.type, users.email 
          FROM inbox INNER JOIN users ON users.id = inbox.user_id WHERE users.appointment = "${usr.appointment}" AND inbox.trashed = "1" 
          ${limitByDate} ${search}
          LIMIT ${L}, ${N}
    
          `
        ))
        break
    }
    // logger.warn(JSON.stringify(trashedB.results))
    res.send(trashedB.results)
  },

  trashMessages: async function (req, res) {
    // const userId = req.body.user_id
    const trashCode = req.body.code // 0 => untrash, 1 => trash, 2 => delete
    const messages = req.body.messages
    let userId = 0 // req.body.userId
    if (!trashCode && trashCode !== 0) return res.send({ success: false, data: 'trash Code missing' })
    let msgs = []

    for (let messageId of messages) {
      await Model.Inbox.update({
        set: {
          trashed: trashCode
        },
        queryKey: {
          id: messageId
        }
      })

      // fetch inbox message to be used in cache
      msgs.push((await Model.Inbox.fetch({
        id: messageId
      }))[0])
      userId = msgs[0].user_id
    }

    // cache the changes
    const cache = await userCache.getCache(userId)
    switch (Number(req.body.code)) {
      case 0: // restore messages
        // decrease the cTrash by messages.length
        cache['inboxCount']['cTrashed'] -= messages.length
        for (let m of msgs) {
          if (Number(m.type) === 0 || Number(m.type) === 1) {
            cache['inboxCount']['cRegular'] += 1
            if (!Number(m.commited)) cache['inboxCount']['cRegularUnread'] += 1
          } else if (Number(m.type) === 2) {
            cache['inboxCount']['cIncoming'] += 1
            if (!Number(m.commited)) cache['inboxCount']['cIncomingUnread'] += 1
          } else if (Number(m.type) === 3) {
            cache['inboxCount']['cOutgoing'] += 1
            if (!Number(m.commited)) cache['inboxCount']['cOutgoingUnread'] += 1
          } else if (Number(m.type) === 4) {
            cache['inboxCount']['cNg'] += 1
            if (!Number(m.commited)) cache['inboxCount']['cNgUnread'] += 1
          } else if (Number(m.type) === 5) {
            cache['inboxCount']['cNhq'] += 1
            if (!Number(m.commited)) cache['inboxCount']['cNhqUnread'] += 1
          }
        }
        await userCache.updateCache(userId, cache, true)

        break

      case 1:
        // increase the cTrash by messages.length
        cache['inboxCount']['cTrashed'] += messages.length
        for (let m of msgs) {
          if (!Number(m.commited)) cache['inboxCount']['cTrashedUnread'] += 1
        }
        await userCache.updateCache(userId, cache, true)
        break

      case 2:
        // decrease the cTrash by messages.length
        cache['inboxCount']['cTrashed'] -= messages.length
        for (let m of msgs) {
          if (!Number(m.commited)) cache['inboxCount']['cTrashedUnread'] -= 1
        }
        await userCache.updateCache(userId, cache, true)
        break
    }

    res.send({
      success: true,
      data: 'Message Trashed'
    })
  },

  markAsRead: async function (req, res) {
    const messages = req.body.messages
    let userId = 0
    let msgs = []
    for (let messageId of messages) {
      await Model.Inbox.update({
        set: {
          commited: 1
        },
        queryKey: {
          id: messageId
        }
      })

      // fetch inbox message to be used in cache
      msgs.push((await Model.Inbox.fetch({
        id: messageId
      }))[0])
      userId = msgs[0].user_id
    }

    // cache the changes
    const cache = await userCache.getCache(userId)
    for (let m of msgs) {
      if (Number(m.type) === 0 || Number(m.type) === 1) {
        if (!Number(m.commited)) cache['inboxCount']['cRegularUnread'] -= 1
      } else if (Number(m.type) === 2) {
        if (!Number(m.commited)) cache['inboxCount']['cIncomingUnread'] -= 1
      } else if (Number(m.type) === 3) {
        if (!Number(m.commited)) cache['inboxCount']['cOutgoingUnread'] -= 1
      } else if (Number(m.type) === 4) {
        if (!Number(m.commited)) cache['inboxCount']['cNgUnread'] -= 1
      } else if (Number(m.type) === 5) {
        if (!Number(m.commited)) cache['inboxCount']['cNhqUnread'] -= 1
      }
    }
    await userCache.updateCache(userId, cache, true)

    res.send({
      success: true,
      data: 'Message Treated'
    })
  },

  countInbox: async function (req, res) {
    // const cAll = await countInboxMessages({
    //   user_id: req.query.userId,
    //   trashed: 0
    // })
    // const cUnread = await countInboxMessages({
    //   user_id: req.query.userId,
    //   commited: 0,
    //   trashed: 0
    // })
    const cache = await userCache.getCache(req.query.userId)
    const cAll = cache['inboxCount']['cRegular']
    const cUnread = cache['inboxCount']['cRegularUnread']

    res.send({ all: cAll, unread: cUnread })
  },

  countRegularInbox: async function (req, res) {
    const cache = await userCache.getCache(req.query.userId)
    const cAll = cache['inboxCount']['cRegular']
    const cUnread = cache['inboxCount']['cRegularUnread']

    res.send({ all: cAll, unread: cUnread })
  },

  countSignals: async function (req, res) {
    const cache = await userCache.getCache(req.query.userId)
    const cAll = cache['inboxCount']['cSignals']
    const cUnread = cache['inboxCount']['cSignalsUnread']

    res.send({ all: cAll, unread: cUnread })
  },

  countOutSignal: async function (req, res) {
    const cache = await userCache.getCache(req.query.userId)
    const cAll = cache['inboxCount']['cOutgoing']
    const cUnread = cache['inboxCount']['cOutgoingUnread']

    res.send({ all: cAll, unread: cUnread })
  },

  countInSignal: async function (req, res) {
    const cache = await userCache.getCache(req.query.userId)
    const cAll = cache['inboxCount']['cIncoming']
    const cUnread = cache['inboxCount']['cIncomingUnread']
    res.send({ all: cAll, unread: cUnread })
  },

  countNgSignal: async function (req, res) {
    const cache = await userCache.getCache(req.query.userId)
    const cAll = cache['inboxCount']['cNg']
    const cUnread = cache['inboxCount']['cNgUnread']
    res.send({ all: cAll, unread: cUnread })
  },

  countNhqSignal: async function (req, res) {
    console.log('req.query:', req.query)
    const cache = await userCache.getCache(req.query.userId)
    const cAll = cache['inboxCount']['cNhq']
    const cUnread = cache['inboxCount']['cNhqUnread']
    res.send({ all: cAll, unread: cUnread })
  },

  countTrashedMessages: async function (req, res) {
    const cache = await userCache.getCache(req.query.userId)
    const cAll = cache['inboxCount']['cNhq']
    const cUnread = cache['inboxCount']['cNhqUnread']
    console.log('serverCountTrashed:', cAll, cUnread)
    res.send({ all: cAll, unread: cUnread })
  }
}

module.exports = controller

// CREATE INDEX inbox_index ON inbox(id, type, title, trashed, commited)
// OPTIMIZE TABLE `appointments`, `files`, `inbox`, `memos`, `recipients`, `signals`, `signatures`, `stamps`, `thread`, `users`, `volumes`
// ALTER TABLE inbox ADD INDEX(id, trashed, commited, title, created_at)
// ALTER TABLE files ADD INDEX(id, volume_id, folio_number, subject)
// ALTER TABLE thread ADD INDEX(id, file_id, user_id)
// ALTER TABLE signal_comments ADD INDEX(id, signal_id, user_id)
// ALTER TABLE users ADD INDEX(id, appointment, email)

// ALTER TABLE inbox ADD FULLTEXT (title, text);
//  ALTER TABLE files ADD FULLTEXT (subject,folio_number)
