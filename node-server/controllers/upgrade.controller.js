const Model = require('../database/models')

const controller = {
  dbUpgrade: async function (req, res) {
    await Model.Inbox.Query(`ALTER TABLE inbox ADD trashed TINYINT(1) DEFAULT 0`)
    // await Model.Signature.Query(`ALTER TABLE signatures CHANGE initial initial VARCHAR(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;`)
    // await Model.Signature.Query(`ALTER TABLE signatures CHANGE path path VARCHAR(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;`)
    await Model.File.Query(`ALTER TABLE files ADD volume_id INT(11) DEFAULT 0`)
    const folders = await Model.Folder.fetch()
    for (let folder of folders) {
      await Model.File.update({
        set: {
          volume_id: folder.volume_id
        },
        queryKey: {
          folder_id: folder.id
        }
      })
    }
    await Model.File.Query(`ALTER TABLE files DROP COLUMN folder_id`)
    // // await Model.File.Query(`ALTER TABLE files CHANGE subject subject VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;`)
    // await Model.File.Query(`ALTER TABLE files CHANGE signature signature VARCHAR(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;`)
    // await Model.File.Query(`ALTER TABLE files CHANGE name_in_block_letter name_in_block_letter VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;`)

    let files = (await Model.File.Query(`SELECT files.id, files.signature, files.initial, files.appointment, signatures.user_id FROM files INNER JOIN signatures ON signatures.path = files.signature`)).results
    let memos = (await Model.File.Query(`SELECT memos.id, memos.signature, memos.initial, memos.appointment, signatures.user_id FROM memos INNER JOIN signatures ON signatures.path = memos.signature`)).results
    const signatures = (await Model.Signature.Query(`SELECT signatures.id, signatures.path, signatures.user_id, appointments.name as appointment FROM signatures INNER JOIN users ON users.id = signatures.user_id INNER JOIN appointments ON appointments.id = users.appointment`)).results
    // let pref = '/hqenc_fms/client/static/img/signature'
    for (let f of files) {
      await Model.File.update({
        set: {
          signature: `${f.appointment}-${f.user_id}.jpg`
        },
        queryKey: {
          id: f.id
        }
      })
    }

    for (let m of memos) {
      await Model.Memo.update({
        set: {
          signature: `${m.appointment}-${m.user_id}.jpg`
        },
        queryKey: {
          id: m.id
        }
      })
    }

    for (let s of signatures) {
      await Model.Signature.update({
        set: {
          path: `${s.appointment}-${s.user_id}.jpg`
        },
        queryKey: {
          id: s.id
        }
      })
    }
    res.send({
      success: true,
      message: 'Done upgrading'
    })
  }
}

module.exports = controller
