const Model = require('../database/models')
const onlineUsers = require('../lib/socketHandler').onlineUsers
const logger = require('../lib/log')
const config = require('../config')

const controller = {
  storeMessage: async function (req, res) {
    const sender = req.body.sender
    const receiver = req.body.receiver
    const groupId = req.body.isGroup ? req.body.groupId : 0
    const groupMembers = groupId ? req.body.groupMembers : []
    const message = req.body.message

    if (!sender || !sender.id) return res.send({ success: false, message: 'Error: Invalid sender' })
    if (!receiver || !receiver.id) return res.send({ success: false, message: 'Error: Invalid receiver' })
    if (!message) return res.send({ success: false, message: 'Error: Invalid message' })

    const msgId = await Model.ChatMessages.create({
      sender_id: sender.id,
      receiver_id: !groupId ? receiver.id : groupId,
      group_msg: groupId ? 1 : 0,
      message: message
    })

    if (msgId && groupId) {
      for (let m of groupMembers) {
        await Model.ChatAcknowledgements.create({
          message_id: msgId,
          user_id: m.id
        })

        onlineUsers[m.email].emit('new-chat-message', {
          sender,
          receiver,
          message,
          groupId
        })
      }
    } else if (msgId && !groupId) {
      await Model.ChatAcknowledgements.create({
        message_id: msgId,
        user_id: receiver.id
      })
      onlineUsers[receiver.email].emit('new-chat-message', {
        sender,
        receiver,
        message,
        groupId
      })
    }

    if (msgId) res.send({ success: true, messageId: msgId })
    else res.send({ success: false, message: 'Error saving message' })
  },

  createGroup: async function (req, res) {
    const groupName = req.body.name
    const owner = req.body.owner
    const members = req.body.members

    if (!groupName) return res.send({ success: false, message: 'Error: Invalid group name' })
    if (!owner || !owner.id) return res.send({ success: false, message: 'Error: Invalid group owner' })
    if (!members || !members.length) return res.send({ success: false, message: 'Error: Invalid group members' })

    const groupId = await Model.ChatGroups.create(
      {
        group_name: groupName
      }
    )
    await Model.ChatGroupMembers.create({
      group_id: groupId,
      user_id: owner.id,
      admin: 1
    })

    for (let m of members) {
      await Model.ChatGroupMembers.create({
        group_id: groupId,
        user_id: m.id
      })
    }

    if (groupId) res.send({ success: true, groupId: groupId })
    else res.send({ success: false, message: 'Error: Failed to create group' })
  },

  recentChats: async function (req, res) {
    const userId = req.params.userId
    // let officers = (await Model.Users.Query(`${Model.queries.usersQuery} WHERE users.id <> "${profile.id}"`)).results
    let groups = (await Model.User.Query(`
      SELECT chat_groups.id, chat_groups.group_name from chat_groups
      INNER JOIN chat_group_members ON chat_groups.id = chat_group_members.group_id
      WHERE chat_group_members.user_id = "${userId}"
      `)).results

    res.send({ success: true, data: groups })
    logger.warn('recent chats', userId, groups)
    // res.send(
    //   {
    //     success: true,
    //     data: {
    //       'CSO': {
    //         userData: {
    //           avatar: `/fms2.0/hqenc/static/img/user2.jpeg`,
    //           text: "Some messages sent before now ...",
    //           time: "11:00",
    //           appointment: 'CSO'
    //         },
    //         messages: [
    //           {
    //             avatar: `/fms2.0/hqenc/static/img/user2.jpeg`,
    //             text: "Hello. How are you today?",
    //             time: "11:00",
    //             isMe: false
    //           },
    //           {
    //             avatar: `/fms2.0/hqenc/static/img/user2.jpeg`,
    //             text: "Hey! I'm fine. Thanks for asking!",
    //             time: "11:01",
    //             isMe: true
    //           },
    //           {
    //             avatar: `/fms2.0/hqenc/static/img/user2.jpeg`,
    //             text: "Sweet! So, what do you wanna do today?",
    //             time: "11:02",
    //             isMe: false
    //           }
    //         ]
    //       },
    //       'CCITO': {
    //         userData: {
    //           avatar: `/fms2.0/hqenc/static/img/user2.jpeg`,
    //           text: "Other messages here to send ...",
    //           time: "14:09",
    //           appointment: 'CCITO'
    //         },
    //         messages: [
    //         ]
    //       },
    //       'CLEGAL': {
    //         userData: {
    //           avatar: `/fms2.0/hqenc/static/img/user2.jpeg`,
    //           text: "Yet stuff other things ...",
    //           time: "09:40",
    //           appointment: 'CLEGAL'
    //         },
    //         messages: []
    //       },
    //       'NETWORK-ADMIN': {
    //         userData: {
    //           avatar: `/fms2.0/hqenc/static/img/user2.jpeg`,
    //           text: "Tomorow is now ...",
    //           time: "10:50",
    //           appointment: 'NETWORK-ADMIN'
    //         },
    //         messages: []
    //       },
    //       'CC': {
    //         userData: {
    //           avatar: `/fms2.0/hqenc/static/img/user2.jpeg`,
    //           text: "document ready for signing ...",
    //           time: "09:40",
    //           appointment: 'CC'
    //         },
    //         messages: []
    //       }
    //     }
    //   }
    // )
  }
}

module.exports = controller
