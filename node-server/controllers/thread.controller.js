const Model = require('../database/models')
const Thread = Model.Thread
const logger = require('../lib/log')
const onlineUsers = require('../lib/socketHandler').onlineUsers
const userCache = require('../lib/cacheHelper')

async function acknowledge (userId, userEmail, fileId) {
  const nUpdated = await Model.Inbox.count({ commited: 0, user_id: userId, file_id: fileId })
  const cache = await userCache.getCache(userId)
  cache['inboxCount']['cRegularUnread'] -= nUpdated

  const inboxUpdated = await Model.Inbox.update({
    set: {
      commited: 1
    },
    queryKey: {
      user_id: userId,
      file_id: fileId
    }
  })

  const recipientsUpdated = await Model.Recipients.update({
    set: {
      commented: 1
    },
    queryKey: {
      user_id: userId,
      file_id: fileId
    }
  })

  if (inboxUpdated && recipientsUpdated) {
    await userCache.updateCache(userId, cache, true)

    // send a socket message here to the client
    if (onlineUsers[userEmail]) {
      onlineUsers[userEmail].emit('message', {
        type: 'update-inbox',
        file_id: fileId,
        n: nUpdated
      })
    } else {
      logger.warn('UNABLE TO SEND MESSAGE VIA SOCKET')
    }
    return true
  } else return false
}

const controller = {
  threadFileId: function (req, res) {
    Thread.get({
      file_id: req.params.threadId
    })
      .then((thread) => {
        const thrd = thread.map((t) => {
          return JSON.stringify(t.toJson())
        })
        res.send(thrd)
      }).catch((err) => {
        logger.error(err.message)
      })
  },

  getStamp: async function (req, res) {
    const stamps = await Thread.Query(`
    SELECT appointments.name as appointment, appointments.color, stamps.id, signatures.path as signature, stamps.date FROM stamps
    INNER JOIN signatures ON signatures.id = stamps.signature_id
    INNER JOIN users ON users.id = signatures.user_id
    INNER JOIN appointments ON users.appointment = appointments.id
    WHERE stamps.file_id = ${req.params.fileId}
    `)

    const stamp = [
      {
        appointment: 'FOC',
        signature: '',
        date: ''
      },
      {
        appointment: 'CSO',
        signature: '',
        date: ''
      },
      {
        appointment: 'COO',
        signature: '',
        date: ''
      },
      {
        appointment: 'CAO',
        signature: '',
        date: ''
      },
      {
        appointment: 'CCITO',
        signature: '',
        date: ''
      },
      {
        appointment: 'CINTO',
        signature: '',
        date: ''
      },
      {
        appointment: 'CPM',
        signature: '',
        date: ''
      },
      {
        appointment: 'CLO',
        signature: '',
        date: ''
      },
      {
        appointment: 'CTO',
        signature: '',
        date: ''
      },
      {
        appointment: 'CEDO',
        signature: '',
        date: ''
      },
      {
        appointment: 'C-TRANS',
        signature: '',
        date: ''
      },
      {
        appointment: 'CABO',
        signature: '',
        date: ''
      },
      {
        appointment: 'CMO',
        signature: '',
        date: ''
      },
      {
        appointment: 'CINFO',
        signature: '',
        date: ''
      },
      {
        appointment: 'C-LEGAL',
        signature: '',
        date: ''
      },
      {
        appointment: 'C-SPORT',
        signature: '',
        date: ''
      },
      {
        appointment: 'C-PROJ',
        signature: '',
        date: ''
      },
      {
        appointment: 'FAC-MGR',
        signature: '',
        date: ''
      },
      {
        appointment: 'CPU-HEAD',
        signature: '',
        date: ''
      },
      {
        appointment: 'NA',
        signature: '',
        date: ''
      },
      {
        appointment: 'CC',
        signature: '',
        date: ''
      }
    ]

    for (let i in stamp) {
      for (let j in stamps.results) {
        if (stamp[i].appointment === stamps.results[j].appointment) {
          stamp[i].signature = stamps.results[j].signature
          stamp[i].date = stamps.results[j].date
          stamp[i].color = stamps.results[j].color
        }
      }
    }
    res.send(stamp)
  },

  commentOnThread: function (req, res) { // implementation of create_thread
    const userId = req.body.user_id
    const userEmail = req.body.user_email
    const comment = req.body.comment
    const fileId = req.body.file_id
    const fileType = req.body.type // file type

    Model.Thread.create({
      user_id: userId,
      comment,
      file_id: fileId,
      type: fileType
    }).then(async (success) => {
      if (success) {
        if (await acknowledge(userId, userEmail, fileId)) {
          res.send({
            success: true,
            data: 'Success'
          })
        } else {
          res.send({
            success: false,
            data: 'Failure'
          })
        }
      } else {
        res.send({
          success: false,
          data: 'Failure'
        })
      }
    })
  },

  acknowledge: async function (req, res) {
    const userId = req.body.user_id
    const userEmail = req.body.user_email
    const fileId = req.body.file_id

    if (await acknowledge(userId, userEmail, fileId)) {
      res.send({
        success: true,
        data: 'Success'
      })
    } else {
      res.send({
        success: false,
        data: 'Failed to acknowledge File'
      })
    }
  },

  updateThreadType: async function (req, res) {
    const fileId = req.body.file_id
    const threadType = req.body.type

    if (fileId && threadType) {
      const success = await Model.Thread.update({
        set: {
          type: threadType
        },
        queryKey: {
          file_id: fileId
        }
      })
      if (success) {
        res.send({
          success: true,
          data: 'thread type updated'
        })
      } else {
        res.send({
          success: false,
          data: 'Failed to update thread type'
        })
      }
    } else {
      res.send({
        success: false,
        data: 'file id or thread type missing'
      })
    }
  }
}

module.exports = controller
