const Model = require('../database/models')
const logger = require('../lib/log')
const config = require('../config')
const archiveHelper = require('../lib/archiveHelper')
// const path = require('path')
// const docManager = require('../lib/docManager')
const draftHelper = require('../lib/draftHelper')

// ****************** multer configuration file upload ******************
const multer = require('multer')
const fs = require('fs')
async function dest (req, file, callback) {
  let dir = config.server.archiveRootURL // the root folder where the archive is located

  // fetch dept and sub-dept
  const dept = (await Model.Department.get({ id: req.body.department }))[0]
  const subdept = (await Model.Subdepartment.get({ id: req.body.sub_department }))[0]
  const vol = (await Model.Volume.get({ id: req.body.volume }))[0]
  dir = dir + `/${dept.number.trim()}/${subdept.number.trim()}/${vol.year.trim()}/${vol.name.trim()}` // concatenate the path where the file should be stored in the archive
  try {
    if (!fs.existsSync(dir)) {
      logger.info(`making a new dir for ${dir}`)
      fs.mkdirSync(dir)
    }
    callback(null, dir)
  } catch (err) {
    logger.error(err.message)
  }
}

var storage = multer.diskStorage({
  destination: dest,
  filename: async function (req, file, callback) {
    if (!req.body.volume) return callback(new Error('Failed to upload - volume id empty'))
    if (!req.body.department) return callback(new Error('Failed to upload - dept id empty'))
    if (!req.body.sub_department) return callback(new Error('Failed to upload - sub_department id empty'))
    if (!req.body.folio_number) return callback(new Error('Failed to upload - folio number empty'))
    if (!req.body.subject) return callback(new Error('Failed to upload - subject matter empty'))
    const f = await Model.File.get({ folio_number: req.body.folio_number, volume_id: req.body.volume })
    if (f.length > 0) return callback(new Error(`Failed to upload - file with folio number ${req.body.folio_number} already exist`))
    Model.File.create({
      type: req.body.type,
      subject: req.body.subject,
      folio_number: req.body.folio_number,
      volume_id: req.body.volume
    }).then((fileId) => {
      logger.success('fileId:', `${fileId}`)
      let fileType = ''
      switch (req.body.type) {
        case 'images':
          fileType = ''
          break
        case 'pdf':
          fileType = ''
          break
        case 'audio':
          fileType = ''
          break
        case 'video':
          fileType = ''
          break
      }
      callback(null, `${req.body.folio_number}` + `${fileType}`)
    }).catch((err) => {
      logger.error(err.message)
      callback(new Error(`Failed to upload file: ${err.message}`))
    })
  }
})

const outGoingDocs = multer.diskStorage({
  destination (req, file, callback) {
    let dir = config.server.outgoingDocsDir // the folder for storing outgoing docs
    try {
      if (!fs.existsSync(dir)) {
        logger.info(`making a new dir for ${dir}`)
        fs.mkdirSync(dir)
      }
      callback(null, dir)
    } catch (err) {
      logger.error(err.message)
    }
  },
  async filename (req, file, callback) {
    // sanity checks
    if (!req.body.subject) return callback(new Error('Subject must not be empty'))
    if (req.body.volume && isNaN(req.body.volume)) return callback(new Error('Invalid volume ID'))
    if (req.body.fileId && isNaN(req.body.fileId)) return callback(new Error('Invalid file ID'))
    if (req.body.folio_number && isNaN(req.body.folio_number)) return callback(new Error('Invalid folio Number'))

    // check for duplicate when fileId is given
    let volumeId = req.body.volume
    if (req.body.fileId) {
      let f = await Model.File.get({ id: req.body.fileId })
      f = f[0]
      volumeId = f.volume_id
      const dupFile = await Model.OutgoingDocs.get({ file_id: req.body.fileId })
      if (dupFile.length) return callback(new Error('Outgoing file already uploaded!!!'))
    }

    // create database record
    const fileId = await Model.OutgoingDocs.create({
      subject: req.body.subject,
      file_id: req.body.fileId,
      volume_id: volumeId,
      folio_number: req.body.folio_number
    })
    if (!fileId) return callback(new Error(`Unable to create record for file ${req.body.fileId}`))

    // finish off
    const fileName = req.body.fileId ? `${fileId}_${req.body.fileId}.pdf` : `${fileId}.pdf`
    callback(null, fileName)
  }
})

const archiveUpload = multer({ storage: storage }).array('file', 12)
const outgoingDocsUpload = multer({ storage: outGoingDocs }).array('file', 12)
// **************** End of Multer config *******************

const controller = {
  department: function (req, res) {
    if (req.query.subject) {
      const subjectLike = Model.File.searchQ('subject', req.query.subject)
      const byFolioNumber = `files.folio_number = "${req.query.subject}"`
      const fileQuery = Model.queries.fileBaseQuery + ' WHERE ' + subjectLike + ` OR ${byFolioNumber}`
      Model.File.Query(fileQuery).then((f) => {
        res.send({
          data: {
            files: f.results
          }
        })
      })
    } else {
      Model.Department.fetch().then((depts) => {
        res.send({
          data: depts
        })
      })
    }
  },

  subDepartment: function (req, res) {
    if (req.query.subject) {
      const subjectLike = Model.File.searchQ('subject', req.query.subject)
      const byFolioNumber = `files.folio_number = "${req.query.subject}"`
      const fileQuery = Model.queries.fileBaseQuery + ' WHERE (' + subjectLike + ` OR ${byFolioNumber}) AND sub_departments.department_id = '${req.params.deptId}'`
      Model.File.Query(fileQuery).then((f) => {
        res.send({
          data: {
            files: f.results
          }
        })
      })
    } else {
      Model.Subdepartment.fetch({ department_id: req.params.deptId }).then((subdepts) => {
        // console.log('suDept:', subdepts)
        res.send({
          data: subdepts
        })
      })
    }
  },

  year: function (req, res) {
    if (req.query.subject) {
      const subjectLike = Model.File.searchQ('subject', req.query.subject)
      const byFolioNumber = `files.folio_number = "${req.query.subject}"`
      const fileQuery = Model.queries.fileBaseQuery + ' WHERE (' + subjectLike + ` OR ${byFolioNumber}) AND sub_departments.department_id = '${req.params.deptId}' AND volumes.sub_department_id = '${req.params.subDeptId}'`
      Model.File.Query(fileQuery).then((f) => {
        res.send({
          data: {
            files: f.results
          }
        })
      })
    } else {
      Model.Volume.Query(`SELECT DISTINCT year from volumes WHERE sub_department_id = "${req.params.subDeptId}"`).then((yrs) => {
        res.send({
          data: yrs.results
        })
      })
    }
  },

  volume: function (req, res) {
    if (req.query.subject) {
      const subjectLike = Model.File.searchQ('subject', req.query.subject)
      const byFolioNumber = `files.folio_number = "${req.query.subject}"`
      const fileQuery = Model.queries.fileBaseQuery + ' WHERE (' + subjectLike + ` OR ${byFolioNumber}) AND sub_departments.department_id = '${req.params.deptId}' AND volumes.sub_department_id = '${req.params.subDeptId}' AND volumes.year = '${req.params.year}'`
      Model.File.Query(fileQuery).then((f) => {
        res.send({
          data: {
            files: f.results
          }
        })
      })
    } else {
      Model.Volume.fetch({
        sub_department_id: req.params.subDeptId,
        year: req.params.year,
        name: { $ne: '-' }
      }).then((vol) => {
        res.send({
          data: {
            volumes: vol,
            department_id: req.params.deptId,
            sub_department_id: req.params.sub_department_id
          }
        })
      })
    }
  },

  files: function (req, res) {
    console.log('HEELOW1:', req.query)

    if (req.query.subject) {
      const subjectLike = Model.File.searchQ('subject', req.query.subject)
      const byFolioNumber = `files.folio_number = "${req.query.subject}"`
      let fileQuery = ''
      if (req.query.outgoing) { // search outgoing files
        console.log('OUTGOING DOCUMENTS')
        fileQuery = Model.queries.outgoingFileBaseQuery + ' WHERE (' + subjectLike + ` OR ${byFolioNumber}) AND sub_departments.department_id = '${req.params.deptId}' AND volumes.id = '${req.params.volId}'`
      } else { // search archive files
        fileQuery = Model.queries.fileBaseQuery + ' WHERE (' + subjectLike + ` OR ${byFolioNumber}) AND sub_departments.department_id = '${req.params.deptId}' AND volumes.id = '${req.params.volId}'`
      }
      Model.File.Query(fileQuery).then((f) => {
        res.send({
          data: {
            files: f.results
          }
        })
      })
    } else {
      const where = ` 
          WHERE departments.id = "${req.params.deptId}" AND 
          sub_departments.id = "${req.params.subDeptId}" AND
          volumes.id = "${req.params.volId}"
      `
      let fileQuery = ''
      if (req.query.outgoing) { // fetch outgoing docs
        fileQuery = Model.queries.outgoingFileBaseQuery + where
      } else { // fetch archive file
        fileQuery = Model.queries.fileBaseQuery + where
      }
      Model.File.Query(fileQuery).then((f) => {
        res.send({
          data: {
            files: f.results
          }
        })
      })
    }
  },

  file: async function (req, res) {
    console.log('HEELOW:', req.query)
    if (req.query.outgoing) { // fetch outgoing file
      const fileSql = `${Model.queries.outgoingFileBaseQuery} WHERE outgoing_docs.id = "${req.params.fileId}"`
      let file = (await Model.File.Query(fileSql)).results[0] // fetch file and its parameters

      // fetch file archive details
      const department = (await Model.Department.get({ id: file.department }))[0]
      const subDepartment = (await Model.Subdepartment.get({ id: file.sub_department }))[0]
      const volume = (await Model.Volume.get({ id: file.volume }))[0]

      file['department'] = department.toJson()
      file['subdepartment'] = subDepartment.toJson()
      file['volume'] = volume.toJson()

      // sendfile
      res.send(file)
    } else { // fetch achive file
      const fileSql = `${Model.queries.fileBaseQuery} WHERE files.id = "${req.params.fileId}"`
      const threadSql = `
        SELECT thread.type, thread.comment, thread.created_at, users.firstname, users.middlename,users.surname, users.email,
        signatures.initial, appointments.name, appointments.color from thread 
        INNER JOIN users ON users.id = thread.user_id 
        INNER JOIN signatures ON signatures.user_id = users.id 
        INNER JOIN appointments ON appointments.id = users.appointment 
      WHERE thread.file_id = "${req.params.fileId}"
      `
      const stampSql = `
        SELECT stamps.date, signatures.path, appointments.name from stamps 
        INNER JOIN signatures ON signatures.id = stamps.signature_id 
        INNER JOIN users ON users.id = signatures.user_id 
        INNER JOIN appointments ON appointments.id = users.appointment 
        WHERE stamps.file_id = "${req.params.fileId}"
      `
      const recipientSql = `
        SELECT recipients.file_id, recipients.color, recipients.commented, users.id, users.firstname, users.middlename, users.surname, users.email, appointments.name as appointment, appointments.color as profileColor from recipients
        INNER JOIN users ON users.id = recipients.user_id
        INNER JOIN appointments ON appointments.id = users.appointment
      WHERE recipients.file_id = "${req.params.fileId}"
      `

      // fetch file and its parameters
      let file = (await Model.File.Query(fileSql)).results[0]

      // fetch file archive details
      const department = (await Model.Department.get({ id: file.department }))[0]
      const subDepartment = (await Model.Subdepartment.get({ id: file.sub_department }))[0]
      const volume = (await Model.Volume.get({ id: file.volume }))[0]

      const thread = await Model.File.Query(threadSql)
      const stamp = await Model.File.Query(stampSql)
      const recipients = (await Model.File.Query(recipientSql))

      // fetch draft info
      let draft = await draftHelper.getDraftByFileId(req.params.fileId)

      // fetch outgoing doc (if available)
      let outgoing = await Model.OutgoingDocs.get({
        file_id: req.params.fileId
      })
      outgoing = outgoing[0]
      // console.log(d, draft)

      file['thread'] = thread.results
      file['stamp'] = stamp.results
      file['recipients'] = recipients.results
      file['department'] = department.toJson()
      file['subdepartment'] = subDepartment.toJson()
      file['volume'] = volume.toJson()
      file['draft'] = draft
      file['outgoing'] = outgoing ? `${outgoing.id}_${outgoing.file_id}.pdf` : ''

      // sendfile
      res.send(file)
    }
  },

  upload: function (req, res) {
    // this function uploads a from the secretariate into the archive
    archiveUpload(req, res, async function (err, filename) {
      if (err) {
        return res.end('Something went wrong:( -' + err.message)
      }

      logger.info('MFileName: ', req.files[0].filename, JSON.stringify(req.body))
      const f = (await Model.File.get({ folio_number: req.body.folio_number, volume_id: req.body.volume }))[0]
      logger.info('InsertId:', f.id)

      // respond to caller
      res.end(JSON.stringify({
        data: {
          fileId: f.id
        },
        success: true
      }))
    })
  },

  signDocument: async function (req, res) {
    const signature = req.body.signature
    const fileId = req.body.file_id

    if (signature) {
      if (fileId) {
        const r = await Model.Stamp.get({ signature_id: signature, file_id: fileId })
        if (r.length > 0) { // document already signed
          res.send({
            success: true,
            data: 'document already signed'
          })
        } else { // sign the document
          Model.Stamp.create({ signature_id: signature, file_id: fileId })
            .then((success) => {
              res.send({
                success: true,
                message: 'document signed'
              })
            })
        }
      } else {
        res.send({
          success: false,
          message: 'File Id is empty'
        })
      }
    } else {
      res.send({
        success: false,
        message: 'Signature Id is empty'
      })
    }
  },

  createArchive: async function (req, res) {
    // console.log('createArchive:', await archiveHelper.isDirectory(config.server.archiveRootURL))
    const depth = []
    if (req.params.deptNumber) depth[0] = req.params.deptNumber.trim()
    if (req.params.subDeptNumber) depth[1] = req.params.subDeptNumber.trim()
    if (req.params.year) depth[2] = req.params.year.trim()
    if (req.params.volume) depth[3] = req.params.volume.trim()

    const itemName = req.body.name || req.body.year
    const itemNumber = req.body.number
    let renameFile = false
    let oldP = ''

    if (await archiveHelper.pathExists(config.server.archiveRootURL) && await archiveHelper.isDirectory(config.server.archiveRootURL)) {
      let p = config.server.archiveRootURL + '/'
      for (let i = 0; i < depth.length - 1; i++) {
        p += `${depth[i]}`
        if (await archiveHelper.pathExists(p) && await archiveHelper.isDirectory(p)) {
          p += '/'
        } else {
          logger.warn(`path:, ${p} does not exist`)
          res.send({
            success: false
          })
          return
        }
      }

      switch (depth.length) {
        case 1:
          const dept = await Model.Department.get({ $or: [{ number: itemNumber }, { number: itemNumber + '\n' }] })
          if (dept.length > 0) { // modify name
            await Model.Department.update({
              set: {
                name: itemName
              },
              queryKey: { $or: [{ number: itemNumber }, { number: itemNumber + '\n' }] }
            })
            p += depth[depth.length - 1] // use old number
            console.log('department already exist')
          } else {
            console.log('creating department')
            await Model.Department.create({
              name: itemName,
              number: itemNumber
            })
            p += itemNumber // use new number
          }
          break

        case 2:
          const d = await Model.Department.get({ $or: [{ number: depth[0] }, { number: depth[0] + '\n' }] })
          const subDept = await Model.Subdepartment.get({ $or: [{ number: itemNumber }, { number: itemNumber + '\n' }], department_id: d[0].id })

          if (subDept.length > 0) { // modify name
            await Model.Subdepartment.update({
              set: {
                name: itemName
              },
              queryKey: { $or: [{ number: itemNumber }, { number: itemNumber + '\n' }], department_id: d[0].id }
            })
            p += depth[depth.length - 1] // use old number
            logger.warn('sub-department already exist')
          } else {
            logger.info('creating sub-department', d)
            await Model.Subdepartment.create({
              name: itemName,
              number: itemNumber,
              department_id: d[0].id
            })
            p += itemNumber // use new number
          }
          break

        case 3:
          const dpt = await Model.Department.get({ $or: [{ number: depth[0] }, { number: depth[0] + '\n' }] })
          const sd = await Model.Subdepartment.get({ $or: [{ number: depth[1] }, { number: depth[1] + '\n' }], department_id: dpt[0].id })
          const year = await Model.Volume.get({ year: depth[2], sub_department_id: sd[0].id })
          if (year.length > 0) { // modify name
            await Model.Volume.update({
              set: {
                year: itemName
              },
              queryKey: { year: depth[2], sub_department_id: sd[0].id }
            })
            // p += itemName //
            oldP = p + depth[depth.length - 1] // use old name
            renameFile = true
            logger.warn('year already exist')
          } else {
            logger.info('creating year')
            await Model.Volume.create({
              name: '-',
              year: depth[2],
              sub_department_id: sd[0].id
            })
            p += itemName // use new name
          }
          break

        case 4:
          const dp = await Model.Department.get({ $or: [{ number: depth[0] }, { number: depth[0] + '\n' }] })

          const subd = await Model.Subdepartment.get({ $or: [{ number: depth[1] }, { number: depth[1] + '\n' }], department_id: dp[0].id })
          const vol1 = await Model.Volume.get({ year: depth[2], name: depth[3], sub_department_id: subd[0].id })
          const vol2 = await Model.Volume.get({ year: depth[2], name: '-', sub_department_id: subd[0].id })
          if (vol1.length > 0) { // modify name
            await Model.Volume.update({
              set: {
                name: itemName
              },
              queryKey: { year: depth[2], sub_department_id: subd[0].id }
            })
            // p += itemName.toUpperCase() //
            oldP = p + depth[depth.length - 1] // use old name
            renameFile = true
            logger.warn('volume already exist')
          } else if (vol2.length > 0) {
            await Model.Volume.update({
              set: {
                name: itemName.toUpperCase()
              },
              queryKey: { year: depth[2], name: '-', sub_department_id: subd[0].id }
            })
            p += depth[depth.length - 1].toUpperCase() // use name
          } else {
            logger.info('creating volume')
            await Model.Volume.create({
              name: itemName,
              year: depth[2],
              sub_department_id: subd[0].id
            })
            p += itemName.toUpperCase() // use new name
          }
          break
      }
      if (!renameFile) {
        archiveHelper.pathExists(p, true) // create the directory p
      } else {
        archiveHelper.moveFile(oldP, p, itemName)
      }
      logger.info('Path Modified ', p)
      res.send({
        success: true
      })
    } else {
      res.send({
        success: false
      })
    }
  },

  editFileSubject: async function (req, res) {
    const fileId = req.params.fileId
    if (!fileId || isNaN(fileId)) return res.send({ success: false, data: 'File Id empty' })
    await Model.File.update({
      set: {
        subject: req.body.subject
      },
      queryKey: {
        id: fileId
      }
    })
    res.send({
      success: true,
      data: 'Subject Edited'
    })
  },

  changeFileLocation: async function (req, res) {
    const oldDetail = req.body.oldDetail
    const newDetail = req.body.newDetail
    if (!oldDetail || !oldDetail.dept || !oldDetail.subdept || !oldDetail.year || !oldDetail.volume || !oldDetail.fileId || isNaN(oldDetail.fileId)) return res.send({ success: false, data: 'old file detail empty!' })
    if (!newDetail || !newDetail.dept || !newDetail.subdept || !newDetail.year || !newDetail.volume) return res.send({ success: false, data: 'new file detail empty!' })

    const dept = (await Model.Department.get({ id: oldDetail.dept }))[0]
    const dept2 = (await Model.Department.get({ id: newDetail.dept }))[0]
    const subdept = (await Model.Subdepartment.get({ id: oldDetail.subdept }))[0]
    const subdept2 = (await Model.Subdepartment.get({ id: newDetail.subdept }))[0]
    const vol = (await Model.Volume.get({ id: oldDetail.volume }))[0]
    const vol2 = (await Model.Volume.get({ id: newDetail.volume }))[0]
    const file = (await Model.File.get({ id: oldDetail.fileId }))[0]

    // construct old and new path
    let dir = config.server.archiveRootURL // the root folder where the archive is located
    let dir2 = config.server.archiveRootURL // the root folder where the archive is located
    let ext = ''
    // if (file.type === 'pdf') ext = '.pdf'
    dir = `${dir}/${dept.number.trim()}/${subdept.number.trim()}/${vol.year.trim()}/${vol.name.trim()}/${file.folio_number}${ext}`
    dir2 = `${dir2}/${dept2.number.trim()}/${subdept2.number.trim()}/${vol2.year.trim()}/${vol2.name.trim()}`
    logger.info(`dir: ${dir}`)
    logger.info(`dir: ${dir2}`)
    // use last folio number from new volume to create a new folio number
    const f = await Model.File.Query(`${Model.queries.fileBaseQuery} WHERE volumes.id = "${newDetail.volume}" ORDER BY files.id DESC LIMIT 1`)
    let fol = 1
    if (f.results.length > 0) {
      fol = Number(f.results[0].folio_number) + 1
    }

    // check if file exist physically
    if (await archiveHelper.pathExists(dir)) {
      // move file to new location
      if (!(await archiveHelper.pathExists(dir2))) return res.send({ success: false, data: 'New Dir does not exist' })
      if (await archiveHelper.moveFile(dir, dir2, fol)) {
        // update file folio and volume id
        await Model.File.update({
          set: {
            folio_number: fol,
            volume_id: vol2.id
          },
          queryKey: {
            id: oldDetail.fileId
          }
        })

        res.send({
          success: true,
          data: 'File moved successfully!!'
        })
      }
    } else {
      // update file folio and volume id
      await Model.File.update({
        set: {
          folio_number: fol,
          volume_id: vol2.id
        },
        queryKey: {
          id: oldDetail.fileId
        }
      })

      res.send({
        success: true,
        data: 'File moved successfully!!'
      })
    }
  },

  getLastFolioNumber: async function (req, res) {
    const volumeId = req.params.volId
    if (!volumeId || isNaN(volumeId)) return res.send({ success: false, data: 'Volume Id missing' })
    const f = await Model.File.Query(`${Model.queries.fileBaseQuery} WHERE volumes.id = "${volumeId}" ORDER BY files.id DESC LIMIT 1`)
    if (f.results.length > 0) {
      res.send({
        success: true,
        data: f.results[0].folio_number
      })
    } else {
      res.send({
        success: true,
        data: 0
      })
    }
  },

  uploadOutgoingDoc (req, res) {
    //
    outgoingDocsUpload(req, res, (err, filename) => {
      if (err) return res.send({ success: false, message: err.message })
      console.log(`file uploaded with name: ${filename}`)
      res.send({ success: true, message: 'File uploaded!!!' })
    })
  }
}

module.exports = controller
