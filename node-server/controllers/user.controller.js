const Model = require('../database/models')
const User = Model.User
const logger = require('../lib/log')
const archiveHelper = require('../lib/archiveHelper')
const config = require('../config')
const crypto = require('crypto')
const userCache = require('../lib/cacheHelper')

// const usersQuery = `SELECT users.id, users.firstname, users.surname, users.middlename, users.email,
//       appointments.name as appointment, appointments.color, signatures.path as signature, signatures.initial, signatures.id as signature_id FROM users
//       INNER JOIN appointments ON users.appointment = appointments.id
//       INNER JOIN signatures ON signatures.user_id = users.id
//       `

// ****************** multer configuration ******************
const multer = require('multer')
const fs = require('fs')
function signatureDest (req, file, callback) {
  var dir = config.server.signatureDir
  try {
    if (!fs.existsSync(dir)) {
      logger.info(`making a new dir for ${dir}`)
      fs.mkdirSync(dir)
    }
    callback(null, dir)
  } catch (err) {
    logger.error(err.message)
  }
}

function profilePicDest (req, file, callback) {
  var dir = config.server.profilePictureDir
  try {
    if (!fs.existsSync(dir)) {
      logger.info(`making a new dir for ${dir}`)
      fs.mkdirSync(dir)
    }
    callback(null, dir)
  } catch (err) {
    logger.error(err.message)
  }
}

var signatureStorage = multer.diskStorage({
  destination: signatureDest,
  filename: async function (req, file, callback) {
    // sanity check
    if (!req.body.userId || isNaN(req.body.userId)) return callback(new Error('User Id Missing'))

    // fetch user detail
    let user = (await Model.User.Query(`${Model.queries.usersQuery} WHERE users.id = "${req.body.userId}"`)).results
    if (user.length < 1) return callback(new Error('User Id Missing'))
    else user = user[0]
    const signatureName = `${user.appointment}-${user.id}`
    logger.success(`${signatureName}`)
    callback(null, `${signatureName}.jpg`)

    // TODO: update signature name in DB
  }
})

var profilePicStorage = multer.diskStorage({
  destination: profilePicDest,
  filename: async function (req, file, callback) {
    // sanity check
    if (!req.body.userId || isNaN(req.body.userId)) return callback(new Error('User Id Missing'))
    logger.success(`PROFILE PIC NAME: ${req.body.userId}`)
    callback(null, `${req.body.userId}`)
    // TODO: update db
  }
})

var signatureUpload = multer({ storage: signatureStorage }).array('file', 12)
var profilePicUpload = multer({ storage: profilePicStorage }).array('file', 12)
// **************** End of Multer config *******************

const controller = {
  getUsers: async function (req, res) {
    User.Query(`${Model.queries.usersQuery} WHERE users.active = "1" ORDER BY appointments.name ASC`).then(async (users) => {
      let usrs = users.results
      let usrsById = {}
      for (let u of usrs) {
        let e = await archiveHelper.pathExists(`${config.server.profilePictureDir}/${u.id}`)
        u.profilePic = e
        usrsById[u.id] = u
      }

      res.send({
        success: true,
        data: {
          byId: usrsById,
          normal: usrs
        }
      })
    }).catch((err) => {
      res.send({
        success: false,
        data: err.message
      })
    })
  },

  getUser: function (req, res) {
    User.Query(`${Model.queries.usersQuery} WHERE users.active = "1" AND users.id = "${req.params.userId}"`).then((user) => {
      res.send({
        success: true,
        data: user.results[0]
      })
    }).catch((err) => {
      res.send({
        success: false,
        data: err.message
      })
    })
  },

  appointments: function (req, res) {
    Model.Appointment.fetch().then((apps) => {
      res.send({
        success: true,
        data: apps
      })
    }).catch((err) => {
      res.send({
        success: false,
        data: err.message
      })
    })
  },

  register: async function (req, res) {
    const email = req.body.email
    const appointmentId = req.body.appointment
    const password1 = req.body.password
    const password2 = req.body.password2

    if (!email) return res.send({ success: false, data: 'No email specified' })
    if (!appointmentId) return res.send({ success: false, data: 'Please select appointment' })
    if (password1 !== password2) return res.send({ success: false, data: 'Passwords do not match' })

    const users = await Model.User.get({ email: email, active: 1 })
    if (users.length > 0) { // active account already exist for this user...
      users[0].set('active', 0) // deactivate old user
      await users[0].save()
    }

    // add new user
    let passHashed = crypto.createHash('md5').update(password1).digest('hex')
    const userId = await Model.User.create({
      email: email,
      password: passHashed,
      appointment: appointmentId
    })

    if (userId) {
      const appointment = (await Model.Appointment.get({ id: appointmentId }))[0]
      await Model.Signature.create({
        path: `${appointment.name.toUpperCase()}-${userId}.jpg`,
        user_id: userId
      })
      res.send({
        success: true,
        data: 'registration successful'
      })
    } else {
      res.send({
        success: false,
        data: 'Registration Failed'
      })
    }
  },

  login: async function (req, res) {
    const email = req.body.email
    const password = req.body.password

    // hash the password
    let passHashed = crypto.createHash('md5').update(password).digest('hex')
    let user = await Model.User.get({ email: email, active: 1 })
    if (user.length === 0) return res.send({ success: false, data: 'Incorrect Email Address' })
    user = user[0]
    if (user.password !== passHashed) return res.send({ success: false, data: 'Incorrect Password' })

    // get user details
    let userData = await Model.User.Query(`${Model.queries.usersQuery} WHERE users.active = "1" AND users.id = "${user.id}"`)
    // console.log('userDATA:', userData, `${Model.queries.usersQuery} WHERE users.active = "1" AND users.id = "${user.id}"`)
    if (!userData.results.length) return res.send({ success: false, data: 'Invalid Login' })
    userData = userData.results[0]
    res.send({
      success: true,
      data: userData
    })
  },

  updateProfile: async function (req, res) {
    const firstName = req.body.firstname
    const middleName = req.body.middlename
    const surname = req.body.surname
    let password = req.body.password
    const initial = req.body.initial
    const userId = req.body.user_id

    if (!password) {
      await Model.User.update({
        set: {
          firstname: firstName,
          surname: surname,
          middlename: middleName
        },
        queryKey: {
          id: userId
        }
      })
    } else {
      password = crypto.createHash('md5').update(password).digest('hex')
      await Model.User.update({
        set: {
          firstname: firstName,
          surname: surname,
          middlename: middleName,
          password: password
        },
        queryKey: {
          id: userId
        }
      })
    }
    // update initials table
    await Model.Signature.update({
      set: {
        initial: initial
      },
      queryKey: {
        user_id: userId
      }
    })

    res.send({
      success: true,
      data: 'Profile updated!!!'
    })
  },

  manageInbox: async function (req, res) {
    const userId = req.body.userId
    const inboxType = req.body.type // 1 => regular, 2 => incom sig, 3 => outgoing sig... 6 => all signals, 7 => all inbox
    const action = req.body.action // 1 => clear inbox, 2 => empty recyle bin
    if (!userId || isNaN(userId)) return res.send({ success: false, data: 'User Id missing' })
    if (!inboxType) return res.send({ success: false, data: 'Inbox type missing' })
    if (!action || isNaN(action)) return res.send({ success: false, data: 'Specify an action to be performed' })

    // fetch user detail
    const user = (await Model.User.get({ id: userId }))[0]
    const users = await Model.User.get({ appointment: user.appointment }) // fetch all users with same appointment
    const cache = await userCache.getCache(userId) // get user cache

    let commited
    let set = {}

    if (action === '1') { // clear inbox
      commited = [{ commited: 0, trashed: 0 }, { commited: 0, trashed: 0 }]
      set['commited'] = 1
    } else if (action === '2') { // empty recycle bin
      commited = [{ commited: 1, trashed: 1 }, { commited: 0, trashed: 1 }]
      set['trashed'] = 2 // two means deleted
    } else if (action === '3') { // restore recycled messages
      commited = [{ commited: 1, trashed: 2 }, { commited: 0, trashed: 2 }]
      set['trashed'] = 1 // 1 means trashed
    }

    // console.log('usrs:', JSON.stringify(users))

    switch (inboxType) {
      case '1':
        for (let u of users) {
          await Model.Inbox.update({
            set,
            queryKey: {
              user_id: u.id,
              $or: commited,
              '$or-1': [{ type: 0 }, { type: 1 }]
            }
          })
        }
        break

      case '2':
        for (let u of users) {
          await Model.Inbox.update({
            set,
            queryKey: {
              user_id: u.id,
              $or: commited,
              type: 2
            }
          })
        }
        // update cache
        cache['inboxCount']['cRegularUnread'] = 0
        await userCache.updateCache(userId, cache, true)
        break

      case '3':
        for (let u of users) {
          await Model.Inbox.update({
            set,
            queryKey: {
              user_id: u.id,
              $or: commited,
              type: 3

            }
          })
        }
        break

      case '4':
        for (let u of users) {
          await Model.Inbox.update({
            set,
            queryKey: {
              user_id: u.id,
              $or: commited,
              type: 4

            }
          })
        }
        break

      case '5':
        for (let u of users) {
          await Model.Inbox.update({
            set,
            queryKey: {
              user_id: u.id,
              $or: commited,
              type: 5

            }
          })
        }
        break

      case '6':
        for (let u of users) {
          await Model.Inbox.update({
            set,
            queryKey: {
              user_id: u.id,
              $or: commited,
              '$or-1': [{ type: 2 }, { type: 3 }, { type: 4 }, { type: 5 }]

            }
          })
        }
        break

      case '7':
        for (let u of users) {
          await Model.Inbox.update({
            set,
            queryKey: {
              user_id: u.id,
              $or: commited

            }
          })
        }
        break
    }
    // refresh cache
    await userCache.removeCache(userId, true)

    res.send({
      success: true,
      data: 'Inbox updated Successfully'
    })
  },

  uploadProfilePic: function (req, res) {
    profilePicUpload(req, res, async function (err, filename) {
      if (err) {
        return res.end(JSON.stringify({
          data: `Unable to upload Profile picture - ${err.message}`,
          success: false
        }))
      }

      // respond to caller
      res.end(JSON.stringify({
        data: 'Profile pic uploaded',
        success: true
      }))
    })
  },

  uploadSignature: function (req, res) {
    signatureUpload(req, res, async function (err, filename) {
      if (err) {
        return res.end('Something went wrong:( -' + err.message)
      }

      // respond to caller
      res.end(JSON.stringify({
        data: 'signature uploaded',
        success: true
      }))
    })
  }
}

module.exports = controller
