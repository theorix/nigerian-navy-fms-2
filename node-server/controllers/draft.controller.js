const Model = require('../database/models')
const logger = require('../lib/log')
const archiveHelper = require('../lib/archiveHelper')
const draftHelper = require('../lib/draftHelper')
const config = require('../config')

const mammoth = require('mammoth')
// const docx2html = require('docx2html')
const docManager = require('../lib/docManager')
const HTMLDocx = require('html-docx-js')
/**
IMPORTANT: please pass a complete, valid HTML (including DOCTYPE, html and body tags) to the mammoth library.
This may be less convenient, but gives you possibility of including CSS rules in style tags.
**/

// ****************** multer configuration file upload ******************
const multer = require('multer')
const fs = require('fs')
const path = require('path')

async function destFinalDocs (req, file, callback) {
  let draftType = req.body.draftType
  let draftId = req.body.draftId
  let userId = req.body.userId
  if (!draftId || isNaN(draftId)) return callback(new Error('Invalid Draft ID'))
  let user = await Model.User.Query(`${Model.queries.usersQuery} WHERE users.id = "${userId}"`)
  user = user.results[0]

  let dir = ''
  if (draftType !== 'formal') dir = `${config.server.draftFinalDocsDir}/USER/${user.appointment}/draft_${draftId}`
  else dir = `${config.server.draftFinalDocsDir}/ARCHIVE/draft_${draftId}`

  try {
    if (!fs.existsSync(dir)) {
      logger.info(`making a new dir for ${dir}`)
      fs.mkdirSync(dir)
    }
    callback(null, dir)
  } catch (err) {
    logger.error(err.message)
  }
}

// function destNewVersion (req, file, callback) {
//   let dir = `${config.server.draftFinalDocsDir}/temp` // the folder for storing Draft docs
//   try {
//     if (!fs.existsSync(dir)) {
//       logger.info(`making a new dir for ${dir}`)
//       fs.mkdirSync(dir)
//     }
//     callback(null, dir)
//   } catch (err) {
//     logger.error(err.message)
//   }
// }

function tempHandlerDest (req, file, callback) {
  let dir = `${config.server.draftFinalDocsDir}/temp` // draft temporary folder
  try {
    if (!fs.existsSync(dir)) {
      logger.info(`making a new dir for ${dir}`)
      fs.mkdirSync(dir)
    }
    callback(null, dir)
  } catch (err) {
    logger.error(err.message)
  }
}

const draftFinalDocs = multer.diskStorage({
  destination: destFinalDocs,
  async filename (req, file, callback) {
    // sanity checks
    if (!req.body.draftId || isNaN(req.body.draftId)) return callback(new Error('Invalid Draft ID'))

    // update database record
    await Model.Draft.update({
      set: {
        final_copy: `${new Date().toDateString()}_${req.body.userId}`
      },
      queryKey: {
        id: req.body.draftId
      }
    })
    // if (!fileId) return callback(new Error(`Unable to create record for file ${req.body.fileId}`))

    // finish off
    callback(null, `${req.body.draftId}.pdf`)
  }
})

const draftNewVersion = multer.diskStorage({
  destination: tempHandlerDest,
  async filename (req, file, callback) {
    // sanity checks
    // if (!req.body.draftId || isNaN(req.body.draftId)) return callback(new Error('Invalid Draft ID'))
    const ext = path.extname(file.originalname)
    console.log(' FILENAME:', ext)

    if (ext !== '.doc' && ext !== '.docx') callback(new Error('Invalid file type'))
    else callback(null, `${new Date().getMilliseconds()}_temp.docx`)
  }
})

const createNewDraftHandler = multer.diskStorage({
  destination: tempHandlerDest,
  async filename (req, file, callback) {
    callback(null, `${new Date().getMilliseconds()}_temp.odt`)
  }
})

// const addTagHandler = multer.diskStorage({
//   destination: tempHandlerDest,
//   async filename (req, file, callback) {
//     callback(null, `${new Date().getMilliseconds()}_temp.odt`)
//   }
// })

const draftFinalDocsUpload = multer({ storage: draftFinalDocs }).array('file', 12)
const draftNewVersionUpload = multer({ storage: draftNewVersion }).array('file', 12)
const createNewDraftUpload = multer({ storage: createNewDraftHandler }).array('file', 12)
// const addDraftTagUpload = multer({ storage: addTagHandler }).array('file', 12)

// **************** End of Multer config *******************

async function fetchInbox (userId, limit = -1) {
  let receiver = await Model.User.Query(`${Model.queries.usersQuery} WHERE users.id = "${userId}"`)
  let draftSequence
  if (limit > 0) {
    draftSequence = await Model.DraftSequence.fetch({
      receiver_id: userId,
      $ext: {
        $limit: limit,
        $orderby: { key: 'id', order: 'DESC' }
      }
    })
  } else {
    draftSequence = await Model.DraftSequence.fetch({
      receiver_id: userId,
      $ext: {
        $orderby: { key: 'id', order: 'DESC' }
      }
    })
  }

  let inbox = []
  // console.log(`Sequence:${draftSequence.length}, user: ${userId}, limit: ${limit}`)

  for (let s of draftSequence) {
    let item = {}
    let draftVersion = await Model.DraftVersions.fetch({ id: s.version_id })
    let draft = await Model.Draft.fetch({ id: draftVersion[0].draft_id })
    let sender = await Model.User.Query(`${Model.queries.usersQuery} WHERE users.id = "${s.sender_id}"`)
    item['sender'] = sender.results[0]
    item['receiver'] = receiver.results[0]
    item['draftVersion'] = draftVersion[0]
    item['draftId'] = draft[0].id
    item['fileId'] = draft[0].file_id
    item['draftType'] = draft[0].type
    item['subject'] = draft[0].subject
    item['created_at'] = s.created_at
    item['commited'] = s.commited
    item['id'] = s.id

    inbox.push(item)
  }

  return inbox
}

async function createDraft (options) {
  let fileId = options.fileId
  let userId = options.userId
  let doc = options.document
  let subject = options.subject
  let type = options.type
  let extraInfo = options.extraInfo ? JSON.stringify(options.extraInfo) : '{}'

  // sanity checks
  if (!fileId && type === 'formal') return { success: false, message: 'Invalid file Id' }
  if (!userId) return { success: false, message: 'Invalid user Id' }
  if (!doc) return { success: false, message: 'Draft cannot be empty' }

  let draftPath = ''
  let user = await Model.User.Query(`${Model.queries.usersQuery} WHERE users.id = "${userId}"`)
  user = user.results[0]
  if (!user) return { success: false, message: 'Invalid user Id' }

  // configure draft path based on draft type
  if (type && type !== 'formal') {
    fileId = `Draft_${new Date().getTime()}_${userId}`
    draftPath = `${config.server.draftFinalDocsDir}`
    if (!(await archiveHelper.pathExists(draftPath, true))) return { success: false, message: 'File system Error' }
    draftPath += `/USER`
    if (!(await archiveHelper.pathExists(draftPath, true))) return { success: false, message: 'File system Error' }
    draftPath += `/${user.appointment}`
  } else {
    draftPath = `${config.server.draftFinalDocsDir}`
    if (!(await archiveHelper.pathExists(draftPath, true))) return { success: false, message: 'File system Error' }
    draftPath += `/ARCHIVE`
  }

  if (!(await archiveHelper.pathExists(draftPath, true))) return { success: false, message: 'File system Error' }

  // check for duplicate
  let d = await Model.Draft.get({ file_id: fileId })
  if (d.length > 0) return { success: false, message: `Draft with file Id ${fileId} already exists` }

  // start db transaction here
  const transaction = await Model.Draft.startTransaction()
  let draftId = await Model.Draft.create({
    file_id: fileId,
    participants: '{}', // json string in the format: { 4: [2,5,3], 5: [3, 8, 1], 3: []}
    user_id: userId,
    type,
    subject,
    extra_info: extraInfo
  })
  if (draftId) {
    draftPath += `/draft_${draftId}`
    if (!(await archiveHelper.pathExists(draftPath, true))) return { success: false, message: 'File system Error' }

    let versionId = await Model.DraftVersions.create({ draft_id: draftId, version_number: 1, document: `document_${fileId}.odt` })
    if (versionId) {
      draftPath += `/version_1`
      if (!(await archiveHelper.pathExists(draftPath, true))) {
        transaction.rollback()
        return { success: false, message: 'File system Error' }
      }
      let draftTagPath = draftPath
      draftPath += `/document_${fileId}.odt` // append filename for main draft
      draftTagPath += `/tagged_document_${fileId}.odt` // append filename for tagged_doc
      const docUrl = (path.resolve(draftPath).split(config.appName))[1]
      const tagUrl = (path.resolve(draftTagPath).split(config.appName))[1]
      if (await docManager.saveFile(draftPath, doc) && await docManager.saveFile(draftTagPath, doc)) {
        await transaction.commit()
        return { success: true, data: { draftId, versionId, docUrl, tagUrl }, message: 'Draft created' }
      } else {
        await transaction.rollback()
        return { success: false, message: 'Unable to create draft - file system error' }
      }
    } else {
      await transaction.rollback()
      return { success: false, message: 'Unable to create draft' }
    }
  } else {
    await transaction.rollback()
    return { success: false, message: 'Failed to create draft' }
  }
}
async function updateVersion (options) {
  console.log('oppp:', options)
  let draftId = options.draftId
  let fileId = options.fileId
  let versionNumber = options.versionNumber
  let doc = options.newDocumentVersion
  let subject = options.subject
  let userId = options.userId
  let type = options.draftType

  let extraInfo = '{}'
  if (options.extraInfo && typeof options.extraInfo === 'string') extraInfo = options.extraInfo
  else if (options.extraInfo) extraInfo = JSON.stringify(options.extraInfo)

  // sanity checks
  if (!draftId) return { success: false, message: 'Invalid draft Id' }
  if (!fileId) return { success: false, message: 'Invalid File Id' }
  if (!userId) return { success: false, message: 'Invalid user Id' }
  if (!versionNumber || isNaN(versionNumber)) return { success: false, message: 'Invalid draft version' }
  if (!doc) return { success: false, message: 'Document must not be empty' }

  // configure draft path based on draft type
  let user = await Model.User.Query(`${Model.queries.usersQuery} WHERE users.id = "${userId}"`)
  user = user.results[0]
  let draftPath = ''
  if (type && type !== 'formal') {
    draftPath = `${config.server.draftFinalDocsDir}/USER/${user.appointment}/draft_${draftId}`
  } else {
    draftPath = `${config.server.draftFinalDocsDir}/ARCHIVE/draft_${draftId}`
  }

  // update draft subject matter
  await Model.Draft.update({
    set: {
      subject,
      extra_info: extraInfo
    },
    queryKey: {
      id: draftId
    }
  })

  // update the document if correction have not been made on current version
  let prev = await Model.DraftVersions.fetch({ draft_id: draftId, version_number: versionNumber })
  prev = prev[0]
  if (!prev.tagged_document) {
    const taggedDocPath = `${draftPath}/version_${versionNumber}/tagged_document_${fileId}.odt`
    draftPath += `/version_${versionNumber}/document_${fileId}.odt`
    const docUrl = (path.resolve(draftPath).split(config.appName))[1]
    const tagUrl = (path.resolve(taggedDocPath).split(config.appName))[1]
    const success = await docManager.saveFile(draftPath, doc)
    const success2 = await docManager.saveFile(taggedDocPath, doc)
    if (success && success2) return { success: true, message: 'Draft Updated', data: { docUrl, tagUrl } }
    else return { success: false, message: 'Draft Update failed' }
  }

  // start db transaction
  const transaction = await Model.Draft.startTransaction()

  // well, correction have been made to this current draft version... simply create a new version
  let versionId = await Model.DraftVersions.create({
    draft_id: draftId,
    version_number: Number(versionNumber) + 1,
    document: `document_${fileId}.odt`
  })

  if (versionId) {
    draftPath += `/version_${Number(versionNumber) + 1}`
    if (!await archiveHelper.pathExists(draftPath, true)) {
      await transaction.rollback()
      return { success: false, message: 'Error updating draft version' }
    }
    const taggedDocPath = `${draftPath}/tagged_document_${fileId}.odt`
    draftPath += `/document_${fileId}.odt`
    const docUrl = (path.resolve(draftPath).split(config.appName))[1]
    const tagUrl = (path.resolve(taggedDocPath).split(config.appName))[1]
    if (await docManager.saveFile(draftPath, doc) && await docManager.saveFile(taggedDocPath, doc)) {
      await transaction.commit()
      return { success: true, message: 'Draft version updated', data: { docUrl, tagUrl } }
    } else {
      await transaction.rollback()
      return { success: false, message: 'Error updating draft version - file system error' }
    }
  } else return { success: false, message: 'Error updating draft version' }
}

const controller = {
  create: async function (req, res) {
    createNewDraftUpload(req, res, async (err) => {
      if (err) return res.send({ success: false, message: err.message })

      // console.log(`file uploaded with name: ${req.files[0].filename}, ${err}, ${JSON.stringify(req.body)}`)
      const filePath = `${config.server.draftFinalDocsDir}/temp/${req.files[0].filename}`
      try {
        const doc = await docManager.readFile(filePath, null, true)
        let body = req.body
        body.document = doc
        const resp = await createDraft(req.body)
        res.send(resp)
      } catch (err) {
        res.send({ success: false, message: 'Error while creating draft' })
      }
    })
  },

  addTag: async function (req, res) {
    createNewDraftUpload(req, res, async (err) => {
      if (err) return res.send({ success: false, message: err.message })

      let versionId = req.body.versionId
      let userId = req.body.userId

      // sanity checks
      if (!userId) return res.send({ success: false, message: 'Invalid user Id' })
      if (!versionId) return res.send({ success: false, message: 'Invalid version Number Id' })

      // console.log(`file uploaded with name: ${req.files[0].filename}, ${err}, ${JSON.stringify(req.body)}`)
      const filePath = `${config.server.draftFinalDocsDir}/temp/${req.files[0].filename}`
      try {
        const doc = await docManager.readFile(filePath, null, true)

        let v = await Model.DraftVersions.fetch({ id: versionId })
        let user = await Model.User.Query(`${Model.queries.usersQuery} WHERE users.id = "${userId}"`)
        user = user.results[0]
        v = v[0]
        let draft = await Model.Draft.fetch({ id: v.draft_id })
        draft = draft[0]

        let draftPath = ''
        if (draft.type !== 'formal') {
          draftPath = `${config.server.draftFinalDocsDir}/USER/${user.appointment}/draft_${draft.id}`
        } else {
          draftPath = `${config.server.draftFinalDocsDir}/ARCHIVE/draft_${draft.id}`
        }
        draftPath += `/version_${v.version_number}/tagged_document_${draft.file_id}.odt`

        const success1 = await Model.DraftVersions.update({
          set: {
            tagged_document: 1
          },
          queryKey: {
            id: versionId
          }
        })
        const success2 = await docManager.saveFile(draftPath, doc)
        if (success1 && success2) res.send({ success: true, message: 'Tagged document saved' })
        else res.send({ success: false, message: 'Error while saving draft tags' })
      } catch (err) {
        console.log('error:', err.message)
        res.send({ success: false, message: 'Error while saving tags' })
      }
    })
  },

  updateVersion: async function (req, res) {
    createNewDraftUpload(req, res, async (err) => {
      if (err) return res.send({ success: false, message: err.message })

      const filePath = `${config.server.draftFinalDocsDir}/temp/${req.files[0].filename}`
      try {
        const doc = await docManager.readFile(filePath, null, true)
        let body = req.body
        body.newDocumentVersion = doc
        const resp = await updateVersion(req.body)
        res.send(resp)
      } catch (err) {
        res.send({ success: false, message: 'Error while Updating draft' })
      }
    })
  },

  getDraft: async function (req, res) {
    let draftId = req.params.draftId
    if (!draftId) return res.send({ success: false, message: 'Invalid draft Id' })

    let draft = ''
    let d = await Model.Draft.fetch({ id: draftId })
    if (d.length > 0) {
      d = d[0]
      d['finalCopy'] = d.final_copy ? `${d.id}.pdf` : ''
      d['extra_info'] = d.extra_info ? JSON.parse(d.extra_info) : ''
      let v = await Model.DraftVersions.fetch({ draft_id: d.id })
      v = v.map((i) => {
        i.tags = JSON.parse(i.tags)
        return i
      })
      draft = {
        info: d,
        versions: v
      }
    }
    res.send({ success: true, data: { draft } })
  },

  getByUserId: async function (req, res) {
    console.log(config)

    let userId = req.params.userId
    let draftId = req.params.draftId
    if (!userId) return res.send({ success: false, message: 'Invalid User Id' })
    if (!draftId) {
      let d = await Model.Draft.fetch({ user_id: userId, $or: [{ type: 'informal' }, { type: 'signal' }] })
      res.send({ success: true, data: d })
    } else {
      const draft = await draftHelper.getDraftById(draftId)
      res.send({ success: true, data: { draft } })
    }
  },

  inbox: async function (req, res) {
    let userId = req.params.userId
    // fetch receiver user object
    let inbox = await fetchInbox(userId)
    console.log('DRAFT INBOX:', userId, inbox.length)
    res.send({ messages: inbox, type: 'draft' })
  },

  recentMessages: async function (req, res) {
    let userId = req.params.userId
    // fetch receiver user object
    let inbox = await fetchInbox(userId, 3)
    // console.log('DRAFT INBOX:', inbox)
    res.send({ data: inbox, type: 'draft' })
  },

  countSequence: async function (req, res) {
    let userId = req.query.userId
    let all = await Model.DraftSequence.count({
      receiver_id: userId
    })
    let unread = await Model.DraftSequence.count({
      receiver_id: userId,
      commited: 0
    })

    res.send({
      all,
      unread
    })
  },

  approveDraft: async function (req, res) {
    let user = req.body.user
    let draftId = req.body.draftId
    let draft = await Model.Draft.get({ id: draftId })
    draft = draft[0]
    draft.status = 1
    draft.approved_by = `${user.id}-${user.appointment}`
    draft.save().then((success) => {
      console.log(success)
      if (success) res.send({ success, message: 'Draft Approved' })
      else res.send({ success, message: 'Unable to approve draft' })
    })
  },

  markAsRead: async function (req, res) {
    let idList = req.body.idList
    for (let id of idList) {
      if (id) {
        await Model.DraftSequence.update({
          set: {
            commited: 1
          },
          queryKey: {
            id
          }
        })
      }
    }
    res.send({
      success: true,
      message: 'Messages marked as read'
    })
  },

  // draft uploads
  upload (req, res) {
    //
    draftFinalDocsUpload(req, res, (err, filename) => {
      if (err) return res.send({ success: false, message: err.message })
      console.log(`file uploaded with name: ${filename}`)
      res.send({ success: true, message: 'File uploaded!!!' })
    })
  },

  uploadNewVersion (req, res) {
    draftNewVersionUpload(req, res, async (err) => {
      if (err) return res.send({ success: false, message: err.message })
      const versionNumber = req.body.versionNumber
      if (!versionNumber || isNaN(versionNumber)) return res.send({ success: false, message: 'Invalid draft version' })

      // console.log(`file uploaded with name: ${req.files[0].filename}, ${err}, ${JSON.stringify(req.body)}`)
      const filePath = `${config.server.draftFinalDocsDir}/temp/${req.files[0].filename}`
      try {
        // const result = await mammoth.convertToHtml({ path: filePath }) // convert the doc file to html
        // const html = result.value // The generated HTML

        const doc = await docManager.convertTo('odt', filePath, true)

        // let result = await docx2html(filePath)
        // const html = result.toString()
        // const messages = result.messages // Any messages, such as warnings during conversion
        let data = req.body
        let resp
        if (data.draftId) { // only update draft version
          data.newDocumentVersion = doc
          resp = await updateVersion(data) // store in database
        } else { // create a new draft
          data.document = doc
          resp = await createDraft(data)
        }
        res.send(resp) // send converted file to client as response
      } catch (err) {
        console.log('erro:', err.message)
        res.send({ success: false, message: err.message })
      }

      // delete file
      fs.unlinkSync(filePath)
    })
  },

  async getRawDocument (req, res) {
    // let userId = req.body.userId
    console.log('QUERYPARMS:', req.query)
    let appointment = req.query.appointment
    let draftId = req.query.draftId
    let type = req.query.type
    let fileId = req.query.fileId
    let version = req.query.version

    let draftPath = ''
    if (type !== 'formal') {
      draftPath = `${config.server.draftFinalDocsDir}/USER/${appointment}/draft_${draftId}`
    } else {
      draftPath = `${config.server.draftFinalDocsDir}/ARCHIVE/draft_${draftId}`
    }

    draftPath += `/version_${version}/document_${fileId}.odt`

    try {
      let doc = await docManager.convertTo('docx', draftPath, false)
      res.send(doc)
    } catch (err) {
      console.log('Error:', err.message)
      res.send(false)
    }
  }
}

module.exports = controller
