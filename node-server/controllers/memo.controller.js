const Model = require('../database/models')
const Memo = Model.Memo
const config = require('../config')
const logger = require('../lib/log')
const archiveHelper = require('../lib/archiveHelper')

// ****************** multer configuration file upload ******************
const multer = require('multer')
const fs = require('fs')
function dest (req, file, callback) {
  let dir = config.server.memoUploadDir
  try {
    if (!fs.existsSync(dir)) {
      logger.info(`making a new dir for ${dir}`)
      fs.mkdirSync(dir)
    }
    callback(null, dir)
  } catch (err) {
    logger.error(err.message)
  }
}

var storage = multer.diskStorage({
  destination: dest,
  filename: async function (req, file, callback) {
    console.log('typ: ', req.body)
    Model.Memo.create({
      type: req.body.file_type,
      content: req.body.description,
      subject: req.body.description,
      user_id: req.body.user_id,
      path: '-'
    }).then((memoId) => {
      logger.success('memoId', `${memoId}`)
      logger.success(`originalFilename: ${file.originalname}`)
      callback(null, `${memoId}.pdf`)
    }).catch((err) => {
      logger.error(err.message)
      callback(new Error('Failed to create Memo'))
    })
  }
})

var upload = multer({ storage: storage }).array('file', 12)
// **************** End of Multer config *******************

const controller = {
  getMemo: async function (req, res) {
    const memoId = req.params.memoId
    const memo = (await Model.Memo.get({ id: memoId }))[0]
    const signature = (await Model.Signature.get({ user_id: memo.user_id }))[0]
    memo.signature = signature.path
    memo.initial = signature.initial
    res.send({
      success: true,
      data: memo
    })
  },
  sendMemo: async function (req, res) {
    const memoId = await Memo.create({
      type: 'html',
      content: req.body.content,
      subject: req.body.subject,
      _to: req.body.to,
      _from: req.body.from,
      name_in_block_letter: req.body.name,
      rank: req.body.rank,
      appointment: req.body.appointment,
      signature: req.body.signature,
      initial: req.body.initial,
      user_id: req.body.userId,
      path: '-'
    })

    if (memoId) {
      res.end(JSON.stringify({
        success: true,
        memoId
      }))
    } else {
      res.end(JSON.stringify({
        success: false
      }))
    }
  },

  createMemo: function (req, res) {
    // this function uploads a memo from officers and sends it to CC
    upload(req, res, async function (err, filename) {
      if (err) {
        return res.end('Something went wrong:( -' + err.message)
      }

      logger.info('MFileName: ', req.files[0].filename)

      // respond to caller
      res.end(JSON.stringify({
        data: {
          fileId: (req.files[0].filename.split('.'))[0]
        },
        success: true
      }))
    })
  },

  uploadMemo: async function (req, res) {
    // this function does not upload any memo, it simply moves an already uploaded memo to the archive
    const deptId = req.body.department
    const subdeptId = req.body.sub_department
    const volId = req.body.volume
    const folioNumber = req.body.folio_number
    const memoId = req.body.memo_id
    const userId = req.body.user_id

    const memo = (await Model.Memo.get({ id: memoId }))[0]
    let memoData = memo.toJson()
    if (req.body.subject) memoData.subject = req.body.subject

    // sanity checks
    if (!deptId || !subdeptId || !volId) {
      return res.send({
        success: false,
        message: 'Department, subDeparment or volume Id is empty'
      })
    }
    if (!folioNumber) {
      return res.send({
        success: false,
        data: 'Folio number is empty'
      })
    }

    const f = await Model.File.get({
      folio_number: folioNumber,
      volume_id: volId
    })

    const dept = (await Model.Department.get({ id: deptId }))[0]
    const subdept = (await Model.Subdepartment.get({ id: subdeptId }))[0]
    const volume = (await Model.Volume.get({ id: volId }))[0]

    if (f.length === 0) {
      if (memoData.type !== 'html') {
        // move the pdf file
        const deptN = dept.number.trim()
        const sdeptN = subdept.number.trim()
        const yr = volume.year.trim()
        const vol = volume.name.trim()
        const memoPath = config.server.memoUploadDir + `/${memoData.id}.pdf`
        const archivePath = config.server.archiveRootURL + `/${deptN}/${sdeptN}/${yr}/${vol}`
        logger.info('memoPath:', memoPath)
        logger.info('archivePath:', archivePath)
        if (!await archiveHelper.moveFile(memoPath, archivePath, `${folioNumber}`)) {
          return res.send({
            success: false,
            message: 'could not locate file'
          })
        }
      }
      console.log('memo type ', memo.type)
      const fileId = await Model.File.create({
        type: memoData.type,
        content: memoData.content,
        _to: memoData._to,
        _from: memoData._from,
        name_in_block_letter: memoData.name_in_block_letter,
        rank: memoData.rank,
        signature: memoData.signature,
        initial: memoData.initial,
        appointment: memoData.appointment,
        subject: memoData.subject,
        created_at: memoData.created_at,
        folio_number: folioNumber,
        volume_id: volId
      })
      if (fileId) {
        res.send({
          success: true,
          data: {
            message: 'Memo Uploaded!!',
            fileId: fileId
          }
        })
      } else {
        res.send({
          success: false,
          data: 'could not upload file'
        })
      }
    } else {
      res.send({
        success: false,
        data: 'File with folio number already exist in that volume'
      })
    }
  }
}

module.exports = controller
