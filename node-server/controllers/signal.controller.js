const Model = require('../database/models')
const Inbox = Model.Inbox
const SignalComment = Model.SignalComment
const Signal = Model.Signal
const User = Model.User
const Appointment = Model.Appointment
const config = require('../config')
const logger = require('../lib/log')
const userCache = require('../lib/cacheHelper')
const onlineUsers = require('../lib/socketHandler').onlineUsers

// ****************** multer configuration ******************
const multer = require('multer')
const fs = require('fs')
function dest (req, file, callback) {
  var dir = config.server.signalUploadDir // './hqenc_fms/static/uploads/signals/';
  try {
    if (!fs.existsSync(dir)) {
      logger.info(`making a new dir for ${dir}`)
      fs.mkdirSync(dir)
    }
    callback(null, dir)
  } catch (err) {
    logger.error(err.message)
  }
}

var storage = multer.diskStorage({
  destination: dest,
  filename: async function (req, file, callback) {
    Signal.create({
      type: req.body.type,
      subject: req.body.subject,
      user_id: req.body.user_id
    }).then((sigId) => {
      logger.success('sigId', `${sigId}`)
      callback(null, `${sigId}`)
    }).catch((err) => {
      logger.error(err.message)
      callback(new Error('Failed to create Signal'))
    })
  }
})

var upload = multer({ storage: storage }).array('files', 12)
// **************** End of Multer config *******************

async function updateSignalCache (type, userId, amount = 1) {
  const cache = await userCache.getCache(userId)
  let cacheInboxType = ''
  switch (type) {
    case 2:
      cacheInboxType = 'cIncoming'
      break

    case 3:
      cacheInboxType = 'cOutgoing'
      break

    case 4:
      cacheInboxType = 'cNg'
      break

    case 5:
      cacheInboxType = 'cNhq'
      break
  }

  if (cache) {
    cache['inboxCount']['cSignals'] += amount
    cache['inboxCount']['cSignalsUnread'] += amount
    cache['inboxCount'][`${cacheInboxType}`] += amount
    cache['inboxCount'][`${cacheInboxType}Unread`] += amount
    await userCache.updateCache(userId, cache, true)
  }
}

const controller = {
  signalTask: async function (req, res) {
    logger.success('signal recieved:', req.body)
    let inboxType = 0
    switch (req.body.type) {
      case 'incomming':
        inboxType = 2
        break

      case 'outgoing':
        inboxType = 3
        break

      case 'ng':
        inboxType = 4
        break

      case 'nhq':
        inboxType = 5
        break
    }

    for (let recipient of req.body.recipients) {
      logger.info('recipient', recipient)
      const inboxId = await Inbox.create(
        {
          title: req.body.title,
          user_id: recipient.id,
          file_id: req.body.file_id,
          sender_email: req.body.sender_email,
          text: req.body.content,
          type: inboxType
        }
      )

      // update signal inbox cache
      await updateSignalCache(inboxType, recipient.id)

      if (onlineUsers[recipient.email]) {
        logger.info('sending to', recipient.email)
        onlineUsers[recipient.email].emit('message', {
          type: 'inbox',
          content: {
            title: req.body.title,
            text: req.body.content,
            user_id: recipient.id,
            fileId: req.body.file_id,
            senderEmail: req.body.sender_email,
            date: new Date().toDateString(),
            commited: 0,
            id: inboxId,
            type: inboxType
          }
        })
      }
    }
    res.send(JSON.stringify({
      success: true
    }))
  },

  signalComment: async function (req, res) {
    logger.info('comment:', req.body)
    const commentId = await SignalComment.create({
      content: req.body.content,
      signal_id: req.body.signal_id,
      user_id: req.body.user_id
    })

    if (commentId) {
      const inbx = await Inbox.get({
        user_id: req.body.user_id,
        file_id: req.body.signal_id
      })
      if (inbx) {
        logger.info('inbx:', inbx)
        for (let i = 0; i < inbx.length; i++) {
          inbx[i].commited = 1
          await inbx[i].save()
          await updateSignalCache(Number(inbx[i].type), req.body.user_id, -1)
          // .then((result) => {
          //   logger.info('result:', result)
          // })
          // .catch((err) => {
          //   console.log(err.message)
          // })
        }
      }
      res.end(JSON.stringify({
        success: true
      }))
    }
  },

  signalComments: async function (req, res) {
    const signalId = req.params.signalId
    const signalQ = `
      SELECT signal_comments.id, signal_comments.signal_id, signal_comments.user_id, signal_comments.content, 
      signal_comments.created_at, appointments.color, appointments.name as appointment FROM signal_comments
      INNER JOIN users ON users.id = signal_comments.user_id
      INNER JOIN appointments ON appointments.id = users.appointment
      WHERE signal_comments.signal_id = "${signalId}"
      `
    const comments = (await Model.SignalComment.Query(signalQ)).results
    res.send(JSON.stringify(comments))
  },

  signalUpload: async function (req, res) {
    upload(req, res, async function (err, filename) {
      if (err) {
        return res.end('Something went wrong:( -' + err.message)
      }
      // respond to caller
      res.end(JSON.stringify({
        success: true
      }))

      // create inbox message to all clients
      logger.info('sent down filename', req.files[0].filename)
      // const users = await User.get({
      //   active: 1
      // })
      const users = JSON.parse(req.body.recipients)
      if (!users || !users.length) return
      console.log('USERS:', users)
      for (let user of users) {
        // const appointment = (await Appointment.get({
        //   id: user.appointment
        // }))[0]

        if (user.appointment && user.appointment !== 'non') {
          const inboxId = await Inbox.create({
            title: req.body.subject,
            user_id: user.id,
            file_id: req.files[0].filename,
            sender_email: req.body.sender_email,
            text: req.body.subject,
            type: req.body.type,
            commited: user.appointment === 'MSO' ? 1 : 0
          })

          // update cache
          await updateSignalCache(Number(req.body.type), user.id, 1)

          if (onlineUsers[user.email]) {
            logger.info('sending to', user.email)
            onlineUsers[user.email].emit('message', {
              type: 'inbox',
              content: {
                title: req.body.subject,
                text: req.body.subject,
                user_id: user.id,
                file_id: req.files[0].filename,
                senderEmail: req.body.sender_email,
                created_at: new Date().toDateString(),
                commited: user.appointment === 'MSO' ? 1 : 0,
                id: inboxId,
                type: req.body.type
              }
            })
          }
        }
      }
    })
  },

  commitSignal: async function (req, res) {
    const userId = req.body.user_id
    const fileId = req.body.file_id
    if (!userId) return res.send({ success: false, data: 'User Id missing' })
    if (!fileId) return res.send({ success: false, data: 'File Id missing' })
    const inbx = await Inbox.fetch({
      user_id: userId,
      file_id: fileId
    })
    const usrs = await userCache.getUsersWithSameAppointment(userId)
    for (let u of usrs) {
      await Inbox.update({
        set: {
          commited: 1
        },
        queryKey: {
          user_id: u.id,
          file_id: fileId
        }
      })
    }
    res.send({
      success: true,
      data: 'Signal Acknowledged!!!'
    })
    await updateSignalCache(Number(inbx[0].type), userId, -1)

    // .then(async (success) => {
    //   if (success) {
    //     res.send({
    //       success: true,
    //       data: 'Signal Acknowledged!!!'
    //     })
    //     await updateSignalCache(Number(inbx[0].type), userId, -1)
    //   } else {
    //     res.send({
    //       success: false,
    //       data: 'Signal acknowledgement failed'
    //     })
    //   }
    // })
  }
}

module.exports = controller
