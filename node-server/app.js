
const express = require('express')
const app = express()
const http = require('http').Server(app)
const config = require('./config')
const io = require('socket.io')(http)
const socketHandler = require('./lib/socketHandler')
const routes = require('./routes/app.routes')
const middleWareLogger = require('./middleware/logger')
const serverFunctions = require('./lib/system')

app.use(middleWareLogger)
app.use(require('cors')())
app.use(express.urlencoded({ extended: true }))
app.use(express.json())

routes(app)

io.on('connection', socketHandler.handler)

http.listen(config.server.port, () => {
  console.log(`api running on port ${config.server.port}`)
  // start pfsense
  serverFunctions.startPfsense((err, output) => {
    if (err) {
      console.log(`PfSense Error: ${err.message}`)
    } else {
      console.log(`PfSense started using Name: ${config.server.pfsenseName}`)
    }
  })
})
