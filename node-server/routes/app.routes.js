const UserController = require('../controllers/user.controller')
const MemoController = require('../controllers/memo.controller')
const SignalController = require('../controllers/signal.controller')
const ThreadController = require('../controllers/thread.controller')
const InboxController = require('../controllers/inbox.controller')
const ArchiveController = require('../controllers/archive.controller')
const DraftController = require('../controllers/draft.controller')
const ChatController = require('../controllers/chat.controller')
const UpgradeController = require('../controllers/upgrade.controller')
const config = require('../config')
const logger = require('../lib/log')
const archiveHelper = require('../lib/archiveHelper')
const scheduler = require('../lib/scheduler')
const system = require('../lib/system')

module.exports = function (app) {
  app.get('/config', (req, res) => {
    res.send({
      memoDir: config.client.memoDir,
      signalDir: config.client.signalDir,
      archiveDir: config.client.archiveDir,
      signatureDir: config.client.signatureDir,
      profilePictureDir: config.client.profilePictureDir,
      outgoingDocsDir: config.client.outgoingDocsDir,
      draftFinalDocsDir: config.client.draftFinalDocsDir
    })
  })

  app.post('/file/exists', async (req, res) => {
    const fileType = req.body.fileType
    const fileName = req.body.fileName
    let e = false
    if (!fileType) return res.send({ success: false, data: 'file Type is empty' })
    switch (fileType) {
      case 'signature':
        e = await archiveHelper.pathExists(`${config.server.signatureDir}/${fileName}`)
        break

      case 'profilePicture':
        e = await archiveHelper.pathExists(`${config.server.profilePictureDir}/${fileName}`)
        break
    }
    console.log('FILE EXIST:', req.body, e)
    res.send({
      success: e,
      data: ''
    })
  })

  // ********* User Routes ***** //
  app.get('/users', UserController.getUsers)
  app.get('/users/:userId', UserController.getUser)
  app.get('/appointments', UserController.appointments)
  app.post('/register', UserController.register)
  app.post('/login', UserController.login)
  app.post('/user/update/profile', UserController.updateProfile)
  app.post('/user/update/picture', UserController.uploadProfilePic)
  app.post('/user/update/signature', UserController.uploadSignature)

  // *********************** INBOX ROUTES ******************************
  app.get('/users/:userId/inbox', InboxController.getUserInbox)
  app.get('/users/:userId/outbox', InboxController.getUserOutbox)
  app.get('/users/:userId/trash', InboxController.getTrashedMessages)
  app.get('/inbox/:fileId', InboxController.getInbox)
  app.get('/inbox/recent/:userId/:type', InboxController.getRecentMessages)
  app.get('/inbox/count', InboxController.countInbox)
  app.get('/inbox/count/regular', InboxController.countRegularInbox)
  app.get('/inbox/count/signals', InboxController.countSignals)
  app.get('/inbox/count/signals/outgoing', InboxController.countOutSignal)
  app.get('/inbox/count/signals/incoming', InboxController.countInSignal)
  app.get('/inbox/count/signals/ng', InboxController.countNgSignal)
  app.get('/inbox/count/signals/nhq', InboxController.countNhqSignal)
  app.get('/inbox/count/trashed', InboxController.countTrashedMessages)
  app.post('/inbox/trash', InboxController.trashMessages)
  app.post('/inbox/markread', InboxController.markAsRead)
  // ********************** END OF INBOX ROUTE **************************

  // *********************** SEND MEMO ******************************
  app.post('/send/memo', MemoController.sendMemo)
  app.post('/create/memo', MemoController.createMemo)
  app.post('/upload/memo', MemoController.uploadMemo)
  app.get('/memo/:memoId', MemoController.getMemo)
  // ********************** END OF MEMO SENDING *********************

  // ********************** SIGNAL ROUTES *********************
  app.post('/signal/task', SignalController.signalTask)
  app.post('/signal/comment', SignalController.signalComment)
  app.post('/signal/commit', SignalController.commitSignal)
  app.get('/signal/comments/:signalId', SignalController.signalComments)
  app.post('/upload/signal', SignalController.signalUpload)
  // ********************** END OF SIGNAL ROUTES *********************

  // ********************** THREAD ROUTES *********************
  // app.get('/thread/:fileId', ThreadController.threadFileId)
  app.get('/thread/:fileId', ArchiveController.file)
  app.get('/stamp/:fileId', ThreadController.getStamp)
  app.post('/thread/create/comment', ThreadController.commentOnThread)
  app.post('/file/acknowledge', ThreadController.acknowledge)
  app.post('/thread/update/type', ThreadController.updateThreadType)
  // ********************** END OF THREAD ROUTES *********************

  // ********************** ARCHIVE ROUTES *********************
  app.get('/archive', ArchiveController.department)
  app.get('/archive/:deptId', ArchiveController.subDepartment)
  app.get('/archive/:deptId/:subDeptId', ArchiveController.year)
  app.get('/archive/:deptId/:subDeptId/:year', ArchiveController.volume)
  app.get('/archive/:deptId/:subDeptId/:year/:volId', ArchiveController.files)
  app.get('/archive/:deptId/:subDeptId/:year/:volId/:fileId', ArchiveController.file)
  app.get('/last/folio/:volId', ArchiveController.getLastFolioNumber)

  app.post('/archive/create/:deptNumber?/:subDeptNumber?/:year?/:volume?', ArchiveController.createArchive)
  app.post('/upload', ArchiveController.upload)
  app.post('/edit/subject/:fileId', ArchiveController.editFileSubject)
  app.post('/edit/location/:fileId', ArchiveController.changeFileLocation)
  app.post('/document/sign', ArchiveController.signDocument)
  app.post('/outgoing/upload', ArchiveController.uploadOutgoingDoc)

  // **********************  *********************
  app.post('/sign', ArchiveController.signDocument)

  // ********************** DRAFT ROUTES *********************
  app.post('/draft/update', DraftController.updateVersion)
  app.post('/draft/create', DraftController.create)
  app.post('/draft/upload', DraftController.upload)
  app.post('/draft/version/upload', DraftController.uploadNewVersion)
  app.post('/draft/tags', DraftController.addTag)
  app.post('/draft/approve', DraftController.approveDraft)
  app.post('/draft/sequence/commit', DraftController.markAsRead)
  app.get('/draft/:draftId', DraftController.getDraft)
  app.get('/drafts/:userId/:draftId?', DraftController.getByUserId)
  app.get('/draft/sequence/:userId', DraftController.inbox)
  app.get('/draft/sequence/recent/:userId', DraftController.recentMessages)
  app.get('/draft/count/sequence', DraftController.countSequence)
  app.get('/draft/fetch/raw', DraftController.getRawDocument)
  // ********************** END OF ROUTES *********************

  // ********************** CHAT ROUTES *********************
  app.post('/chat/message', ChatController.storeMessage)
  app.get('/chat/recent/:userId', ChatController.recentChats)
  // ********************** END CHAT ROUTES *********************

  // ********************** ADMIN ROUTES *********************
  app.post('/admin/uinbox', UserController.manageInbox)
  app.post('/admin/notification', (req, res) => {
    const message = req.body.message
    const beginTime = req.body.beginTime
    const duration = req.body.duration
    const interval = req.body.interval
    const enableAlert = req.body.alert
    const type = req.body.type
    const socketHandler = require('../lib/socketHandler')

    switch (type) {
      case '1':
        scheduler.schedule({
          task: function (message) {
            logger.info(`sending message... ${message}`)
            for (let c in socketHandler.onlineUsers) {
              socketHandler.onlineUsers[c].emit('message', {
                type: 'notify',
                content: {
                  type: '1',
                  message,
                  enableAlert
                }
              })
            }
          },
          input: message,
          duration: Number(duration) * 60 * 1000,
          interval: Number(interval) * 1000,
          beginTime
        }, (err, tId) => {
          if (err) return res.send({ success: false, data: err.message })
          // listen to the end event and shutdown server
          scheduler.on(`end-${tId}`, (tId) => {
            // shutdown server imediately
            system.shutdown((err, output) => {
              if (err) {
                throw err
              }
            })
          })
          res.send({
            success: true,
            data: tId
          })
        })
        break

      case '2':
        scheduler.schedule({
          task: function (message) {
            logger.info(`sending reboot message... ${message}`)
            for (let c in socketHandler.onlineUsers) {
              socketHandler.onlineUsers[c].emit('message', {
                type: 'notify',
                content: {
                  type: '2',
                  message,
                  enableAlert
                }
              })
            }
          },
          input: message,
          duration: Number(duration) * 60 * 1000,
          interval: Number(interval) * 1000,
          beginTime
        }, (err, tId) => {
          if (err) return res.send({ success: false, data: err.message })
          // listen to the end event
          scheduler.on(`end-${tId}`, (tId) => {
            // reboot server imediately
            system.reboot((err, output) => {
              if (err) {
                throw err
              }
            })
          })
          res.send({
            success: true,
            data: tId
          })
        })
        break

      case '3':
        // general notification
        scheduler.schedule({
          task: function (message) {
            logger.info(`sending message... ${message}`)
            for (let c in socketHandler.onlineUsers) {
              socketHandler.onlineUsers[c].emit('message', {
                type: 'notify',
                content: {
                  type: '3',
                  message,
                  enableAlert
                }
              })
            }
          },
          input: message,
          duration: Number(duration) * 60 * 1000,
          interval: Number(interval) * 1000,
          beginTime
        }, (err, tId) => {
          if (err) return res.send({ success: false, data: err.message })
          res.send({
            success: true,
            data: tId
          })
        })
        break
    }
  })
  // ********************** END OF ADMIN ROUTES  ***************

  // upgrade script
  app.post('/upgrade/db', UpgradeController.dbUpgrade)
}
