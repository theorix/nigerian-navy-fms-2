const File = require('./database/models/File')
const Folder = require('./database/models/Folder')
const Category = require('./database/models/Category')
const Subcategory = require('./database/models/Subcategory')
const Volume = require('./database/models/Volume')
const fs = require('fs')
const Path = require('path')

/* console.log()
if (process.argv.length <= 2) {
  console.log('Usage: ' + __filename + 'path/to/directory')
  process.exit(-1)
}

const path = process.argv[2] */

/* fs.readdir(path, function (err, items) {
  console.log(items)
  if (err) return

  for (var i = 0; i < items.length; i++) {
    console.log(items[i])
  }
}) */

// function will check if a directory exists, and create it if it doesn't
function pathExists2 (path, create = false) {
  return new Promise((resolve, reject) => {
    fs.stat(path, function (err, stats) {
      // Check if error defined and the error code is "not exists"
      if (err && err.code === 'ENOENT') {
        // Create the directory
        if (create) {
          fs.mkdir(path, (err) => {
            if (err) return reject(err)
            resolve(true)
          })
        } else {
          resolve(false)
        }
      } else {
        // just in case there was a different error:
        if (err) {
          return reject(err)
        }
        resolve(true)
      }
    })
  })
}

function pathExists (path, create = false) {
  return new Promise((resolve, reject) => {
    fs.access(path, (err) => {
      if (err && err.code === 'ENOENT') {
        if (create) {
          fs.mkdir(path, (err) => {
            if (err) return reject(err)
            resolve(true)
          })
        } else {
          resolve(false)
        }
      } else resolve(true)
      // resolve(true)
    })
  })
}

function isDirectory (path) {
  return new Promise((resolve, reject) => {
    fs.stat(path, (err, stats) => {
      if (err) return reject(err)
      return resolve(stats.isDirectory())
    })
  })
}

function readDir2 (path) {
  return new Promise(async (resolve, reject) => {
    fs.stat(path, (err, stats) => {
      if (err) return resolve([])
      if (stats.isDirectory()) {
        fs.readdir(path, function (err, items) {
          if (err) return reject(err)
          return resolve(items)
        })
      } else {
        return resolve([])
      }
    })
  })
}

function readDir (path) {
  return new Promise(async (resolve, reject) => {
    if (await isDirectory(path)) {
      fs.readdir(path, function (err, items) {
        if (err) return reject(err)
        return resolve(items)
      })
    } else return resolve([])
  })
}

async function upload (file, D) {
  // console.log('uploaded', file, 'in ', Path.join(D))
  const data = D.split('/'); console.log(data)

  let catId = await Category.get({
    name: data[2]
  })

  if (catId.length === 0) {
    catId = await Category.create({
      name: data[2]
    })
  } else catId = catId[0].id

  if (catId) {
    let subcatId = await Subcategory.get({
      name: data[3],
      category_id: catId
    })

    if (subcatId.length > 0) {
      subcatId = subcatId[0].id
    } else {
      subcatId = await Subcategory.create({
        name: data[3],
        category_id: catId
      })
    }
    let volume = data[5].replace(/[^a-zA-Z ]/g, ''); console.log('vole:',volume)

    let volumeId = await Volume.get({
      year: data[4],
      name: volume
    })

    if (volumeId.length > 0) {
      volumeId = volumeId[0].id
    } else {
      volumeId = await Volume.create({
        opening_date: '2018-12-12',
        closing_date: '2019-12-12',
        year: data[4],
        name: volume
      })
    }

    let folderId = await Folder.get({
      path: Path.join(D),
      sub_category_id: subcatId,
      volume_id: volumeId
    })

    if (folderId.length > 0) {
      folderId = folderId[0].id
    } else {
      folderId = await Folder.create({
        path: Path.join(D),
        sub_category_id: subcatId,
        volume_id: volumeId
      })
    }

    const fileId = await File.create({
      folio_number: file,
      folder_id: folderId,
      content: '-'
    })
    console.log('file ID:', fileId)
  }
}

function resolvePath (name, d, D) {
  const path = Path.join(D.slice(0, d).join('/'), name); console.log('resolve:', path)
  return path
}

async function processDir (dir, d, D) {
  D[d] = dir
  const path = resolvePath(dir, d, D) // console.log(path, 'Exist?', await pathExists(path))
  const dirs = await readDir(path)
  for (let file of dirs) {
    if (await isDirectory(Path.join(path, file))) {
      await processDir(file, d + 1, D)
    } else {
      await upload(file, path)
    }
  }
}

// generate random file structure
/*
  {

  }
*/

async function generateDirs (path, prefix = 'D') {
  const dirList = []
  let ok = await pathExists(path, true)
  if (ok) { // console.log('ok:', path)
    const n = Math.floor(Math.random() * 10 + 1)
    // const m = Math.floor(Math.random() * 1000)
    for (let i = 0; i < n; i++) {
      let d = Math.floor(Math.random() * 1000)
      d = prefix + d
      const status = await pathExists(Path.join(path, d), true)
      if (status === true) dirList.push(d)
    }
    return dirList
  } else {
    return dirList
  }
}

function generateFile (name, path) {
  return new Promise((resolve, reject) => {
    fs.open(Path.join(path, 'f' + name), 'a', (error, fd) => {
      console.log('generating file...', name, 'in', path)
      if (error) return reject(error)
      fs.close(fd, (err) => {
        if (err) return reject(err)
        resolve(true)
      })
    })
  })
}

async function generateDir (dir, d, D) {
  console.log(d)
  const path = resolvePath(dir, d, D)
  if (d < 4) {
    D[d] = dir
    const dirs = await generateDirs (path)
    for (let file of dirs) {
      await generateDir(file, d + 1, D)
      console.log('ok then:', d)
    }
  } else {
    const n = Math.floor(Math.random() * 100)
    for (let i = 0; i < n; i++) {
      await generateFile(Math.floor(Math.random() * 1000), path)
    }
  }
}

(
  async function () {
    // const items = await readDir(path)
    // return items
    // await processDir(path, 0, [])
    // console.log('Done.......')
    // console.log(await pathExists2('./f', true))
    // console.log(await generateDirs('../f'))
    // await generateDir(path, 0, [])
    // process.exit(1)
    // console.log('waiting...')
  }
)()

module.exports = processDir
